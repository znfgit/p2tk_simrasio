select 
        distinct
        --top 100
        rjm.mata_pelajaran_id,
        rjm.nama_matpel,
        --ref_linier_bidstudi_matpel.bidang_studi_id, 
        ptk_tersedia.sekolah_id,
        ptk_tersedia.nama_sekolah,
        ptk_tersedia.kode_wilayah,
        ptk_tersedia.ptk_id, 
        ptk_tersedia.nama_ptk,
        ptk_tersedia.kode_bidang_studi_sertifikasi,
        ptk_tersedia.nama_bid_studi_sertifikasi,
        ptk_tersedia.bid_studi_ijazah_terakhir,
        ptk_tersedia.nama_bid_studi_ijazah_terakhir,
        ptk_tersedia.is_honorer,
        ptk_tersedia.is_pns,
        ptk_tersedia.is_s1,
        ptk_tersedia.is_sertifikasi,
        ptk_tersedia.jjm
        --ref_linierisasi.is_linier
        from 
        (       
            select distinct
                mata_pelajaran_id, 
                nama as nama_matpel,
                kurikulum_id,
                jumlah_jam_maksimum
            from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui
                where nama NOT LIKE '%Agama%' 
                and (kurikulum_id = 3 or kurikulum_id = 4)
        ) rjm
        inner join ref_linier_bidstudi_matpel on rjm.mata_pelajaran_id = ref_linier_bidstudi_matpel.mata_pelajaran_id
        inner join pendataan_2014.dbo.ref_linierisasi ref_linierisasi on rjm.mata_pelajaran_id = ref_linierisasi.mata_pelajaran_id and is_linier = 1
        inner join ptk_tersedia 
            on ptk_tersedia.bid_studi_utama = ref_linier_bidstudi_matpel.bidang_studi_id
            or ptk_tersedia.matpel_diampu = rjm.mata_pelajaran_id

        --order by sekolah_id, mata_pelajaran_id
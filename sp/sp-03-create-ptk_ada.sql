ALTER PROCEDURE [dbo].[sp_03_create_ptk_ada]
AS
BEGIN

declare @semester int;
set @semester = 20141;

drop table ptk_sd_ada;

select 
		distinct
		--top 100
		rjm.mata_pelajaran_id,
		rjm.nama_matpel,
		--ref_linier_bidstudi_matpel.bidang_studi_id, 
		ptk_tersedia.sekolah_id,
		ptk_tersedia.nama_sekolah,
		ptk_tersedia.kode_wilayah,
		ptk_tersedia.ptk_id, 
		ptk_tersedia.nama_ptk,
		ptk_tersedia.tgl_lahir,
		ptk_tersedia.nuptk,
		ptk_tersedia.nrg,
		ptk_tersedia.kode_bidang_studi_sertifikasi,
		ptk_tersedia.nama_bid_studi_sertifikasi,
		ptk_tersedia.bid_studi_ijazah_terakhir,
		ptk_tersedia.nama_bid_studi_ijazah_terakhir,
		ptk_tersedia.is_honorer,
		ptk_tersedia.is_pns,
		ptk_tersedia.is_s1,
		ptk_tersedia.is_sertifikasi,
		ptk_tersedia.jjm
		--ref_linierisasi.is_linier
		into ptk_sd_ada
		from 
		(		
			select distinct
				mata_pelajaran_id, 
				nama as nama_matpel,
				kurikulum_id,
				jumlah_jam_maksimum
			from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui
				where nama NOT LIKE '%Agama%' 
				and (kurikulum_id = 3 or kurikulum_id = 4)
		) rjm
		inner join ref_linier_bidstudi_matpel on rjm.mata_pelajaran_id = ref_linier_bidstudi_matpel.mata_pelajaran_id
		inner join pendataan_2014.dbo.ref_linierisasi ref_linierisasi on rjm.mata_pelajaran_id = ref_linierisasi.mata_pelajaran_id and is_linier = 1
		inner join ptk_tersedia 
			on ptk_tersedia.bid_studi_utama = ref_linier_bidstudi_matpel.bidang_studi_id
			or ptk_tersedia.matpel_diampu = rjm.mata_pelajaran_id;

		--order by sekolah_id, mata_pelajaran_id
-- Add Primary Key
alter table ptk_sd_ada alter column sekolah_id varchar(255) not null;
alter table ptk_sd_ada add constraint pk_ptk_sd_ada PRIMARY KEY (mata_pelajaran_id, sekolah_id, ptk_id);

drop table ptk_smp_ada;

select 
		distinct
		--top 100
		rjm.mata_pelajaran_id,
		rjm.nama_matpel,
		--ref_linier_bidstudi_matpel.bidang_studi_id, 
		ptk_tersedia.sekolah_id,
		ptk_tersedia.nama_sekolah,
		ptk_tersedia.kode_wilayah,
		ptk_tersedia.ptk_id, 
		ptk_tersedia.nama_ptk,
		ptk_tersedia.tgl_lahir,
		ptk_tersedia.nuptk,
		ptk_tersedia.nrg,
		ptk_tersedia.kode_bidang_studi_sertifikasi,
		ptk_tersedia.nama_bid_studi_sertifikasi,
		ptk_tersedia.bid_studi_ijazah_terakhir,
		ptk_tersedia.nama_bid_studi_ijazah_terakhir,
		ptk_tersedia.is_honorer,
		ptk_tersedia.is_pns,
		ptk_tersedia.is_s1,
		ptk_tersedia.is_sertifikasi,
		ptk_tersedia.jjm
		--ref_linierisasi.is_linier
		into ptk_smp_ada
		from 
		(		
			select distinct
				mata_pelajaran_id, 
				nama as nama_matpel,
				kurikulum_id,
				jumlah_jam_maksimum
			from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui
				where nama NOT LIKE '%Agama%' 
				and (kurikulum_id = 7 or kurikulum_id = 8)
		) rjm
		inner join ref_linier_bidstudi_matpel on rjm.mata_pelajaran_id = ref_linier_bidstudi_matpel.mata_pelajaran_id
		inner join pendataan_2014.dbo.ref_linierisasi ref_linierisasi on rjm.mata_pelajaran_id = ref_linierisasi.mata_pelajaran_id and is_linier = 1
		inner join ptk_tersedia 
			on ptk_tersedia.bid_studi_utama = ref_linier_bidstudi_matpel.bidang_studi_id
			or ptk_tersedia.matpel_diampu = rjm.mata_pelajaran_id;

		--order by sekolah_id, mata_pelajaran_id

alter table ptk_smp_ada alter column sekolah_id varchar(255) not null;
alter table ptk_smp_ada add constraint pk_ptk_smp_ada PRIMARY KEY (mata_pelajaran_id, sekolah_id, ptk_id);

END
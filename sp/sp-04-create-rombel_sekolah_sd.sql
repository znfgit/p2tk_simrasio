ALTER PROCEDURE [dbo].[sp_04_create_rombel_sekolah_sd]
AS
BEGIN
  
	declare @maxSiswaPerRombelSD int;
	set @maxSiswaPerRombelSD = 32;

	declare @semester int;
	set @semester = 20141;

	drop table rombel_sekolah_sd;

	select
			--top 10
			sekolah.sekolah_id, 
			sekolah.nama, 
			sekolah.bentuk_pendidikan_id,
			sekolah.kode_wilayah,
			--20131 as semester_id,
			@semester as semester_id,

			rbl_ktsp_1.jml_rbl_ktsp_1,
			rbl_ktsp_2.jml_rbl_ktsp_2,
			rbl_ktsp_3.jml_rbl_ktsp_3,
			rbl_ktsp_4.jml_rbl_ktsp_4,
			rbl_ktsp_5.jml_rbl_ktsp_5,
			rbl_ktsp_6.jml_rbl_ktsp_6,
			
			rbl_2013_1.jml_rbl_2013_1,
			rbl_2013_2.jml_rbl_2013_2,
			rbl_2013_3.jml_rbl_2013_3,
			rbl_2013_4.jml_rbl_2013_4,
			rbl_2013_5.jml_rbl_2013_5,
			rbl_2013_6.jml_rbl_2013_6,

			rbl_ktsp_1.jml_siswarbl_ktsp_1,
			rbl_ktsp_2.jml_siswarbl_ktsp_2,
			rbl_ktsp_3.jml_siswarbl_ktsp_3,
			rbl_ktsp_4.jml_siswarbl_ktsp_4,
			rbl_ktsp_5.jml_siswarbl_ktsp_5,
			rbl_ktsp_6.jml_siswarbl_ktsp_6,

			rbl_2013_1.jml_siswarbl_2013_1,
			rbl_2013_2.jml_siswarbl_2013_2,
			rbl_2013_3.jml_siswarbl_2013_3,
			rbl_2013_4.jml_siswarbl_2013_4,
			rbl_2013_5.jml_siswarbl_2013_5,
			rbl_2013_6.jml_siswarbl_2013_6

	into rombel_sekolah_sd
	from Dapodik_2014.dbo.sekolah sekolah

	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_1, sum(jumlah_siswa) as jml_siswarbl_ktsp_1 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 1 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_1 
			on rbl_ktsp_1.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_1.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_2, sum(jumlah_siswa) as jml_siswarbl_ktsp_2 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 2 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_2 
			on rbl_ktsp_2.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_2.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_3, sum(jumlah_siswa) as jml_siswarbl_ktsp_3 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 3 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_3 
			on rbl_ktsp_3.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_3.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_4, sum(jumlah_siswa) as jml_siswarbl_ktsp_4 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 4 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_4 
			on rbl_ktsp_4.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_4.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_5, sum(jumlah_siswa) as jml_siswarbl_ktsp_5 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 5 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_5 
			on rbl_ktsp_5.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_5.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_6, sum(jumlah_siswa) as jml_siswarbl_ktsp_6 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 6 and kurikulum_id = 3 group by sekolah_id, semester_id) rbl_ktsp_6 
			on rbl_ktsp_6.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_6.semester_id = @semester)

	-- 2013 --
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_1, sum(jumlah_siswa) as jml_siswarbl_2013_1 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 1 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_1 
			on rbl_2013_1.sekolah_id = sekolah.sekolah_id and (rbl_2013_1.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_2, sum(jumlah_siswa) as jml_siswarbl_2013_2 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 2 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_2 
			on rbl_2013_2.sekolah_id = sekolah.sekolah_id and (rbl_2013_2.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_3, sum(jumlah_siswa) as jml_siswarbl_2013_3 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 3 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_3 
			on rbl_2013_3.sekolah_id = sekolah.sekolah_id and (rbl_2013_3.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_4, sum(jumlah_siswa) as jml_siswarbl_2013_4 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 4 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_4 
			on rbl_2013_4.sekolah_id = sekolah.sekolah_id and (rbl_2013_4.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_5, sum(jumlah_siswa) as jml_siswarbl_2013_5 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 5 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_5 
			on rbl_2013_5.sekolah_id = sekolah.sekolah_id and (rbl_2013_5.semester_id = @semester)
	left join 
			( select sekolah_id, semester_id, count(*) as jml_rbl_2013_6, sum(jumlah_siswa) as jml_siswarbl_2013_6 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 6 and kurikulum_id = 4 group by sekolah_id, semester_id) rbl_2013_6 
			on rbl_2013_6.sekolah_id = sekolah.sekolah_id and (rbl_2013_6.semester_id = @semester)

	where (sekolah.bentuk_pendidikan_id = 5 or sekolah.bentuk_pendidikan_id = 7);
	
	-- and sekolah.status_sekolah = 1; -- BERLAKUKAN NEGERI DAN SWASTA
	
	-- Add Primary Key
	alter table rombel_sekolah_sd add constraint pk_sekolah_id PRIMARY KEY (sekolah_id);

	update rombel_sekolah_sd set jml_rbl_ktsp_1 = 0 where jml_rbl_ktsp_1 is null;
	update rombel_sekolah_sd set jml_rbl_ktsp_2 = 0 where jml_rbl_ktsp_2 is null;
	update rombel_sekolah_sd set jml_rbl_ktsp_3 = 0 where jml_rbl_ktsp_3 is null;
	update rombel_sekolah_sd set jml_rbl_ktsp_4 = 0 where jml_rbl_ktsp_4 is null;
	update rombel_sekolah_sd set jml_rbl_ktsp_5 = 0 where jml_rbl_ktsp_5 is null;
	update rombel_sekolah_sd set jml_rbl_ktsp_6 = 0 where jml_rbl_ktsp_6 is null;

	update rombel_sekolah_sd set jml_rbl_2013_1 = 0 where jml_rbl_2013_1 is null;
	update rombel_sekolah_sd set jml_rbl_2013_2 = 0 where jml_rbl_2013_2 is null;
	update rombel_sekolah_sd set jml_rbl_2013_3 = 0 where jml_rbl_2013_3 is null;
	update rombel_sekolah_sd set jml_rbl_2013_4 = 0 where jml_rbl_2013_4 is null;
	update rombel_sekolah_sd set jml_rbl_2013_5 = 0 where jml_rbl_2013_5 is null;
	update rombel_sekolah_sd set jml_rbl_2013_6 = 0 where jml_rbl_2013_6 is null;

--select top 10 sekolah_id, semester_id, count(*) as jml_rbl_ktsp_1 from pendataan_2014.dbo.t_rombongan_belajar group by sekolah_id, semester_id
END
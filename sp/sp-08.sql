ALTER PROCEDURE [dbo].[sp-08-create-guru-permatpel]
AS
BEGIN

drop table guru_permatpel_sd;

select * 
into guru_permatpel_sd
from ptk_sd_ada;

drop table guru_permatpel_smp;

select * 
into guru_permatpel_smp
from ptk_smp_ada;

alter table guru_permatpel_sd alter column sekolah_id varchar(255) not null;
alter table guru_permatpel_sd add constraint pk_guru_permatpel_sd PRIMARY KEY (mata_pelajaran_id, sekolah_id, ptk_id);

alter table guru_permatpel_smp alter column sekolah_id varchar(255) not null;
alter table guru_permatpel_smp add constraint pk_guru_permatpel_smp PRIMARY KEY (mata_pelajaran_id, sekolah_id, ptk_id);

-- alter table ref_mst_wilayah add constraint pk_ref_mst_wilayah PRIMARY KEY (kode_wilayah);

END
ALTER PROCEDURE [dbo].[sp_02_create_ptk_tersedia]
AS
BEGIN

	declare @semester int;
	set @semester = 20141;

	drop table ptk_tersedia;

	select 
		--top 1000
		t_ptk_id as ptk_id,
		nama as nama_ptk,
		tgl_lahir,
		nuptk as nuptk,
		'000000000000' as nrg,
		status_kepegawaian_id,
		sekolah_id,
		nama_sekolah,
		'000000 ' as kode_wilayah,
		is_pns,
		0 is_honorer,
		is_guru_bk,
		kode_bidang_studi_sertifikasi,
		ijazah_terakhir_id,
		nama_ijazah_terakhir,
		99999 as bid_studi_ijazah_pra_s1,
		'Nanti Diisi Nama Mata Pelajaran Panjangan Dikit Lah .. Blabla Blabla Blabla Blabla Blabla Blabla ' as nama_bid_studi_ijazah_pra_s1,
		99999 as bid_studi_ijazah_terakhir,
		'Nanti Diisi Nama Mata Pelajaran Panjangan Dikit Lah .. Blabla Blabla Blabla Blabla Blabla Blabla ' as nama_bid_studi_ijazah_terakhir,
		0 is_s1,
		99999 as bid_studi_sertifikasi,
		'Nanti Diisi Nama Mata Pelajaran Panjangan Dikit Lah .. Blabla Blabla Blabla Blabla Blabla Blabla ' as nama_bid_studi_sertifikasi,
		99999 as bid_studi_utama,
		'Nanti Diisi Nama Mata Pelajaran Panjangan Dikit Lah .. Blabla Blabla Blabla Blabla Blabla Blabla ' as nama_bid_studi_utama,

		0 is_sertifikasi,
		99999 as matpel_diampu,
		'Nanti Diisi Nama Mata Pelajaran Panjangan Dikit Lah .. Blabla Blabla Blabla Blabla Blabla Blabla ' as nama_matpel_diampu,

		nama_tugas_tambahan,
		tugas_tambahan_id,
		jam_tugas_tambahan,
		tgl_pensiun,
		status_aktif,
		tt_aktif,
		status_aktif_tw1,
		status_aktif_tw2,
		status_aktif_tw3,
		status_aktif_tw4,
		jumlah_jam_mengajar as jjm
	into ptk_tersedia 
	from 
		pendataan_2014.dbo.t_ptk
	where sekolah_id is not null 
	and status_sekolah = 1
	and is_sekolah_induk = 1
	and status_aktif = 1
	order by sekolah_id;
	
	-- Add Primary Key
	alter table ptk_tersedia add constraint pk_ptk_id PRIMARY KEY (ptk_id);


	-- Update Bidang Studi Pendidikan Terakhir Pra S1
	with rwy_pnd as (
		select 
			-- top 100
			a.ptk_id,
			a.bidang_studi_id,
			a.jenjang_pendidikan_id,
			b.bidang_studi,
			ROW_NUMBER() OVER(PARTITION BY a.ptk_id ORDER BY a.jenjang_pendidikan_id DESC) AS rk 
		from Dapodik_2014.dbo.rwy_pend_formal a
		inner join Dapodik_2014.dbo.ref_bidang_studi b 
			on a.bidang_studi_id = b.bidang_studi_id
		where a.jenjang_pendidikan_id < 30
	)
	update ptk_tersedia
	set 
			bid_studi_ijazah_pra_s1 = rwy_pnd.bidang_studi_id,
			nama_bid_studi_ijazah_pra_s1 = rwy_pnd.bidang_studi
	from rwy_pnd
	where ptk_tersedia.ptk_id = rwy_pnd.ptk_id
	and rk = 1;

	-- Update Bidang Studi Pendidikan Terakhir S1 & S2
	with rwy_pnd as (
		select 
			-- top 100
			a.ptk_id,
			a.bidang_studi_id,
			a.jenjang_pendidikan_id,
			b.bidang_studi,
			ROW_NUMBER() OVER(PARTITION BY a.ptk_id ORDER BY a.jenjang_pendidikan_id DESC) AS rk 
		from Dapodik_2014.dbo.rwy_pend_formal a
		inner join Dapodik_2014.dbo.ref_bidang_studi b 
			on a.bidang_studi_id = b.bidang_studi_id
		where a.jenjang_pendidikan_id >= 30
	)
	update ptk_tersedia
	set 
			bid_studi_ijazah_terakhir = rwy_pnd.bidang_studi_id,
			nama_bid_studi_ijazah_terakhir = rwy_pnd.bidang_studi
	from rwy_pnd
	where ptk_tersedia.ptk_id = rwy_pnd.ptk_id
	and rk = 1;

	-- Baca mengajar apa sajakah guru ybs, kemudian masukkan ke ptk_tersedia
	with mengajar_per_ptk as (
		select *, ROW_NUMBER() OVER(PARTITION BY t_ptk_id ORDER BY jjm DESC) AS rk
		from 
		(
			select
				top 100
				t_ptk_id, 
				nama,
				bidang_studi_id,
				nama_matpel_diajarkan,
				sum(jjm) as jjm
			from 
				pendataan_2014.dbo.t_mengajar t_mengajar
			group by 
				t_ptk_id,
				nama,
				bidang_studi_id,
				nama_matpel_diajarkan
		) mengajar
	)
	update ptk_tersedia
	set 
			matpel_diampu = mengajar_per_ptk.bidang_studi_id,
			nama_matpel_diampu = mengajar_per_ptk.nama_matpel_diajarkan
	from mengajar_per_ptk
	where ptk_tersedia.ptk_id = mengajar_per_ptk.t_ptk_id
	and rk = 1;


	-- Bersih2
	update ptk_tersedia
	set 
			bid_studi_ijazah_terakhir = '',
			nama_bid_studi_ijazah_terakhir = ''
	where bid_studi_ijazah_terakhir = '99999'

	update ptk_tersedia
	set 
			bid_studi_ijazah_pra_s1 = '',
			nama_bid_studi_ijazah_pra_s1 = ''
	where bid_studi_ijazah_pra_s1 = '99999'

	-- Update status honorer
	update ptk_tersedia set is_honorer = 1 where status_kepegawaian_id = 8;

	-- Update bidang studi sertifikasi
	with bid_studi_sertifikasi as (
		select * from Dapodik_2014.dbo.ref_bidang_studi
	)
	update ptk_tersedia
	set
		bid_studi_sertifikasi = bidang_studi_id,
		nama_bid_studi_sertifikasi = bidang_studi
	from bid_studi_sertifikasi
	where 
		cast (kode as int) = cast (ptk_tersedia.kode_bidang_studi_sertifikasi as int);


  -- Update status S1 atau lebih
	update ptk_tersedia set is_s1 = 1 where ijazah_terakhir_id >= 30;

  -- Update status S1 atau lebih
	update ptk_tersedia set is_sertifikasi = 1 where kode_bidang_studi_sertifikasi is not null;

	-- Update kode_wilayah
	update ptk_tersedia 
	set ptk_tersedia.kode_wilayah = sekolah.kode_wilayah
	from sekolah
	where ptk_tersedia.sekolah_id = sekolah.sekolah_id 

	-- Bersih2 
	update ptk_tersedia
	set 
			bid_studi_sertifikasi = '',
			nama_bid_studi_sertifikasi = ''
	where bid_studi_sertifikasi = '99999'

	update ptk_tersedia
	set 
			matpel_diampu = '',
			nama_matpel_diampu = ''
	where matpel_diampu = '99999'
	
	-- Menentukan bidang studi utama: Bidang studi yang melekat pada guru ybs
  -- Ditentukan dengan urutan berikut
  -- 1. Sertifikat Pendidik
  -- 2. Ijazah Terakhir S1 / S2
  -- 3. Mata Pelajaran yang Diampu
	
	-- 1
	update ptk_tersedia 
	set
		bid_studi_utama = bid_studi_sertifikasi,
		nama_bid_studi_utama = nama_bid_studi_sertifikasi
	where 
		bid_studi_sertifikasi != ''
	
	-- 2
	update ptk_tersedia 
	set
		bid_studi_utama = bid_studi_ijazah_terakhir,
		nama_bid_studi_utama = nama_bid_studi_ijazah_terakhir
	where 
		bid_studi_ijazah_terakhir != ''
		and bid_studi_utama = '99999'			-- still empty

	-- 3
	update ptk_tersedia 
	set
		bid_studi_utama = bid_studi_ijazah_pra_s1,
		nama_bid_studi_utama = nama_bid_studi_ijazah_pra_s1
	where 
		bid_studi_ijazah_pra_s1 != ''
		and bid_studi_utama = '99999'			-- still empty

	-- Update NRG
	update ptk_tersedia
	set nrg = nrgtable.nrg
	from 
	(
		select 
			nrg_nuptk as nuptk, nrg_nrg as nrg
		from
			NRG.dbo.nrg_new
	) nrgtable
	where ptk_tersedia.nuptk = nrgtable.nuptk
	
	-- Clean up
	update ptk_tersedia set nrg = ''
	where nrg = '000000000000'

END
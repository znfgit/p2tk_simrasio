ALTER PROCEDURE [dbo].[sp-01-exec-all]
AS
BEGIN

exec [sp-02-create-ptk_tersedia];
exec [sp-03-create-rombel_sekolah_sd];
exec [sp-04-create-kg-sd];
exec [sp-05-create-rombel_sekolah_smp];
exec [sp-06-create-kg-smp];
exec [sp-07-create-guru-permatpel];

END
ALTER PROCEDURE [dbo].[sp_07_create_kg_smp]
AS
BEGIN

drop table kebutuhan_guru_smp;

with refjmp as 
( 
    select distinct
        mata_pelajaran_id, 
        nama as nama_matpel,
        kurikulum_id,
        jumlah_jam_maksimum
    from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui
        where nama NOT LIKE '%Agama%' 
        and (kurikulum_id = 7 or kurikulum_id = 8)
    --order by kurikulum_id
) 
select distinct
    --top 100
        convert(nvarchar(36), rombel_sekolah_smp.sekolah_id) + ':' + convert(nvarchar(10), refjmp.mata_pelajaran_id) as kebutuhan_guru_smp_id,
    rombel_sekolah_smp.sekolah_id, 
    rombel_sekolah_smp.nama as nama_sekolah, 
    rombel_sekolah_smp.kode_wilayah,
    kecamatan.nama as nama_kecamatan,
    kabkota.kode_wilayah as kode_wilayah_kabkota,
    kabkota.nama as nama_kabkota,
    propinsi.kode_wilayah as kode_wilayah_propinsi,
    propinsi.nama as nama_propinsi,

    -- refjmp.kurikulum_id,
    refjmp.mata_pelajaran_id, 
    refjmp.nama_matpel,
    
    -- Rombel KTSP --
    rombel_sekolah_smp.jml_rbl_ktsp_7,
    jam_ktsp_tk_7.jumlah_jam_maksimum as ref_jam_ktsp_7,
    rombel_sekolah_smp.jml_rbl_ktsp_7 * jam_ktsp_tk_7.jumlah_jam_maksimum as jam_ktsp_7,

    rombel_sekolah_smp.jml_rbl_ktsp_8,
    jam_ktsp_tk_8.jumlah_jam_maksimum as ref_jam_ktsp_8,
    rombel_sekolah_smp.jml_rbl_ktsp_8 * jam_ktsp_tk_8.jumlah_jam_maksimum as jam_ktsp_8,

    rombel_sekolah_smp.jml_rbl_ktsp_9,
    jam_ktsp_tk_9.jumlah_jam_maksimum as ref_jam_ktsp_9,
    rombel_sekolah_smp.jml_rbl_ktsp_9 * jam_ktsp_tk_9.jumlah_jam_maksimum as jam_ktsp_9,

    -- Rombel 2013 --
    rombel_sekolah_smp.jml_rbl_2013_7,
    jam_2013_tk_7.jumlah_jam_maksimum as ref_jam_2013_7,
    rombel_sekolah_smp.jml_rbl_2013_7 * jam_2013_tk_7.jumlah_jam_maksimum as jam_2013_7,

    rombel_sekolah_smp.jml_rbl_2013_8,
    jam_2013_tk_8.jumlah_jam_maksimum as ref_jam_2013_8,
    rombel_sekolah_smp.jml_rbl_2013_8 * jam_2013_tk_8.jumlah_jam_maksimum as jam_2013_8,

    rombel_sekolah_smp.jml_rbl_2013_9,
    jam_2013_tk_9.jumlah_jam_maksimum as ref_jam_2013_9,
    rombel_sekolah_smp.jml_rbl_2013_9 * jam_2013_tk_9.jumlah_jam_maksimum as jam_2013_9,

        
    0 as total_jam_dibutuhkan,
    
        0 as jumlah_ptk_ada_utk_matpel,
    0 as jumlah_ptk_honorer_s1_ada_utk_matpel,
    0 as jumlah_ptk_honorer_nons1_ada_utk_matpel,
    0 as jumlah_ptk_pns_nons1_ada_utk_matpel,
    0 as jumlah_ptk_pns_s1_ada_utk_matpel,
    0 as jumlah_ptk_nonpns_nons1_ada_utk_matpel,
    0 as jumlah_ptk_nonpns_s1_ada_utk_matpel,

        0 as jumlah_ptk_ada_utk_matpel_sert,
    0 as jumlah_ptk_honorer_s1_ada_utk_matpel_sert,
    0 as jumlah_ptk_honorer_nons1_ada_utk_matpel_sert,
    0 as jumlah_ptk_pns_nons1_ada_utk_matpel_sert,
    0 as jumlah_ptk_pns_s1_ada_utk_matpel_sert,
    0 as jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert,
    0 as jumlah_ptk_nonpns_s1_ada_utk_matpel_sert,

    --0 as jumlah_ptk_sertifikasi_s1_ada_utk_matpel,
    --0 as jumlah_ptk_sertifikasi_nons1_ada_utk_matpel,

    0 as jumlah_jam_ada_utk_matpel,
    0 as kelebihan_guru,
    0 as kekurangan_guru

into kebutuhan_guru_smp
from refjmp

cross join rombel_sekolah_smp
inner join Dapodik_2014.dbo.ref_mst_wilayah kecamatan on rombel_sekolah_smp.kode_wilayah = kecamatan.kode_wilayah
inner join Dapodik_2014.dbo.ref_mst_wilayah kabkota on kecamatan.mst_kode_wilayah = kabkota.kode_wilayah
inner join Dapodik_2014.dbo.ref_mst_wilayah propinsi on kabkota.mst_kode_wilayah = propinsi.kode_wilayah

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 7  ) jam_ktsp_tk_7 
    on jam_ktsp_tk_7.mata_pelajaran_id = refjmp.mata_pelajaran_id

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 7  ) jam_ktsp_tk_8 
    on jam_ktsp_tk_8.mata_pelajaran_id = refjmp.mata_pelajaran_id

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 7  ) jam_ktsp_tk_9 
    on jam_ktsp_tk_9.mata_pelajaran_id = refjmp.mata_pelajaran_id

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 8  ) jam_2013_tk_7 
    on jam_2013_tk_7.mata_pelajaran_id = refjmp.mata_pelajaran_id

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 8  ) jam_2013_tk_8 
    on jam_2013_tk_8.mata_pelajaran_id = refjmp.mata_pelajaran_id

left join ( select * from pendataan_2014.dbo.ref_matapelajaran_kurikulum_diakui where kurikulum_id = 8  ) jam_2013_tk_9 
    on jam_2013_tk_9.mata_pelajaran_id = refjmp.mata_pelajaran_id

order by sekolah_id, mata_pelajaran_id;

-- Set PK
alter table kebutuhan_guru_smp alter column kebutuhan_guru_smp_id nvarchar(47) NOT NULL;
alter table kebutuhan_guru_smp add constraint pk_kebutuhan_guru_smp_id PRIMARY KEY (kebutuhan_guru_smp_id);


-- Menghitung seluruh guru linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id; 


-- Menghitung seluruh guru honorer S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_honorer_s1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_s1 = 1 and is_honorer = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- ++ Tambahan ++ --
-- Menghitung seluruh guru honorer non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_honorer_nons1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_s1 = 0 and is_honorer = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- Menghitung seluruh guru pns non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_pns_nons1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 1 and is_s1 != 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- Menghitung seluruh guru pns S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_pns_s1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 1 and is_s1 = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;     

-- ++ Tambahan ++ --
-- Menghitung seluruh guru pns non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_nonpns_nons1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 0 and is_honorer = 0 and is_s1 != 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- ++ Tambahan ++ --
-- Menghitung seluruh guru pns S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_nonpns_s1_ada_utk_matpel = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 0 and is_honorer = 0  and is_s1 = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;     

----------------------------
----  SERTIFIKASI BRUH  ----
----------------------------

-- Menghitung seluruh guru honorer S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_honorer_s1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_s1 = 1 and is_honorer = 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- ++ Tambahan ++ --
-- Menghitung seluruh guru honorer non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_honorer_nons1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_s1 = 0 and is_honorer = 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- Menghitung seluruh guru pns non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_pns_nons1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 1 and is_s1 != 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- Menghitung seluruh guru pns S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_pns_s1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 1 and is_s1 = 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;     

-- ++ Tambahan ++ --
-- Menghitung seluruh guru pns non S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 0 and is_honorer = 0 and is_s1 != 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;   

-- ++ Tambahan ++ --
-- Menghitung seluruh guru pns S1 linier tersedia
update kebutuhan_guru_smp SET
    kebutuhan_guru_smp.jumlah_ptk_nonpns_s1_ada_utk_matpel_sert = ptk_ada_utk_matpel.jumlah_ptk_ada_utk_matpel
from (
    select 
        sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel, 
        count(*) as jumlah_ptk_ada_utk_matpel 
    from ptk_smp_ada
    where is_pns = 0 and is_honorer = 0  and is_s1 = 1 and is_sertifikasi = 1
    group by sekolah_id, nama_sekolah, mata_pelajaran_id, nama_matpel
        
) ptk_ada_utk_matpel 
    where kebutuhan_guru_smp.sekolah_id = ptk_ada_utk_matpel.sekolah_id 
    and kebutuhan_guru_smp.mata_pelajaran_id = ptk_ada_utk_matpel.mata_pelajaran_id;     

---------
---------
---------





-- Null kan --
update kebutuhan_guru_smp set jml_rbl_ktsp_7 = 0 where jml_rbl_ktsp_7 is null;
update kebutuhan_guru_smp set ref_jam_ktsp_7 = 0 where ref_jam_ktsp_7 is null;
update kebutuhan_guru_smp set jam_ktsp_7 = 0 where jam_ktsp_7 is null;

update kebutuhan_guru_smp set jml_rbl_ktsp_8 = 0 where jml_rbl_ktsp_8 is null;
update kebutuhan_guru_smp set ref_jam_ktsp_8 = 0 where ref_jam_ktsp_8 is null;
update kebutuhan_guru_smp set jam_ktsp_8 = 0 where jam_ktsp_8 is null;

update kebutuhan_guru_smp set jml_rbl_ktsp_9 = 0 where jml_rbl_ktsp_9 is null;
update kebutuhan_guru_smp set ref_jam_ktsp_9 = 0 where ref_jam_ktsp_9 is null;
update kebutuhan_guru_smp set jam_ktsp_9 = 0 where jam_ktsp_9 is null;

update kebutuhan_guru_smp set jml_rbl_2013_7 = 0 where jml_rbl_2013_7 is null;
update kebutuhan_guru_smp set ref_jam_2013_7 = 0 where ref_jam_2013_7 is null;
update kebutuhan_guru_smp set jam_2013_7 = 0 where jam_2013_7 is null;

update kebutuhan_guru_smp set jml_rbl_2013_8 = 0 where jml_rbl_2013_8 is null;
update kebutuhan_guru_smp set ref_jam_2013_8 = 0 where ref_jam_2013_8 is null;
update kebutuhan_guru_smp set jam_2013_8 = 0 where jam_2013_8 is null;

update kebutuhan_guru_smp set jml_rbl_2013_9 = 0 where jml_rbl_2013_9 is null;
update kebutuhan_guru_smp set ref_jam_2013_9 = 0 where ref_jam_2013_9 is null;
update kebutuhan_guru_smp set jam_2013_9 = 0 where jam_2013_9 is null;
    
update kebutuhan_guru_smp set jumlah_ptk_ada_utk_matpel = 0 where jumlah_ptk_ada_utk_matpel is null;
--update kebutuhan_guru_smp set total_jam_dibutuhkan = 0 where total_jam_dibutuhkan is null;

-- Update jumlah --
update kebutuhan_guru_smp 
set total_jam_dibutuhkan = jam_ktsp_7 + jam_ktsp_8 + jam_ktsp_9 + 
                            jam_2013_7 + jam_2013_8 + jam_2013_9;

update kebutuhan_guru_smp  set 
    jumlah_jam_ada_utk_matpel = jumlah_ptk_ada_utk_matpel * 24,
    kelebihan_guru = (jumlah_ptk_ada_utk_matpel * 24) - total_jam_dibutuhkan,
    kekurangan_guru = total_jam_dibutuhkan - (jumlah_ptk_ada_utk_matpel * 24);

update kebutuhan_guru_smp set
    kelebihan_guru = (abs(kelebihan_guru) + kelebihan_guru)/2 ,
    kekurangan_guru = (abs(kekurangan_guru) + kekurangan_guru)/2;

END
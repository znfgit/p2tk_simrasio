ALTER PROCEDURE [dbo].[sp_06_create_rombel_sekolah_smp]
AS
BEGIN
  
    declare @maxSiswaPerRombelSMP int;
    set @maxSiswaPerRombelSMP = 36;

    declare @semester int;
    set @semester = 20141;

    drop table rombel_sekolah_smp;

    select
            --top 10
            sekolah.sekolah_id, 
            sekolah.nama, 
            sekolah.bentuk_pendidikan_id,
            sekolah.kode_wilayah,
            --20131 as semester_id,
            @semester as semester_id,

            rbl_ktsp_7.jml_rbl_ktsp_7,
            rbl_ktsp_8.jml_rbl_ktsp_8,
            rbl_ktsp_9.jml_rbl_ktsp_9,
            
            rbl_2013_7.jml_rbl_2013_7,
            rbl_2013_8.jml_rbl_2013_8,
            rbl_2013_9.jml_rbl_2013_9,

            rbl_ktsp_7.jml_siswarbl_ktsp_7,
            rbl_ktsp_8.jml_siswarbl_ktsp_8,
            rbl_ktsp_9.jml_siswarbl_ktsp_9,
            
            rbl_2013_7.jml_siswarbl_2013_7,
            rbl_2013_8.jml_siswarbl_2013_8,
            rbl_2013_9.jml_siswarbl_2013_9


    into rombel_sekolah_smp
    from Dapodik_2014.dbo.sekolah sekolah

    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_7, sum(jumlah_siswa) as jml_siswarbl_ktsp_7  from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 7 and kurikulum_id = 7 group by sekolah_id, semester_id) rbl_ktsp_7 
            on rbl_ktsp_7.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_7.semester_id = @semester)
    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_8, sum(jumlah_siswa) as jml_siswarbl_ktsp_8 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 8 and kurikulum_id = 7 group by sekolah_id, semester_id) rbl_ktsp_8 
            on rbl_ktsp_8.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_8.semester_id = @semester)
    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_ktsp_9, sum(jumlah_siswa) as jml_siswarbl_ktsp_9 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 9 and kurikulum_id = 7 group by sekolah_id, semester_id) rbl_ktsp_9 
            on rbl_ktsp_9.sekolah_id = sekolah.sekolah_id and (rbl_ktsp_9.semester_id = @semester)

    -- 2013 --
    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_2013_7, sum(jumlah_siswa) as jml_siswarbl_2013_7 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 7 and kurikulum_id = 8 group by sekolah_id, semester_id) rbl_2013_7 
            on rbl_2013_7.sekolah_id = sekolah.sekolah_id and (rbl_2013_7.semester_id = @semester)
    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_2013_8, sum(jumlah_siswa) as jml_siswarbl_2013_8 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 8 and kurikulum_id = 8 group by sekolah_id, semester_id) rbl_2013_8 
            on rbl_2013_8.sekolah_id = sekolah.sekolah_id and (rbl_2013_8.semester_id = @semester)
    left join 
            ( select sekolah_id, semester_id, count(*) as jml_rbl_2013_9, sum(jumlah_siswa) as jml_siswarbl_2013_9 from pendataan_2014.dbo.t_rombongan_belajar where tingkat_pendidikan_id = 9 and kurikulum_id = 8 group by sekolah_id, semester_id) rbl_2013_9 
            on rbl_2013_9.sekolah_id = sekolah.sekolah_id and (rbl_2013_9.semester_id = @semester)

    where (sekolah.bentuk_pendidikan_id = 6 or sekolah.bentuk_pendidikan_id = 8);
        
        -- and sekolah.status_sekolah = 1; BERLAKUKAN PERHITUNGAN NEGERI DAN SWASTA
    
    -- Add Primary Key
    alter table rombel_sekolah_smp add constraint pk_smp_sekolah_id PRIMARY KEY (sekolah_id);

    update rombel_sekolah_smp set jml_rbl_ktsp_7 = 0 where jml_rbl_ktsp_7 is null;
    update rombel_sekolah_smp set jml_rbl_ktsp_8 = 0 where jml_rbl_ktsp_8 is null;
    update rombel_sekolah_smp set jml_rbl_ktsp_9 = 0 where jml_rbl_ktsp_9 is null;

    update rombel_sekolah_smp set jml_rbl_2013_7 = 0 where jml_rbl_2013_7 is null;
    update rombel_sekolah_smp set jml_rbl_2013_8 = 0 where jml_rbl_2013_8 is null;
    update rombel_sekolah_smp set jml_rbl_2013_9 = 0 where jml_rbl_2013_9 is null;

--select top 10 sekolah_id, semester_id, count(*) as jml_rbl_ktsp_7 from pendataan_2014.dbo.t_rombongan_belajar group by sekolah_id, semester_id
END
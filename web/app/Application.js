/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.ns('SimptkRasio');

Ext.util.Format.thousandSeparator = ".";
Ext.util.Format.decimalSeparator = ",";

// SimptkRasio.kode_wilayah = "000000";
// SimptkRasio.nama_wilayah = "Indonesia";
// SimptkRasio.mst_kode_wilayah = "";
SimptkRasio.last_updated = "-";
SimptkRasio.mata_pelajaran_id = 0;

SimptkRasio.sekolah_id ="";
SimptkRasio.nama_sekolah = "";

SimptkRasio.sekolah_selected = "";

//Login stuff

var globalNamaWilayah = "";

// Override shit

Ext.grid.feature.Summary.override({
    createSummaryRecord: function(view) {
        var columns = view.headerCt.getVisibleGridColumns(),
            summaryRecord = this.summaryRecord,
            colCount = columns.length, i, column,
            dataIndex, summaryValue, Model, modelData;
        
        if (!summaryRecord) {
            Model = view.store.getModel();
            modelData = {};
            modelData[Model.idProperty] = view.id + '-summary-record';
            summaryRecord = this.summaryRecord = new Model(modelData);
        }

        // Set the summary field values
        summaryRecord.beginEdit();
        if (this.remoteRoot) {
            if (view.store.proxy.reader.rawData) {
                summaryRecord.set(view.store.proxy.reader.rawData.summaryData); // hardcoded "summaryData"
            }
        } else {
            for (i = 0; i < colCount; i++) {
                column = columns[i];

                // In summary records, if there's no dataIndex, then the value in regular rows must come from a renderer.
                // We set the data value in using the column ID.
                dataIndex = column.dataIndex || column.id;

                // We need to capture this value because it could get overwritten when setting on the model if there
                // is a convert() method on the model.
                summaryValue = this.getSummary(view.store, column.summaryType, dataIndex);
                summaryRecord.set(dataIndex, summaryValue);

                // Capture the columnId:value for the summaryRenderer in the summaryData object.
                this.setSummaryData(summaryRecord, column.id, summaryValue);
            }
        }
        summaryRecord.endEdit(true);
        // It's not dirty
        summaryRecord.commit(true);
        summaryRecord.isSummary = true;

        return summaryRecord;
    }
});

Ext.define('SimptkRasio.Application', {
    extend: 'Ext.app.Application',
    
    name: 'SimptkRasio',

    views: [
        // TODO: add views here
    ],

    controllers: [
        'Root'
        // TODO: add controllers here
    ],

    stores: [
        // TODO: add stores here
    ],
    
    launch: function () {
        
        // TODO - Launch the application
        Ext.setGlyphFontFamily("FontAwesome");
        Ext.setGlyphFontFamily("font-fileformats-icons");
        Ext.setGlyphFontFamily("font-icomoon");

        // Loader for ux & lib
        Ext.Loader.setPath('Ext.ux', 'app/ux');
        
        // Setup Ext Direct
        //Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);

        // Initialize tooltip
        Ext.tip.QuickTipManager.init();
        
        Ext.Ajax.request({
            waitMsg: 'Menyimpan...',                        
            url: '/cekLogin',                     
            method: 'GET',
            failure:function(response,options){                        
                Ext.Msg.alert('Warning','Response : ' + response );
            },
            success: function(response,options){
                
                var json = Ext.util.JSON.decode(response.responseText);
                if (json.success == true) {                             
                    
                    // console.log(json);

                    //Ext.Msg.alert('OK', json.message);
                    SimptkRasio.username = json.username;
                    SimptkRasio.last_updated = json.last_updated;

                    SimptkRasio.user_kode_wilayah = json.kode_wilayah;
                    SimptkRasio.user_nama_wilayah = json.nama_wilayah;
                    SimptkRasio.user_mst_kode_wilayah = json.mst_kode_wilayah;
                    SimptkRasio.user_skup = json.skup;

                    SimptkRasio.kode_wilayah = json.kode_wilayah;
                    SimptkRasio.nama_wilayah = json.nama_wilayah;
                    SimptkRasio.mst_kode_wilayah = json.mst_kode_wilayah;
                    SimptkRasio.skup = json.skup;

                    console.log(SimptkRasio);
                    
                    Ext.getCmp('info-app').update(json.nama_wilayah);
                    Ext.fly('last_updated').update('Tgl Penarikan Data: ' + json.last_updated);
                    // console.log(SimptkRasio);
                    // console.log('SimptkRasio.skup = ' + SimptkRasio.skup);
                    // console.log('json.skup = ' + json.skup);
                    // console.log(SimptkRasio.kode_wilayah);
                    // console.log(SimptkRasio.nama_wilayah);
                    // console.log(SimptkRasio.mst_kode_wilayah);

                } else if (json.success == false){
                    
                    //Ext.Msg.alert('Error', json.message);
                    // var debug = 0;
                    // var win = new Ext.widget({
                    //     xtype: 'window_login'
                    // });
                    // if (!debug) {
                    //     win.show();
                    // }
                }
        
            }
        });

    }
});

Ext.define('SimptkRasio.view.modules.SimulasiKgSmpWilayah', {
    extend: 'Ext.Panel',
    xtype: 'simulasi_kg_smp_wilayah',
    layout: 'fit', 
    requires: [
        'SimptkRasio.view.grids.GridSimulasiKg',
        'SimptkRasio.view.components.WindowPrint'
    ],

    initComponent: function() {

        me = this;
        
        var filterForm = {
            xtype: 'panel',
            layout: 'anchor',
            title: 'Simulasi Kebutuhan Guru Per Wilayah',
            items: {
                xtype: 'form_filter_simulasi_kg',
                anchor: '50%'
            },
            flex: 2
        };

        var myGrid = {
            xtype: 'grid_simulasi_kg',
            flex: 5,
            purpose: 'formasi',
            per: 'matpel',
            jenjang: 'smp',
            listeners: {
                changewilayah: function(record) {
                    // Do shit
                }
            }
        };

        me.items = {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            items: [filterForm, myGrid]
        }
        
        this.callParent();

    }
});
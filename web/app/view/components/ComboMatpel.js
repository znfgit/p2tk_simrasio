Ext.define('SimptkRasio.view.components.ComboMatpel', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'combo_matpel',

    emptyText: 'Ketik untuk mencari matpel',
    displayField: 'nama',
    valueField: 'mata_pelajaran_id',
    hiddenName: 'mata_pelajaran_id',
    typeAhead: false,
    hideLabel: true,
    //hideTrigger:true,
    anchor: '100%',

    listeners: {
        select: function(combo, record){

            var rec = combo.findRecordByValue(combo.getValue());
            // Ext.getCmp('info-app').update(rec.data.nama);
            // SimptkRasio.kode_wilayah = rec.data.kode_wilayah;
            // SimptkRasio.nama_wilayah = rec.data.nama;
            // SimptkRasio.skup = rec.data.id_level_wilayah;
            // SimptkRasio.mst_kode_wilayah = rec.data.mst_kode_wilayah;
            SimptkRasio.mata_pelajaran_id = rec.data.mata_pelajaran_id;
            SimptkRasio.nama_matpel = rec.data.nama_matpel;

        }
    },

    initComponent: function() {
        
        var me = this;
        
        var url = '';

        if (me.jenjang == 'smp') {
            url = '/CariMatpelComboSmp';
        } else {
            url = '/CariMatpelCombo';
        }

        me.store = Ext.create('Ext.data.Store', {
            fields: ['mata_pelajaran_id', 'nama'],
            idProperty: 'mata_pelajaran_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: url,
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        me.store.on('load', function(store){
            
            if (!me.getValue() && me.initialValue) {
                
                setTimeout(function(){
                    me.setValue(me.initialValue);
                }, 1000);
            }

        });

        me.getStore().load();

        this.callParent();
    }

});
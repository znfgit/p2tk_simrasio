Ext.define('SimptkRasio.view.components.ComboWilayah', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'combo_wilayah',

    emptyText: 'Ketik untuk mencari wilayah',
    displayField: 'nama',
    valueField: 'kode_wilayah',
    hiddenName: 'kode_wilayah',
    typeAhead: false,
    hideLabel: true,
    hideTrigger:true,
    anchor: '100%',

    listeners: {
        select: function(combo, record){

            var rec = combo.findRecordByValue(combo.getValue());
            Ext.getCmp('info-app').update(rec.data.nama);
            SimptkRasio.kode_wilayah = rec.data.kode_wilayah;
            SimptkRasio.nama_wilayah = rec.data.nama;
            SimptkRasio.skup = rec.data.id_level_wilayah;
            SimptkRasio.mst_kode_wilayah = rec.data.mst_kode_wilayah;
            
        }
    },

    initComponent: function() {
        
        var me = this;
        
        me.store = Ext.create('Ext.data.Store', {
            fields: ['kode_wilayah', 'nama', 'id_level_wilayah', 'mst_kode_wilayah'],
            idProperty: 'kode_wilayah',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/CariWilayahCombo',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        this.callParent();
    }

});
Ext.define('SimptkRasio.view.components.WindowNominasiMutasiPenempatan', {
    extend: 'Ext.window.Window',
    xtype: 'window_nominasi_mutasi_penempatan',
    // width: 300,
    // height: 200,
    anchor: '100%',
    emptyText: 'Tidak ada data',
    selModel: new Ext.selection.RowModel(),
    // store: wilayahStore,
    hideHeaders: true,
    kodeWilayah: '000000 ',
    parentWilayah: '',
    kodeWilayahStr: '<b>Indonesia</b>',
    parentWilayahStr: '',


    initComponent: function() {
        
        var me = this;

        var listStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'ptk_id', type: 'string'  } ,
                { name: 'nama_ptk', type: 'string'  } , 
                { name: 'mata_pelajaran_id', type: 'string'  } ,
                { name: 'nama_matpel', type: 'string'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'is_pns', type: 'int'  } ,
                { name: 'is_honorer', type: 'int'  } ,
                { name: 'is_sertifikasi', type: 'int'  } ,
                { name: 'is_s1', type: 'int'  } ,
                { name: 'jjm', type: 'int'  } 
            ],
            idProperty: 'ptk_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/DaftarNominasiKeluar',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });
        
        listStore.load({
            params: {
                sekolah_id: me.sekolah_id,
                mata_pelajaran_id: me.mata_pelajaran_id
                // sekolah_id: '20659C94-2BF5-E011-906F-335FFCD534E6',
                // mata_pelajaran_id: '4020'
            }
        });

        var listGrid = new Ext.grid.GridPanel({
            // flex: 3,
            width: 450,
            padding: '20px 0 0 0',
            anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel({
                mode: 'MULTI'
            }),
            store: listStore,
            tbar: [{
                text: 'Reload',
                glyph: '61666@font-awesome'
            },'->',{
                text: 'Keluarkan',
                glyph: '61537@font-awesome',
                listeners: {
                    click: function(){

                        var selections = listGrid.getSelectionModel().getSelection();
                        if (!selections) {
                            Ext.Msg.alert('Error', 'Mohon pilih minimal satu guru untuk dikeluarkan');
                            return false;
                        }
                        
                        var count = 0;
                        jsonData = "[";

                        for (i = 0; i < selections.length; i++) {
                            var record = selections[i];
                            jsonData += Ext.JSON.encode(record.data) + ",";
                            count++;
                        }

                        jsonData = jsonData.substring(0,jsonData.length-1) + "]";

                        Ext.Ajax.request({
                            waitMsg : 'Menyimpan...',
                            url     : '/SaveNominasi',
                            method  : 'POST',
                            params  : {
                                data: jsonData
                            },
                            failure: function(response, options){
                                Ext.Msg.alert('Warning','Ada Kesalahan dalam pengisian data. Mohon dicek lagi');
                            },
                            success: function(response, options){
                                var json = Ext.JSON.decode(response.responseText);
                                
                                if(json.success){
                                    
                                    Ext.Msg.alert('OK', json.affected + ' data tersimpan.');
                                    
                                    setTimeout(function(){
                                        
                                        listStore.reload();
                                        nominasiStore.reload();

                                    },1000);

                                } else if(json.success == false) {
                                    
                                    Ext.Msg.alert('Error', json.message);

                                }
                            }
                        });

                        //var kode_wilayah = me.down('grid_wilayah').kodeWilayah;

                    }
                }
            }],
            columns:[{
                header: 'No',
                width: 45,
                sortable: true, 
                hidden: true,
                dataIndex: 'ptk_id'
            },{                
                header: 'Nama Guru',
                width: 150,
                sortable: true, 
                dataIndex: 'nama_ptk'
            },{                
                header: 'Sert',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_sertifikasi',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'S1',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_s1',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'PNS',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_pns',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'JJM',
                width: 70,
                align: 'right',
                sortable: true, 
                dataIndex: 'jjm'
            }]
        });

        var nominasiStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'ptk_id', type: 'string'  } ,
                { name: 'nama_ptk', type: 'string'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'is_pns', type: 'int'  } ,
                { name: 'is_honorer', type: 'int'  } ,
                { name: 'is_sertifikasi', type: 'int'  } ,
                { name: 'is_s1', type: 'int'  } ,
                { name: 'jjm', type: 'int' } 
            ],
            idProperty: 'ptk_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/DaftarPenempatan',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });

        var nominasiGrid = new Ext.grid.GridPanel({
            // flex: 2,
            width: 450,
            padding: '20px 0 0 0',
            anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel({
                mode: 'MULTI'
            }),
            store: nominasiStore,
            
            tbar: [{
                text: 'Batalkan',
                glyph: '61536@font-awesome',
                listeners: {
                    
                    click: function(){

                        var selections = nominasiGrid.getSelectionModel().getSelection();
                        if (!selections) {
                            Ext.Msg.alert('Error', 'Mohon pilih minimal satu guru untuk dikeluarkan');
                            return false;
                        }
                        
                        var count = 0;
                        jsonData = "[";

                        for (i = 0; i < selections.length; i++) {
                            var record = selections[i];
                            jsonData += Ext.JSON.encode(record.data) + ",";
                            count++;
                        }

                        jsonData = jsonData.substring(0,jsonData.length-1) + "]";

                        Ext.Ajax.request({
                            waitMsg : 'Menyimpan...',
                            url     : '/DeleteNominasi',
                            method  : 'POST',
                            params  : {
                                data: jsonData
                            },
                            failure: function(response, options){
                                Ext.Msg.alert('Warning','Ada Kesalahan dalam pengisian data. Mohon dicek lagi');
                            },
                            success: function(response, options){
                                var json = Ext.JSON.decode(response.responseText);
                                
                                if(json.success){
                                    
                                    Ext.Msg.alert('OK', json.affected + ' data tersimpan.');
                                    
                                    setTimeout(function(){
                                        
                                        listStore.reload();
                                        nominasiStore.reload();

                                    },100);

                                } else if(json.success == false) {
                                    
                                    Ext.Msg.alert('Error', json.message);

                                }
                            }
                        });

                        //var kode_wilayah = me.down('grid_wilayah').kodeWilayah;

                    }
                }
            },'->',{
                text: 'Reload',
                glyph: '61666@font-awesome'
            }],
            columns: [{
                header: 'No',
                width: 45,
                sortable: true, 
                hidden: true,
                dataIndex: 'ptk_id'
            },{                
                header: 'Nama Guru',
                width: 150,
                sortable: true, 
                dataIndex: 'nama_ptk'
            },{                
                header: 'Sert',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_sertifikasi',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'S1',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_s1',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'PNS',
                width: 60,
                align: 'right',
                sortable: true, 
                dataIndex: 'is_pns',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{                
                header: 'JJM',
                width: 70,
                align: 'right',
                sortable: true, 
                dataIndex: 'jjm'
            }]
        });

        nominasiStore.load({
            params: {
                sekolah_id: me.sekolah_id,
                mata_pelajaran_id: me.mata_pelajaran_id
            }
        });

        me.items = {
            xtype: 'container',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [listGrid, nominasiGrid]
        };

        this.callParent();
    }

});

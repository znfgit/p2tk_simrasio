Ext.define('SimptkRasio.view.components.GridWilayah', {
    extend: 'Ext.grid.GridPanel',
    xtype: 'grid_wilayah',
    // width: 300,
    // height: 200,
    anchor: '100%',
    emptyText: '-',
    selModel: new Ext.selection.RowModel(),
    // store: wilayahStore,
    hideHeaders: true,
    kodeWilayah: '000000 ',
    parentWilayah: '',
    kodeWilayahStr: '',
    levelWilayah: '',
    //kodeWilayahStr: '<b>'+ SimptkRasio.nama_wilayah +'</b>',
    parentWilayahStr: '',

    initComponent: function() {
        
        var me = this;

        // console.log(SimptkRasio.kode_wilayah);
        // console.log(SimptkRasio.nama_wilayah);
        // console.log(SimptkRasio.mst_kode_wilayah);

        me.kodeWilayah = SimptkRasio.kode_wilayah;
        me.parentWilayah = SimptkRasio.mst_kode_wilayah;
        me.kodeWilayahStr = '<b>'+ SimptkRasio.nama_wilayah +'</b>';

        me.store = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'kode_wilayah', type: 'string'  } ,
                { name: 'mst_kode_wilayah', type: 'string'  } ,
                { name: 'id_level_wilayah', type: 'int'  } ,
                { name: 'nama', type: 'string'  }
            ],
            idProperty: 'kode_wilayah',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/GridWilayah',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });
        
        me.store.load({
            params: {
                jenjang: me.jenjang,
                status_sekolah: me.status_sekolah,
                kode_wilayah: SimptkRasio.kode_wilayah
            }
        });

        me.tbar = [{
            xtype: 'tbtext',
            text: me.kodeWilayahStr
        },{
            xtype: 'combo_wilayah',
            hidden: true,
            listeners: {
                blur: function(cmb) {
                    cmb.setHidden(true);
                    
                    var tbtext = cmb.up('toolbar').down('tbtext');
                    tbtext.show();
                    
                    var btn = cmb.up('toolbar').down('button[itemId="cari"]');
                    btn.setHidden(false);
                },
                select: function(cmb, recs) {
                    var rec = recs[0];

                    cmb.setHidden(true);
                    
                    // Bring back text
                    var tbtext = cmb.up('toolbar').down('tbtext');
                    tbtext.setText(rec.get('nama'));
                    tbtext.show();
                    
                    // Bring back 'cari'
                    var btn = cmb.up('toolbar').down('button[itemId="cari"]');
                    btn.show();
                }
            }
        },'->',{
            //text: 'Indonesia',
        //     itemId: 'cari',
        //     glyph: '61442@font-awesome',
        //     listeners: {
        //         click: function(btn) {
        //             btn.setHidden(true);
                    
        //             var tbtext = btn.up('toolbar').down('tbtext');
        //             tbtext.setHidden(true);

        //             var cbw = btn.up('toolbar').down('combo_wilayah');
        //             cbw.show();
        //             cbw.focus();

        //         }
        //     }
        // },{
            //text: 'Indonesia',
            itemId: 'up',
            glyph: '61714@font-awesome',
            listeners: {
                click: function(btn) {

                    // Misal Kota bandung
                    // Cari nama parentnya dulu
                    if (me.parentWilayah == SimptkRasio.user_mst_kode_wilayah)
                        return;
                    
                    Ext.Ajax.request({
                        url: '/SearchWilayah/' + me.parentWilayah,

                        success: function(response){
                            var text = response.responseText;
                            var res = Ext.JSON.decode(response.responseText);

                            me.kodeWilayah = res.kode_wilayah;
                            me.levelWilayah = res.id_level_wilayah;
                            me.kodeWilayahStr = res.nama;
                            me.parentWilayah = res.mst_kode_wilayah;

                            var tbtext = me.down('toolbar').down('tbtext');
                            tbtext.setText("<b>"+ me.kodeWilayahStr.substr(0,30) + "</b>");

                            me.getStore().reload({
                                params: { 
                                    jenjang: me.jenjang,
                                    status_sekolah: me.status_sekolah,
                                    kode_wilayah: me.kodeWilayah
                                }
                            });
                        }
                    });
                }
            }

        }];

        me.columns = [{
            header: 'No',
            width: 45,
            sortable: true, 
            hidden: true,
            dataIndex: 'kode_wilayah'
        },{                
            header: 'Nama Wilayah',
            width: 380,
            sortable: true, 
            dataIndex: 'nama'
        }];


        me.on('itemdblclick', function(view, record, item, index, e, eOpts){

            var kode_wilayah = record.data.kode_wilayah;
            var level_wilayah = record.data.id_level_wilayah;
            var parent_kode_wilayah = record.data.mst_kode_wilayah;
            var kode_wilayah_str = record.data.nama;

            //console.log('kode_wilayah: ' + kode_wilayah);
            //console.log('parent_kode_wilayah: ' + parent_kode_wilayah);

            // Misal posisi di jabar
            // Misal Diklik Kota bandung
            me.kodeWilayah = kode_wilayah;
            me.levelWilayah = level_wilayah;                            
            me.kodeWilayahStr = kode_wilayah_str;       // Kota bandung
            me.parentWilayah = parent_kode_wilayah;     // Jabar

            var tbtext = me.down('toolbar').down('tbtext');
            //tbtext.setText(me.kodeWilayahStr);
            tbtext.setText("<b>"+ me.kodeWilayahStr.substr(0,30) + "</b>");

            me.getStore().reload({
                params: {
                    jenjang: me.jenjang,
                    status_sekolah: me.status_sekolah,
                    kode_wilayah: kode_wilayah
                }
            });


        });


        this.callParent();
    }

});
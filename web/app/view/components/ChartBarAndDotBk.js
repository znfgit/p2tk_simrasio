Ext.define('SimptkRasio.view.components.ChartBarAndDotBk', {
    extend: 'Ext.Panel',
    // extend: 'Ext.chart.CartesianChart',
    xtype: 'chart_bar_and_dot_bk',
    initComponent: function() {
        
        var me = this;

        me.items = {
            xtype: 'cartesian',
            // title: me.title,
            style: 'background: #fff;',
            legend: {
                docked: 'right'
            },
            insetPadding: {
                top: 30,
                left: 30,
                right: 30,
                bottom: 20
            },
            width: '100%',
            height: 310,
            store: me.store,
            axes: [{
                type: 'numeric',
                position: 'left',
                grid: true,
                fields: [
                    'jumlah_gurubk_minimal',
                    'jumlah_gurubk_maksimal',
                    'jumlah_gurubk'
                ],
                //renderer: function (v) { return v.toFixed(v < 10 ? 1: 0) + '%'; },
                label: {
                    fontWeight: 300
                },
                renderer: function (v) { return Ext.util.Format.number(v, '0,000'); },
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['nama_rincian'],
                label: {
                    // fontFamily: "Raleway",
                    fontWeight: 300,
                    rotate: {
                        degrees: -27
                    }
                }
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 'Jml Guru BK' ],
                xField: 'nama_rincian',
                yField: [
                    'jumlah_gurubk'
                ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: '#000',
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var browser = item.series.title[item.series.yField.indexOf(item.yField)];
                        this.setTitle(browser + ' for ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.yField) + '%');
                    }
                }
            },{
                type: 'scatter',
                axis: 'left',
                title: '<b>KG BK Minimal</b>',
                xField: 'nama_rincian',
                yField: 'jumlah_gurubk_minimal',
                marker: {
                    radius: 4
                },
                highlight: {
                    fill: '#000',
                    radius: 3,
                    'stroke-width': 2,
                    stroke: '#ccc'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var title = item.series.title;
                        this.setTitle(title + ' untuk ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.series.yField) );
                    }
                }
            },{
                type: 'scatter',
                axis: 'left',
                title: '<b>KG BK Maksimal</b>',
                xField: 'nama_rincian',
                yField: 'jumlah_gurubk_maksimal',
                marker: {
                    radius: 4
                },
                highlight: {
                    fill: '#000',
                    radius: 3,
                    'stroke-width': 2,
                    stroke: '#ccc'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var title = item.series.title;
                        this.setTitle(title + ' untuk ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.series.yField) );
                    }
                }
            }]
        };
       
        this.callParent();
    }

});
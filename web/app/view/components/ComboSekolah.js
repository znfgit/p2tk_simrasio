Ext.define('SimptkRasio.view.components.ComboSekolah', {
    extend: 'Ext.form.field.ComboBox',
    xtype: 'combo_sekolah',

    emptyText: 'Ketik untuk mencari sekolah',
    displayField: 'nama',
    valueField: 'sekolah_id',
    hiddenName: 'sekolah_id',
    typeAhead: false,
    hideLabel: true,
    //hideTrigger:true,
    anchor: '100%',

    listeners: {
        select: function(combo, record){

            var rec = combo.findRecordByValue(combo.getValue());
            // Ext.getCmp('info-app').update(rec.data.nama);
            // SimptkRasio.kode_wilayah = rec.data.kode_wilayah;
            // SimptkRasio.nama_wilayah = rec.data.nama;
            // SimptkRasio.skup = rec.data.id_level_wilayah;
            // SimptkRasio.mst_kode_wilayah = rec.data.mst_kode_wilayah;
            SimptkRasio.sekolah_id = rec.data.sekolah_id;
            SimptkRasio.nama_sekolah = rec.data.nama_sekolah;

        }
    },

    initComponent: function() {
        
        var me = this;
        
        var url = '';

        if (this.jenjang == 'smp') {
            url = '/CariSekolahComboSmp';
        } else {
            url = '/CariSekolahCombo';
        }

        me.store = Ext.create('Ext.data.Store', {
            fields: ['sekolah_id', 'nama'],
            idProperty: 'sekolah_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: url,
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        me.store.on('load', function(store){

            me.setValue(SimptkRasio.sekolah_id);
            
        });

        this.callParent();
    }

});
Ext.define('SimptkRasio.view.components.WindowListPtk', {
    extend: 'Ext.window.Window',
    xtype: 'window_list_ptk',
    // width: 300,
    // height: 200,
    anchor: '100%',
    layout: 'fit',
    emptyText: 'Tidak ada data',
    selModel: new Ext.selection.RowModel(),
    // store: wilayahStore,
    hideHeaders: true,
    kodeWilayah: '000000 ',
    parentWilayah: '',
    kodeWilayahStr: '<b>Indonesia</b>',
    parentWilayahStr: '',


    initComponent: function() {
        
        var me = this;

        var listStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'ptk_id', type: 'string'  },
                { name: 'nama_ptk', type: 'string'  },
                { name: 'tgl_lahir', type: 'date',  dateFormat: 'Y-m-d H:i:s'  },
                { name: 'tgl_pensiun', type: 'date',  dateFormat: 'Y-m-d H:i:s'  },
                { name: 'nuptk', type: 'string'  },
                { name: 'nrg', type: 'string'  },
                { name: 'mata_pelajaran_id', type: 'string'  },
                { name: 'nama_matpel', type: 'string'  },
                { name: 'sekolah_id', type: 'string'  },
                { name: 'nama', type: 'string'  },
                { name: 'is_pns', type: 'int'  },
                { name: 'is_honorer', type: 'int'  },
                { name: 'is_sertifikasi', type: 'int'  },
                { name: 'is_s1', type: 'int'  },
                { name: 'jjm', type: 'int'  },
                { name: 'nama_tugas_tambahan', type: 'string'  },
                { name: 'induk_jjm_diampu', type: 'int'  },
                { name: 'induk_jjm_linier', type: 'int'  },
                { name: 'negeri_jjm_diampu', type: 'int'  },
                { name: 'negeri_jjm_linier', type: 'int'  },
                { name: 'negeri_jumlah', type: 'int'  },
                { name: 'swasta_jjm_diampu', type: 'int'  },
                { name: 'swasta_jjm_linier', type: 'int'  },
                { name: 'swasta_jumlah', type: 'int'  }
            ],
            idProperty: 'ptk_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/ListPtkSekolah',                
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });
        
        listStore.on('beforeload', function(store){
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: me.bentuk_pendidikan_id,
                level_wilayah: me.level_wilayah,
                sekolah_id: me.sekolah_id,
                status_sekolah: me.status_sekolah,
                mata_pelajaran_id: me.mata_pelajaran_id,
                guru_bk: me.guru_bk,
                purpose: me.purpose
            });

        });
        
        listStore.load();

        var listGrid = new Ext.grid.GridPanel({
            // flex: 3,
            //padding: '20px 0 0 0',
            //anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel({
                mode: 'MULTI'
            }),
            viewConfig: {
                enableTextSelection: true
            },
            store: listStore,
            tbar: [{
                text: 'Info PTK',
                glyph: '61537@font-awesome',
                handler: function(e){
                    
                    var selections = listGrid.getSelectionModel().getSelection();
                    if (!selections) {
                        Ext.Msg.alert('Error', 'Mohon pilih guru yang akan dilihat Info PTK-nya.');
                        return false;
                    }
                    var nuptk = selections[0].data.nuptk;
                    var nrg = selections[0].data.nrg;
                    var which = (nrg !== '') ? nrg : nuptk;

                    if (which !== '') {
                        var winInfoPtk = Ext.create('widget.window', {
                            title: 'Info PTK',
                            height: 580,
                            width: 1100,
                            modal: true,
                            layout : 'fit',
                            items : [{
                                xtype : "component",
                                    autoEl : {
                                    tag : "iframe",
                                    src : "/InfoPtk/" + which
                                }
                            }]
                        });
                        winInfoPtk.show();
                    } else {
                        Ext.Msg.alert('Error', 'PTK ybs belum memiliki NRG maupun NUPTK');
                    }
                }
            },'->',{
                text: 'Reload',
                glyph: '61666@font-awesome',
                handler: function(e){
                    listStore.reload();
                }
            }],
            
            bbar: Ext.create('Ext.PagingToolbar', {
                store: listStore,
                displayInfo: true,
                displayMsg: 'Menampilkan data {0} - {1}',
                emptyMsg: "Tidak ada data"
            }),

            lockedGridConfig: {
                forceFit: true
            },

            lockedViewConfig: {
                scroll: 'horizontal'
            },

            columns:[{
                header: 'No',
                width: 60,
                sortable: true,
                hidden: true,
                locked: true,
                dataIndex: 'ptk_id'
            },{
                xtype: 'rownumberer',
                width: 60,
                locked: true
            },{
                header: 'Nama Guru',
                width: 200,
                sortable: true,
                locked: true,
                dataIndex: 'nama_ptk'
            },{
                header: 'NUPTK',
                width: 150,
                sortable: true,
                dataIndex: 'nuptk'
            },{
                header: 'NRG',
                width: 150,
                sortable: true,
                dataIndex: 'nrg'
            },{
                header: 'Tgl Lahir',
                width: 150,
                sortable: true,
                renderer : Ext.util.Format.dateRenderer('Y-m-d'),
                dataIndex: 'tgl_lahir'
            },{
                header: 'Sert',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_sertifikasi',
                renderer: function(v) { return (v==1) ? "Y" : "N"; }
            },{
                header: 'S1',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_s1',
                renderer: function(v) { return (v==1) ? "Y" : "N"; }
            },{
                header: 'PNS',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_pns',
                renderer: function(v) { return (v==1) ? "Y" : "N"; }
            },{
                header: 'Pensiun<br>thn ini',
                width: 80,
                align: 'right',
                sortable: true,
                dataIndex: 'tgl_pensiun',
                renderer: function(v) {
                    var pensiunYear = new Date(v).getYear();
                    var thisYear = new Date().getYear();
                    // console.log(pensiunYear);
                    // console.log(thisYear);
                    return (pensiunYear == thisYear) ? "Y" : "N";
                }
            },{
                header: 'Tgs Tambahan',
                width: 100,
                align: 'right',
                sortable: true,
                dataIndex: 'nama_tugas_tambahan'
            },{
                header: 'JJM',
                width: 70,
                align: 'right',
                sortable: true,
                dataIndex: 'jjm'
            },{
                header: 'JJM Satminkal',
                columns: [{
                    header: 'Diampu',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'induk_jjm_diampu'
                },{
                    header: 'Linier',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'induk_jjm_linier'
                }]
            },{
                header: 'JJM Luar Satminkal (Negeri)',
                columns: [{
                    header: 'Diampu',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'negeri_jjm_diampu'
                },{
                    header: 'Linier',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'negeri_jjm_linier'
                },{
                    header: 'Jml Sekolah',
                    width: 110,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'negeri_jumlah'

                }]
            },{
                header: 'JJM Luar Satminkal (Swasta)',
                columns: [{
                    header: 'Diampu',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'swasta_jjm_diampu'
                },{
                    header: 'Linier',
                    width: 90,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'swasta_jjm_linier'
                },{
                    header: 'Jml Sekolah',
                    width: 110,
                    align: 'right',
                    sortable: true,
                    dataIndex: 'swasta_jumlah'
                }]
            }]
        });

        me.items = {
            xtype: 'container',
            layout: 'fit',
            items: [listGrid]
        };

        this.callParent();
    }

});

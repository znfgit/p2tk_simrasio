Ext.define('SimptkRasio.view.components.FormFilterSimulasiKg', {
    extend: 'Ext.form.FormPanel', 
    xtype: 'form_filter_simulasi_kg',
    //layout: 'fit',
    //frame: true,
    defaultType: 'textfield',
    defaults: {
        labelWidth: 150,
        padding: '0 0 0 20px'
    },
    
    initComponent: function() {

		var me = this;

        me.tbar = [{
            xtype: 'tbtext',
            text: '<b>Parameter</b>'
        // },'->',{
        //     glyph: '61639@font-awesome'
        // },{
        //     glyph: '61564@font-awesome'
        // },{
        //     glyph: '61666@font-awesome'
        }];
        
        me.items = [{
            xtype: 'radiogroup',
            fieldLabel: 'Rasio Siswa Guru',
            anchor: '100%',
            items: [
                {boxLabel: 'Existing', name: 'rsg', inputValue: 1, checked: true},
                {boxLabel: 'PP 74', name: 'rsg', inputValue: 20},
                {boxLabel: 'SNP', name: 'rsg', inputValue: 28},
                {boxLabel: 'SPM', name: 'rsg', inputValue: 32}
            ]
        },{
            xtype: 'radiogroup',
            fieldLabel: 'Beban Mengajar',
            anchor: '100%',
            items: [
                {boxLabel: '24 jam', name: 'bm', inputValue: 24, checked: true},
                {boxLabel: '36 jam', name: 'bm', inputValue: 36},
                {boxLabel: '40 jam', name: 'bm', inputValue: 40}
            ]
        // },{
        //     xtype: 'radiogroup',
        //     fieldLabel: 'Rasio Siswa G.BK',
        //     anchor: '100%',
        //     items: [
        //         {boxLabel: 'Ideal', name: 'rsgbk', inputValue: 150, checked: true},
        //         {boxLabel: 'Minimal', name: 'rsgbk', inputValue: 200}
        //     ]
        },{
            xtype: 'checkbox',
            fieldLabel: 'Formasi CPNS',
            anchor: '100%',
            inputValue: 1,
            checked: false,
            name: 'is_pns'
        },{
            xtype: 'checkbox',
            fieldLabel: 'Filter Pensiun',
            anchor: '100%',
            inputValue: 1,
            checked: false,
            name: 'is_pensiun'
        }];

        this.callParent();
    }
});
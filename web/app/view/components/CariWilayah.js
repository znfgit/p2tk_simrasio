Ext.define('SimptkRasio.view.forms.CariWilayah', {
    extend: 'Ext.Window',
    xtype: 'form-cari-wilayah',
    width: 500,
    height: 150,
    title: 'Pencarian Wilayah',
    initComponent: function() {
		
		var me = this;
    	
    	this.myDataStore = Ext.create('Ext.data.Store', {
            fields: ['kode_wilayah', 'nama', 'id_level_wilayah', 'mst_kode_wilayah'],
            idProperty: 'kode_wilayah',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/CariWilayahCombo',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

    	me.items = [{
    		xtype: 'form',
    		bodyPadding: '5 5 0',
	        items: [{
	            xtype: 'combo',
	           	emptyText: 'Ketik Nama Wilayah',
	            store: this.myDataStore,
	            displayField: 'nama',
	            hiddenName: 'kode_wilayah',
	            typeAhead: false,
	            hideLabel: true,
	            hideTrigger:true,
	            anchor: '100%'
	        }]
	    }];

	    me.buttons = [{
            text: 'Batal',
            handler: function() {
                me.close();
            }
        },{
            text: 'Pilih dan Tutup',
            handler: function() {
                var combo = me.down('combo');
                var rec = combo.findRecordByValue(combo.getValue());
                //Ext.Msg.alert('Info', 'Anda memilih ' + rec.data.nama);
                Ext.getCmp('info-app').update(rec.data.nama);
                
                SimptkRasio.kode_wilayah = rec.data.kode_wilayah;
				SimptkRasio.nama_wilayah = rec.data.nama;
				SimptkRasio.skup = rec.data.id_level_wilayah;
				SimptkRasio.mst_kode_wilayah = rec.data.mst_kode_wilayah;
				
				globalNamaWilayah = rec.data.nama;

                me.close();
            }
        }];

        this.callParent();
    }

});
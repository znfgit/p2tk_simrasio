Ext.define('SimptkRasio.view.components.WindowListKeluar', {
    extend: 'Ext.window.Window',
    xtype: 'window_list_keluar',
    // width: 300,
    // height: 200,
    layout: 'fit',
    anchor: '100%',
    emptyText: 'Tidak ada data',
    selModel: new Ext.selection.RowModel(),
    // store: wilayahStore,
    hideHeaders: true,
    kodeWilayah: '000000 ',
    parentWilayah: '',
    kodeWilayahStr: '<b>Indonesia</b>',
    parentWilayahStr: '',


    initComponent: function() {
        
        var me = this;

        var nominasiStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'ptk_id', type: 'string'  } ,
                { name: 'nama_ptk', type: 'string'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'nama_sekolah', type: 'string'  } ,
                { name: 'nuptk', type: 'string'  } ,
                { name: 'nrg', type: 'string'  } ,
                { name: 'tgl_lahir', type: 'date'  } ,
                { name: 'is_pns', type: 'int'  } ,
                { name: 'is_honorer', type: 'int'  } ,
                { name: 'is_sertifikasi', type: 'int'  } ,
                { name: 'is_s1', type: 'int'  } ,
                { name: 'jjm', type: 'int' }
            ],
            idProperty: 'ptk_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/ListNominasi',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });

        var nominasiGrid = new Ext.grid.GridPanel({
            // flex: 2,
            width: 450,
            padding: '20px 0 0 0',
            anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel({
                mode: 'MULTI'
            }),
            store: nominasiStore,
            
            tbar: [
            '->',{
                text: 'Reload',
                glyph: '61666@font-awesome'
            }],

            bbar: Ext.create('Ext.PagingToolbar', {
                store: nominasiStore,
                displayInfo: true,
                displayMsg: 'Menampilkan data {0} - {1} of {2}',
                emptyMsg: "Tidak ada data"
            }),

            columns: [{
                header: 'No',
                width: 45,
                sortable: true,
                hidden: true,
                dataIndex: 'ptk_id'
            },{
                header: 'Nama Guru',
                width: 170,
                sortable: true,
                dataIndex: 'nama_ptk'
            },{
                header: 'Nama Sekolah',
                width: 200,
                sortable: true,
                dataIndex: 'nama_sekolah'
            },{
                header: 'NUPTK',
                width: 150,
                sortable: true,
                dataIndex: 'nuptk'
            },{
                header: 'NRG',
                width: 120,
                sortable: true,
                dataIndex: 'nrg'
            },{
                header: 'Sert',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_sertifikasi',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{
                header: 'S1',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_s1',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{
                header: 'PNS',
                width: 60,
                align: 'right',
                sortable: true,
                dataIndex: 'is_pns',
                renderer: function(v) { return (v==1) ? "Y" : "N"}
            },{
                header: 'JJM',
                width: 70,
                align: 'right',
                sortable: true,
                dataIndex: 'jjm'
            }]
        });

        nominasiStore.on('beforeload', function(store){
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: me.bentuk_pendidikan_id,
                level_wilayah: me.level_wilayah,
                sekolah_id: me.sekolah_id,
                mata_pelajaran_id: me.mata_pelajaran_id,
                guru_bk: me.guru_bk
            });

        });

        nominasiStore.load();

        me.items = {
            xtype: 'container',
            layout: 'fit',
            // layout: {
            //     type: 'hbox',
            //     align: 'stretch'
            // },
            items: [nominasiGrid]
        };

        this.callParent();
    }

});

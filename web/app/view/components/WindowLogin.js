Ext.define('SimptkRasio.view.components.WindowLogin', {
    extend: 'Ext.window.Window',
    xtype: 'window_login',
    height: 400,
    width: 600,
    resizable: false,
    modal: false,
    title: 'Direktorat P2TK Dikdas Kemdikbud',
    autoScroll: false,
    frame: true,            
    border: true,
    layout: 'vbox',
    movable: false,
    closable: false,
    url: null,

    initComponent: function() {

		var me = this;

        me.items = [{
            width: 600,
            height: 250,
            layout: 'fit',
            items: [{
                style: 'background-image:url(/resources/images/splash-simrasio.png)',
                // style: 'background-image: url(/resources/images/blur-bg-ipad2.jpg); background-size: 800px 160px; width: 800px; height: 160px;',
                bodyStyle: 'background:none; color: rgba(255,255,255,0.9); text-shadow: 1px 1px 2px rgba(255,255,255,0.3); font-size: 24pt; font-family: "Raleway", sans-serif; line-height:normal; margin-bottom: 0px;',
                padding: '16 0 0 16',
                plain: true,
                border: false,
                anchor: '100%',
                html: 'SIM Pemerataan Guru'
            }]
        },{
            layout: 'fit',
            border: false,
            items: [{   
                xtype: 'form',
                bodyStyle: 'background-color: #EEEEEE; padding-left:20; padding-right:20, color: white',                
                // xtype: 'login-form',
                frame: false,            
                border: false,
                width: 600,
                bodyPadding: '10 10 10 10',
                defaultType: 'textfield',

                defaults: {
                    anchor: '100%'
                },
                items: [
                    {
                        allowBlank: false,
                        fieldLabel: 'Username',
                        name: 'username',
                        emptyText: 'Username'
                        // ,
                        // emptyText: 'Email',
                        // vtype: 'email'
                    },{
                        allowBlank: false,
                        fieldLabel: 'Password',
                        name: 'password',
                        emptyText: 'Password',
                        inputType: 'password'
                    }
                ]

            }]
        }];

        me.buttons = [{
            
            text: 'Login' ,
            handler: function() {
                var form = this.up('window').down('form').getForm();

                if (form.isValid()) { 
                
                    Ext.MessageBox.show({
                        msg: 'Please wait...',
                        progressText: 'Please wait...',
                        width: 200,
                        wait: true
                    });
                
                    form.submit({
                        clientValidation: true,
                        url: me.url,
                        method: 'GET',
                        submitEmptyText: false,
                        success: function(form, action) {
                            
                            var val = action.result;
                            if (val.success) {
                                Ext.MessageBox.hide();
                                //Ext.MessageBox.alert('Berhasil', 'Login Berhasil<br> Mohon tunggu beberapa saat....');
                                
                                // setTimeout( function() {
                                //     window.location='/';
                                // }, 50);
                                // 
                                console.log(val);
                                
                                SimptkRasio.user_kode_wilayah = val.kode_wilayah;
                                SimptkRasio.user_nama_wilayah = val.nama_wilayah;
                                SimptkRasio.user_mst_kode_wilayah = val.mst_kode_wilayah;
                                SimptkRasio.user_skup = val.skup;
                                 
                                SimptkRasio.username = val.username;
                                SimptkRasio.kode_wilayah = val.kode_wilayah;
                                SimptkRasio.nama_wilayah = val.nama_wilayah;
                                SimptkRasio.mst_kode_wilayah = val.mst_kode_wilayah;
                                SimptkRasio.user_skup = val.skup;

                                Ext.getCmp('info-app').update(val.nama_wilayah);
                                
                                me.close();

                            } else {
                                Ext.MessageBox.alert('Gagal', val.message);
                            }
                        },
                        failure: function(form, action) {
                            //...
                            var val = action.result;
                            Ext.MessageBox.alert('Gagal', val.message);
                        }
                    });
                } else {
                    Ext.Msg.alert('Error', 'Form harap isi dengan benar');
                }
            }
        }];

        this.callParent();

    }

});

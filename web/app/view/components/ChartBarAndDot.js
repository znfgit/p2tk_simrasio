Ext.define('SimptkRasio.view.components.ChartBarAndDot', {
    extend: 'Ext.Panel',
    // extend: 'Ext.chart.CartesianChart',
    xtype: 'chart_bar_and_dot',
    initComponent: function() {
        
        var me = this;

        me.items = {
            xtype: 'cartesian',
            // title: me.title,
            style: 'background: #fff;',
            legend: {
                docked: 'right'
            },
            insetPadding: {
                top: 30,
                left: 30,
                right: 30,
                bottom: 20
            },
            width: '100%',
            height: 310,
            store: me.store,
            axes: [{
                type: 'numeric',
                position: 'left',
                grid: true,
                fields: [
                    'jumlah_kebutuhan',
                    'jumlah_ptk_pns_s1_ada_utk_matpel',
                    'jumlah_ptk_pns_nons1_ada_utk_matpel',
                    'jumlah_ptk_honorer_s1_ada_utk_matpel',
                    'jumlah_ptk_honorer_nons1_ada_utk_matpel',
                    'jumlah_ptk_nonpns_s1_ada_utk_matpel',
                    'jumlah_ptk_nonpns_nons1_ada_utk_matpel'
                ],
                //renderer: function (v) { return v.toFixed(v < 10 ? 1: 0) + '%'; },
                label: {
                    fontWeight: 300
                },
                renderer: function (v) { return Ext.util.Format.number(v, '0,000'); },
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['nama_rincian'],
                label: {
                    // fontFamily: "Raleway",
                    fontWeight: 300,
                    rotate: {
                        degrees: -27
                    }
                }
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 'PNS S1', 'PNS < S1', 'Honda S1', 'Honda < S1', 'Hnr.Lain S1', 'Hnr.Lain < S1' ],
                xField: 'nama_rincian',
                yField: [
                    'jumlah_ptk_pns_s1_ada_utk_matpel',
                    'jumlah_ptk_pns_nons1_ada_utk_matpel',
                    'jumlah_ptk_honorer_s1_ada_utk_matpel',
                    'jumlah_ptk_honorer_nons1_ada_utk_matpel',
                    'jumlah_ptk_nonpns_s1_ada_utk_matpel',
                    'jumlah_ptk_nonpns_nons1_ada_utk_matpel'
                ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: '#000',
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var browser = item.series.title[item.series.yField.indexOf(item.yField)];
                        this.setTitle(browser + ' for ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.yField) + '%');
                    }
                }
            },{
                type: 'scatter',
                axis: 'left',
                title: '<b>Keb. Guru</b>',
                xField: 'nama_rincian',
                yField: 'jumlah_kebutuhan',
                marker: {
                    radius: 4
                },
                highlight: {
                    fill: '#000',
                    radius: 3,
                    'stroke-width': 2,
                    stroke: '#ccc'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var title = item.series.title;
                        this.setTitle(title + ' untuk ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.series.yField) );
                    }
                }
            }]
        };
       
        this.callParent();
    }

});
/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimptkRasio.view.main.Main', {
    extend: 'Ext.container.Container',

    xtype: 'app-main',
    
    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
        region: 'north',
        xtype: 'component',
        padding: 10,
        height: 40,
        html: '<div style="float: left;">Direktorat P2TK Dikdas - Kemdikbud</div><div id="last_updated" style="float: right; text-align: right;">&nbsp;</div>'
    },{
        xtype: 'panel',
        title: 'Menu',
        region: 'west',
        width: 280,              
        split: true,
        collapsible: true,
        layout: {
            type: 'fit'
        },
        items: [{
            xtype: 'treepanel',
            id: 'menu-tree-panel',
            rootVisible: false,
            root: {
                text: 'Report',
                expanded: true,
                children: [{
                //     text: 'Pilih Wilayah',
                //     leaf: true
                // }, {
                //     text: 'Test TabPanel',
                //     leaf: true
                // }, {
                // text: 'Rekap SD Berdasar Matpel',
                // leaf: true
                // }, {
                    text: 'Dashboard',
                    itemId: 'dashboard',
                    leaf: true
                }, {
                    text: 'Analisis SD Per-Wilayah (Formasi)',
                    itemId: 'combined_chart_kg_sd_pns_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis SD Per-Wilayah (Sertifikasi)',
                    itemId: 'combined_chart_kg_sd_sertifikasi_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis SD Per-Matpel (Formasi)',
                    itemId: 'combined_chart_kg_sd_pns_matpel',
                    leaf: true
                }, {
                    text: 'Analisis SD Swasta Per-Wilayah',
                    itemId: 'combined_chart_kg_sd_swasta_pns_wilayah',
                    leaf: true
                }, {
                    text: 'Simulasi KG SD Per-Matpel',
                    itemId: 'simulasi_kg_sd',
                    leaf: true
                }, {
                    text: 'Simulasi KG SD Per-Wilayah',
                    itemId: 'simulasi_kg_sd_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis SMP Per-Wilayah (Formasi)',
                    itemId: 'combined_chart_kg_smp_pns_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis SMP Per-Wilayah (Sertifikasi)',
                    itemId: 'combined_chart_kg_smp_sertifikasi_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis SMP Per-Matpel (Formasi)',
                    itemId: 'combined_chart_kg_smp_pns_matpel',
                    leaf: true
                }, {
                    text: 'Analisis SMP Swasta Per-Wilayah',
                    itemId: 'combined_chart_kg_smp_swasta_pns_wilayah',
                    leaf: true
                }, {
                    text: 'Simulasi KG SMP Per-Matpel',
                    itemId: 'simulasi_kg_smp',
                    leaf: true
                }, {
                    text: 'Simulasi KG SMP Per-Wilayah',
                    itemId: 'simulasi_kg_smp_wilayah',
                    leaf: true
                }, {
                    text: 'Analisis KG Guru BK',
                    itemId: 'combined_chart_kg_guru_bk',
                    leaf: true
                }, {
                    text: 'Keluar Aplikasi',
                    itemId: 'keluar_aplikasi',
                    leaf: true

                // }, {
                //     text: 'Kurang/Lebih Guru SD',
                //     leaf: true
                // }, {
                //     text: 'Rekap SMP Berdasar Matpel',
                //     leaf: true
                // }, {
                //     text: 'Rekap SMP Berdasar Wilayah',
                //     leaf: true
                // }, {
                //     text: 'Kurang/Lebih Guru SMP',
                //     leaf: true

                }]
            },
            listeners: {
                // //console.log(tabpanel);

                // switch (record.data.text) {
                //     case 'Formulir':
                //         tabpanel.setActiveTab( tabpanel.down("panel[itemId='PanelFormulir']") );
                //         break;
                //     case 'Data':
                //         tabpanel.setActiveTab( tabpanel.down("panel[itemId='PanelData']") );
                //         break;
                // }
                select: function(row, record, index, eOpts ) {
                    //Ext.Msg.alert('Info', 'Anda pilih ' + record.data.text);
                    var searchAddActivateTab = function(xtypeName, title) {

                        var tabpanel = Ext.getCmp('main-tabpanel');
                        var cnt = {};
                        
                        cnt = tabpanel.down("panel[xtype='" + xtypeName + "']")
                        if (cnt) {
                            
                        } else {
                            
                            cnt = Ext.create({
                                xtype: xtypeName,
                                layout: 'fit',
                                closable: true,
                                title: title
                            });
                            tabpanel.add(cnt);
                        }
                        tabpanel.setActiveTab(cnt);
                    }
                    

                    switch (record.data.itemId) {
                        // case 'Pilih Wilayah':
                        //     var winCariWilayah = new SimptkRasio.view.forms.CariWilayah();
                        //     winCariWilayah.show();
                        //     break;

                        // case 'Test TabPanel':
                        //     searchAddActivateTab ('form');
                        //     break;

                        case 'dashboard':
                            searchAddActivateTab('dashboard', 'Dashboard');
                            break;

                        case 'combined_chart_kg_sd_pns_wilayah':
                            searchAddActivateTab('combined_chart_kg_sd_pns_wilayah', 'KG SD Per-Wilayah (Formasi)');
                            break;

                        case 'combined_chart_kg_sd_sertifikasi_wilayah':
                            searchAddActivateTab('combined_chart_kg_sd_sertifikasi_wilayah', 'KG SD Per-Wilayah (Sert)');
                            break;

                        case 'combined_chart_kg_sd_pns_matpel':
                            searchAddActivateTab('combined_chart_kg_sd_pns_matpel', 'KG SD Per-Matpel (Formasi)');
                            break;

                        case 'combined_chart_kg_sd_swasta_pns_wilayah':
                            searchAddActivateTab('combined_chart_kg_sd_swasta_pns_wilayah', 'KG SD Swasta Per-Wilayah');
                            break;

                        case 'combined_chart_kg_smp_pns_wilayah':
                            searchAddActivateTab('combined_chart_kg_smp_pns_wilayah', 'KG SMP Per-Wilayah (Formasi)');
                            break;

                        case 'combined_chart_kg_smp_sertifikasi_wilayah':
                            searchAddActivateTab('combined_chart_kg_smp_sertifikasi_wilayah', 'KG SMP Per-Wilayah (Sert)');
                            break;

                        case 'combined_chart_kg_smp_pns_matpel':
                            searchAddActivateTab('combined_chart_kg_smp_pns_matpel', 'KG SMP Per-Matpel (Formasi)');
                            break;

                        case 'combined_chart_kg_smp_swasta_pns_wilayah':
                            searchAddActivateTab('combined_chart_kg_smp_swasta_pns_wilayah', 'KG SMP Swasta Per-Wilayah');
                            break;

                        case 'combined_chart_kg_guru_bk':
                            searchAddActivateTab('combined_chart_kg_guru_bk', 'Analisis KG Guru BK');
                            break;

                        case 'simulasi_kg_sd':
                            searchAddActivateTab('simulasi_kg_sd', 'Simulasi KG SD');
                            break;

                        case 'simulasi_kg_smp':
                            searchAddActivateTab('simulasi_kg_smp', 'Simulasi KG SMP');
                            break;
                        
                        case 'simulasi_kg_sd_wilayah':
                            searchAddActivateTab('simulasi_kg_sd_wilayah', 'Simulasi KG SD (Wil)');
                            break;

                        case 'simulasi_kg_smp_wilayah':
                            searchAddActivateTab('simulasi_kg_smp_wilayah', 'Simulasi KG SMP (Wil)');
                            break;

                        // case 'perhitungan_kg_sd':
                        //     searchAddActivateTab('perhitungan_kg_sd', 'Simulasi KG SD');
                        //     break;

                        case 'keluar_aplikasi':
                            //searchAddActivateTab('perhitungan_kg_sd', 'Simulasi KG SD');
                            //
                            // Ext.Ajax.request({
                            //     waitMsg: 'Menyimpan...',                        
                            //     url: '/logoutApp',                     
                            //     method: 'GET',
                            //     failure:function(response,options){                        
                            //         Ext.Msg.alert('Warning','Response : ' + response );
                            //     },
                            //     success: function(response,options){
                            //         var json = Ext.util.JSON.decode(response.responseText);
                            //         if (json.success == true) {                             
                                        
                            //             Ext.Msg.alert('OK', json.message);
                                        
                            //             setTimeout( function() {
                            //                 window.location='/';
                            //             }, 1000);

                            //         } else if (json.success == false){
                            //             Ext.Msg.alert('Error', json.message);
                            //         }
                            //     }
                            // });
                            
                            window.location.assign("/logout");

                            break;

                        // case 'Rekap SD Berdasar Wilayah':
                        //     // var winRekapWilayah = new SimptkRasio.view.forms.RekapWilayah();
                        //     // winRekapWilayah.show();
                        //     searchAddActivateTab('form-rekap-wilayah', 'Rekap Wilayah');
                        //     break;

                        // case 'Kurang/Lebih Guru SD':
                        //     // var winRekapWilayah = new SimptkRasio.view.forms.WinRekapJmlLebihKurangGuruSd();
                        //     // winRekapWilayah.show();
                        //     searchAddActivateTab('rekap-jml-lebih-kurang-guru-sd', 'Rekap Wilayah');

                        //     break;

                        // case 'Rekap SMP Berdasar Matpel':
                        //     if (!SimptkRasio.kode_wilayah) {
                        //         Ext.Msg.alert('Error', 'Mohon pilih dulu wilayah yang diinginkan');
                        //         return;
                        //     }
                        //     // var winRekapMatpel = new SimptkRasio.view.forms.RekapMatpelSmp();
                        //     // winRekapMatpel.show();
                        //     searchAddActivateTab('rekap-jml-lebih-kurang-guru-sd', 'Rekap Wilayah');
                        //     break;

                        // case 'Rekap SMP Berdasar Wilayah':
                        //     var winRekapWilayah = new SimptkRasio.view.forms.RekapWilayahSmp();
                        //     winRekapWilayah.show();
                        //     break;

                        // case 'Kurang/Lebih Guru SMP':
                        //     var winRekapWilayah = new SimptkRasio.view.forms.WinRekapJmlLebihKurangGuruSmp();
                        //     winRekapWilayah.show();
                        //     break;

                    }
                    var tree = Ext.getCmp('menu-tree-panel');
                    tree.getSelectionModel().deselectAll();
                }
            }

        }]
    },{
        region: 'center',
        xtype: 'tabpanel',
        id: 'main-tabpanel',
        tabPosition: 'bottom',
        items: [{
            // Tab Pertama - Beranda
            //title: 'Beranda',
            //html: 'Ini adalah beranda'
            title: 'Dashboard',
            xtype: 'dashboard',
            //reference: 'dashboard',
            region: 'center',
            stateful: false,

            columnWidths: [
                0.70,
                0.30
            ],

            parts: {
                appTitle: {
                    viewTemplate: {
                        title: 'Aplikasi',
                        items: [{
                            xtype: 'panel',
                            padding: '16 0 0 16',
                            style: 'background-image: url(/resources/images/blur-bg-ipad2.jpg); background-size: 800px 160px; width: 800px; height: 160px;',
                            bodyStyle: 'background:none; color: rgba(255,255,255,0.9); text-shadow: 1px 1px 2px rgba(255,255,255,0.3); font-size: 24pt; font-family: "Raleway", sans-serif; line-height:normal; margin-bottom: 0px;',
                            html: 'SIM Pemerataan Guru'
                        }]
                    }
                },
                barchartRekapRasioNasional: {
                    viewTemplate: {
                        title: 'Analisis Kebutuhan Guru Kelas SD Nasional',
                        items: [{
                            //xtype: 'barchart-rekap-rasio-nasional'
                            //combined-chart-kg-sd
                            xtype: 'combined_chart_kg_sd_dashboard'
                        }]
                    }
                },
                appInfo: {
                    viewTemplate: {
                        title: 'Parameter',
                        items: [{
                            xtype: 'panel',
                            id: 'info-app',
                            padding: '16 0 0 16',
                            style: 'background-image: url(/resources/images/blur-bg-ipad3.jpg); background-position: right; background-size: 800px 160px; width: 800px; height: 160px;',
                            bodyStyle: 'background:none; color: rgba(255,255,255,0.9); text-shadow: 1px 1px 2px rgba(255,255,255,0.3); font-size: 24pt; font-family: "Raleway", sans-serif; line-height:normal; margin-bottom: 0px;',
                            html: 'Skup: Nasional'
                        }]
                    }
                },
                sampleHtml: {
                    viewTemplate: {
                        title: 'About',
                        items: [{
                            xtype: 'panel',
                            padding: 16,
                            html: 'Aplikasi SIM Rasio adalah salah satu dari paket Aplikasi Warehouse P2TK Dikdas Kemdikbud. Aplikasi ini berfungsi ' +
                                    'untuk menganalisis rasio kecukupan Guru dari jenjang sekolah, rekapitulasi kecamatan, kabupaten/kota, propinsi ' +
                                    'sampai rekapitulasi secara nasional. Melalui aplikasi ini juga, dapat disimulasikan formasi untuk pemerataan guru ' +
                                    'secara nasional. <br><br>' +
                                    'Aplikasi masih dalam pengembangan yang baru dimulai dalam waktu yang sangat singkat sehingga masih banyak kekurangan '+
                                    'yang dapat ditemukan. Di aspek fitur analisis data, saat ini SIM Rasio masih fokus pada kualitas data sekolah, dan ' + 
                                    'menghitung kebutuhan guru yang direkap pada jumlah sekolah.    ' +
                                    'Diagram di samping menunjukkan sebagai berikut: <br><ul>' + 
                                    '<li>warna hijau adalah jumlah sekolah yang terhitung kelebihan guru.</li>' +
                                    '<li>warna biru adalah jumlah sekolah yang terhitung kebutuhannya pas dengan jumlah guru tersedia. </li>' +
                                    '<li>warna merah adalah jumlah sekolah yang terhitung kekurangan guru. </li>' +
                                    '<li>warna orange adalah jumlah sekolah yang datanya dianggap tidak lengkap, yaitu yang tidak satu pun guru tersedia utk mapel ybs. </li></ul>'

                        }]
                    }
                } 
            },

            defaultContent: [{
                type: 'appTitle',
                columnIndex: 0,
                height: 130
            }, {
                type: 'barchartRekapRasioNasional',
                columnIndex: 0,
                height: 520
            }, {
                type: 'appInfo',
                columnIndex: 1,
                height: 130
            }, {
                type: 'sampleHtml',
                columnIndex: 1,
                height: 520
            }]

        }]
    }]
});

/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('SimptkRasio.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'SimptkRasio'
    }

    //TODO - add data, formulas and/or methods to support your view
});
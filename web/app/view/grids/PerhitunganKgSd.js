Ext.define('SimptkRasio.view.grids.PerhitunganKgSd', {
    extend: 'SimptkRasio.view.grids.PerhitunganKgBase',
    xtype: 'perhitungan_kg_sd',
    layout: 'fit',

    initComponent: function(config){
        
        ////////////////////
        this.cm = [{           
            header: 'No',
            width: 45,
            sortable: true, 
            dataIndex: 'perhitungan_rasio_id'
        },{                
            header: 'Jenis Guru',
            width: 200,
            sortable: true, 
            dataIndex: 'nama'
        },{                
            header: 'MP1',
            width: 60,
            sortable: true,
            hidden: false, 
            dataIndex: 'mp1'
        },{                
            header: 'K1',
            width: 100,
            sortable: true, 
            dataIndex: 'k1'
        },{                
            header: 'MP2',
            width: 60,
            sortable: true,
            hidden: true, 
            dataIndex: 'mp2'
        },{                
            header: 'K2',
            width: 100,
            sortable: true, 
            dataIndex: 'k2'
        },{                
            header: 'MP3',
            width: 60,
            sortable: true, 
            hidden: true, 
            dataIndex: 'mp3'
        },{                
            header: 'K3',
            width: 100,
            sortable: true, 
            dataIndex: 'k3'
        },{                
            header: 'MP4',
            width: 60,
            sortable: true, 
            hidden: true, 
            dataIndex: 'mp4'
        },{                
            header: 'K4',
            width: 100,
            sortable: true, 
            dataIndex: 'k4'
        },{                
            header: 'MP5',
            width: 60,
            sortable: true, 
            hidden: true, 
            dataIndex: 'mp5'
        },{                
            header: 'K5',
            width: 100,
            sortable: true, 
            dataIndex: 'k5'
        },{                
            header: 'MP6',
            width: 60,
            sortable: true, 
            hidden: true, 
            dataIndex: 'mp6'
        },{                
            header: 'K6',
            width: 100,
            sortable: true, 
            dataIndex: 'k6'
        },{                
            header: 'JR',
            align: 'right',
            width: 80,
            renderer: Ext.util.Format.formatNumber,
            sortable: true, 
            dataIndex: 'jr'
        },{                
            header: 'JTM',
            width: 80,
            align: 'right', 
            renderer: Ext.util.Format.formatNumber,
            sortable: true, 
            dataIndex: 'jtm'
        },{                
            header: 'RGK',
            width: 80,
            align: 'right', 
            renderer: Ext.util.Format.formatNumberWithPrecision,
            sortable: true, 
            dataIndex: 'rasio_gk'
        },{                
            header: 'JS',
            width: 80,
            align: 'right', 
            renderer: Ext.util.Format.formatNumber,
            sortable: true, 
            dataIndex: 'jumlah_siswa'
        },{                
            header: 'KG',
            width: 80,
            align: 'right', 
            renderer: Ext.util.Format.formatNumberWithPrecision,
            sortable: true, 
            dataIndex: 'kg'
        }];

        this.ds = Ext.create('Ext.data.Store', {
            // pageSize: 17,
            fields: [
                { name: 'perhitungan_rasio_id', type: 'int'  } ,
                { name: 'mata_pelajaran_id', type: 'int'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'bentuk_pendidikan_id', type: 'int'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'tahun_ajaran_id', type: 'float'  } ,
                { name: 'mp1', type: 'float' } ,
                { name: 'k1', type: 'float' } ,
                { name: 'mp2', type: 'float' } ,
                { name: 'k2', type: 'float' } ,
                { name: 'mp3', type: 'float' } ,
                { name: 'k3', type: 'float' } ,
                { name: 'mp4', type: 'float' } ,
                { name: 'k4', type: 'float' } ,
                { name: 'mp5', type: 'float' } ,
                { name: 'k5', type: 'float' } ,
                { name: 'mp6', type: 'float' } ,
                { name: 'k6', type: 'float' } ,
                { name: 'k6', type: 'float' } ,
                { name: 'jr', type: 'float' } ,
                { name: 'jtm', type: 'float' } ,
                { name: 'rasio_gk', type: 'float' } ,
                { name: 'jumlah_siswa', type: 'float' } ,
                { name: 'kg_jam', type: 'float' } ,
                { name: 'kg_orang', type: 'float' } ,
                { name: 'gt_jam', type: 'float' } ,
                { name: 'gt_orang', type: 'float' } ,
                { name: 'lebih_jam', type: 'float' } ,
                { name: 'kurang_jam', type: 'float' } ,
                { name: 'lebih_orang', type: 'float' } ,
                { name: 'kurang_orang', type: 'float' } 
            ],
            idProperty: 'perhitungan_rasio_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/PerhitunganKgSd',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });
        
        Ext.apply(this, {   
            jenjang: 'sd',
            title: this.title + ' SD'
        });
        
        this.callParent();

    }
    
});
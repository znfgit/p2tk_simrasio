Ext.define('SimptkRasio.view.grids.GridPerMatpel', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_per_matpel',
    // layout: 'fit',

    // Defaults
    width: '100%',
    autoScroll: true,
    defaults: {
        sortable: true
    },
    requires: [
        'SimptkRasio.view.components.WindowPrint',
        'Ext.state.*',
        'Ext.grid.feature.Summary'
    ],
    user_kode_wilayah: null,
    user_nama_wilayah: null,
    user_mst_kode_wilayah: null,
    kode_wilayah: null,
    nama_wilayah:  null,
    mst_kode_wilayah: null,
    nama_mst_wilayah: null,
    skup: null,
    mata_pelajaran_id: null,
    columns: null,
    tbar: null,
    lockedGridConfig: {
        forceFit: true
    },
    // lockedViewConfig: {
    //     scroll: 'horizontal'
    // },


    // The 'global' var used no longer global, but as dynamic properties inside this component using getters
    
    // Getter for MataPelajaran Id
    getMataPelajaranId: function() {
        return this.mata_pelajaran_id;
    },

    // Getter for KodeWilayah. 
    getKodeWilayah: function() {
        return this.kode_wilayah;
    },

    getStore: function() {
        return this.store;
    },
    
    // Always update extraParams to the newest values upon load. Don't forget the scope
    beforeLoad: function() {

        me = this;
        store = me.getStore();

        var proxy = store.getProxy();
        proxy.setExtraParams({
            mata_pelajaran_id: me.mata_pelajaran_id,
            kode_wilayah: me.kode_wilayah,
            satuan: me.satuan
        });

    },

    // Constructor shit
    initComponent: function() {

        me = this;

        me.addStateEvents('changewilayah');

        // Empty
        me.columns = [];
        me.tbar = [];

        // Init vars
        console.log(SimptkRasio);

        me.user_kode_wilayah = SimptkRasio.user_kode_wilayah;
        me.user_nama_wilayah = SimptkRasio.user_nama_wilayah;
        me.user_mst_kode_wilayah = SimptkRasio.user_mst_kode_wilayah;
        me.kode_wilayah = SimptkRasio.user_kode_wilayah;
        me.nama_wilayah =  SimptkRasio.user_nama_wilayah;
        me.mst_kode_wilayah = SimptkRasio.user_mst_kode_wilayah;
        me.satuan = 'jam';

        // me.nama_mst_wilayah = null;
        me.skup = SimptkRasio.user_skup;
        me.mata_pelajaran_id = (me.jenjang === 'sd') ? 4020 : 4200;

        me.pagingTb = Ext.create('Ext.PagingToolbar', {
            store: me.store,
            displayInfo: true,
            displayMsg: 'Data #{0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });

        switch (me.per) {
            
            case 'wilayah':
                
                me.persen = '';
                
                me.features = [{
                    ftype: 'summary',
                    dock: 'top',
                    remoteRoot: 'summaryData'
                }];

                me.tbar = [{
                    xtype: 'button',
                    text: 'Wilayah Induk',
                    //cls: 'addbutton',
                    glyph: '61714@font-awesome',
                    scope: this,
                    action: 'add',
                    handler: function(btn) {
                        
                        if (me.mst_kode_wilayah === '') {
                            return;
                        }

                        if (me.mst_kode_wilayah == me.user_mst_kode_wilayah) {
                            return;
                        }

                        // Record Last Postion
                        var cp = new Ext.state.CookieProvider();
                        Ext.state.Manager.setProvider(cp);

                        var key = "key" + me.kode_wilayah.toString().trim();
                        cp.set(key, me.store.currentPage);
                        //console.log(cp.state);
                        // Record end
                                                
                        Ext.Ajax.request({
                            url: '/SearchWilayah/' + me.mst_kode_wilayah,
                            success: function(response){
                                
                                // var text = response.responseText;
                                // var res = Ext.JSON.decode(response.responseText);
                                // SimptkRasio.kode_wilayah = res.kode_wilayah;
                                // SimptkRasio.nama_wilayah = res.nama;
                                // SimptkRasio.skup = res.id_level_wilayah;
                                // SimptkRasio.mst_kode_wilayah = res.mst_kode_wilayah;
                                // Ext.getCmp('info-app').update(res.nama);
                                
                                // //me.close();
                                // var winRekapWilayah = new SimptkRasio.view.forms.RekapWilayah();
                                // winRekapWilayah.down('tabpanel').setActiveTab(1);
                                // winRekapWilayah.show();
                                var text = response.responseText;
                                var res = Ext.JSON.decode(response.responseText);
                                
                                me.kode_wilayah = res.kode_wilayah;
                                me.nama_wilayah = res.nama;
                                me.skup = res.id_level_wilayah;
                                me.mst_kode_wilayah = res.mst_kode_wilayah;

                                // Load Page
                                var key = "key" + me.kode_wilayah.toString().trim();
                                var page = cp.get(key,1);

                                //console.log('Back wilayah executed');
                                //console.log('Type: ' + typeof cp.get(key));
                                //console.log('State: ' + cp.get(key));
                                //console.log('Current page ' + me.store.currentPage);
                                //console.log('New page ' + page);
                                //console.log(cp.state);
                                
                                me.store.currentPage = page;
                                me.store.loadPage(page);
                                //

                                // The component fires event. Parameters passed via "this" object 
                                me.fireEvent('changewilayah', me);

                            }
                        });
                        
            //             Actions.Controller.search_wilayah ({ kode_wilayah: me.mst_kode_wilayah}, function(result, ev) {

                        //     var res = Ext.JSON.decode(result);
                           //  me.kode_wilayah = res.kode_wilayah;
                           //  me.nama_wilayah = res.nama;
                           //  me.skup = res.id_level_wilayah;
                           //  me.mst_kode_wilayah = res.mst_kode_wilayah;

                           //  me.store.reload();

                           //  // The component fires event. Parameters passed via "this" object 
                           //  me.fireEvent('changewilayah', me);
                        // });

                    }
                },{
                    xtype: 'combo_matpel',
                    width: 160,
                    initialValue: me.mata_pelajaran_id,
                    jenjang: me.jenjang,
                    listeners: {
                        select: function(combo) {
                            me.mata_pelajaran_id = combo.getValue();
                            me.store.reload();
                        }
                    }
                },{
                    xtype: 'combo',
                    itemId: 'satuancombo',
                    valueField: 'satuan',
                    displayField: 'nama',
                    emptyText: 'Satuan..',
                    value: me.satuan,
                    width: 80,
                    store: {
                        type: 'array',
                        fields: [ 'satuan', 'nama' ],
                        data: [
                            ['jam', 'Jam'],
                            ['org', 'Orang']
                        ]
                    },
                    listeners: {
                        select: function(combo) {
                            if (combo.getValue() == 'org') {
                                var win = new Ext.create('Ext.window.Window',{
                                    title: '<b>Perhatian</b>',
                                    width: 700,
                                    height: 680,
                                    style: 'padding: 20px;',
                                    html: [
                                        '<span style="text-decoration: blink;color: red;"><b>PENTING</span>: MOHON BACA DULU DENGAN SEKSAMA!<br><br>',
                                        'Perhitungan Kebutuhan Guru PALING AKURAT adalah dalam format JAM. Dengan format JAM, maka jumlah ',
                                        'agregat antar jenjang AKAN PAS karena TIDAK ADA PEMBULATAN. Oleh karena itu, jika anda lebih ',
                                        'menyukai KESAMAAN AGREGAT ANTAR JENJANG, maka disarankan UNTUK MELAKUKAN ANALISIS BERBASIS JAM.',
                                        '<br><br>Namun demikian, baru saja anda memilih SATUAN ORANG. MOHON DIPERHATIKAN bahwa dalam SATUAN ORANG, ',
                                        'JUMLAH AGREGAT ANTAR JENJANG AKAN SEDIKIT BERBEDA (sekitar satu sampai belasan orang), karena adanya pembagian ',
                                        'dengan 24 jam yang konsekuensinya adalah terjadinya PEMBULATAN DI SETIAP JENJANG.<br><br>',
                                        'Sebagai contoh, DALAM SATUAN JAM:<br>- Sekolah A KG nya 36 jam<br>- Sekolah B KG nya 12 jam<br>- Di level kecamatan agregatnya ',
                                        'akan konsisten, 36 + 12 = 48 jam.<br><br>TAPI JIKA DIHITUNG DALAM SATUAN ORANG per sekolah, maka akan terjadi ',
                                        'pembulatan ke atas:<br>- KG sekolah A 36 / 24 = 1.5 guru dibulatkan jadi 2 (DUA ORANG),<br>- KG sekolah B 12/24 = 0.5 ',
                                        'guru dibulatkan menjadi 1 (SATU ORANG).<br>- Maka jumlah KG nya jika dihitung per sekolah 2 + 1 = 3 (TIGA ORANG).<br>- Sedangkan ',
                                        'jika dihitung grouping per kecamatan maka (36 jam + 12 jam) / 24 = 2 (DUA ORANG) saja.<br><br>Pertanyaannya mengapa ',
                                        'tidak menghitung pembulatan di sekolah saja, terus hitung ke atas? Jawabannya adalah: <br>1) nanti agregat ',
                                        'nasional akan MELESET JAUH karena terlalu banyak pembulatan ke atas di level sekolah. <br>2) akan memberatkan ',
                                        'sistem karena aplikasi akan harus menghitung looping per sekolah sejumlah ratusan ribu sekolah setiap ',
                                        'kali perhitungan query yang agregatnya tingkat tinggi (nasional/propinsi). Maka beban server akan BERAT/MATI. ',
                                        '<br><br>Dengan menekan YA anda menyatakan SUDAH MEMAHAMI DAN MEMAKLUMI MENGAPA DALAM SATUAN ORANG, AGREGATNYA AKAN SEDIKIT BERBEDA PADA ',
                                        'SETIAP JENJANG, YAITU KARENA PEMBULATAN, BUKAN KARENA KESALAHAN SISTEM. DAN MERUPAKAN KESADARAN ANDA ',
                                        'SENDIRI MEMILIH FORMAT INI KARENA KEBUTUHAN ANDA.<br><br>YAKIN AKAN LANJUT?</b>'
                                    ],
                                    buttons: [{
                                        text: '<b>Ya</b>',
                                        handler: function(){
                                            me.satuan = combo.getValue();
                                            me.store.reload();
                                            me.columns[1].setText('Keb.Guru ('+ combo.getValue() + ')');
                                            me.columns[2].setText('Jml Guru ('+ combo.getValue() + ')');

                                            win.close();
                                        }
                                    },{
                                        text: '<b>Tidak</b>',
                                        handler: function(){
                                            combo.setValue('jam');
                                            win.close();
                                            return;
                                        }
                                    }]
                                });
                                win.show();
                            } else {
                                me.satuan = combo.getValue();
                                me.store.reload();
                                me.columns[1].setText('Keb.Guru ('+ combo.getValue() + ')');
                                me.columns[2].setText('Jml Guru ('+ combo.getValue() + ')');
                            }
                        }
                    }

                },
                    Ext.create('Ext.PagingToolbar', {
                        store: me.store,
                        displayInfo: true,
                        displayMsg: 'Data #{0} - {1} dari {2}',
                        emptyMsg: "Tidak ada data"
                    })
                , '->',  {
                    text: 'Daftar PTK',
                    itemId: 'daftar_ptk',
                    hidden: false,
                    glyph: '61632@font-awesome',
                    menu: [{
                        text: 'Tampilkan',
                        listeners: {
                            
                            click: function(btn){

                                // var sels = kgGrid.getSelectionModel().getSelections();
                                // var sel = sels[0];

                                var selections = me.getSelectionModel().getSelection();
                                if (selections.length <= 0) {
                                    //Ext.Msg.alert('Error', 'Mohon drilldown sampai list sekolah dan pilih salah satu sekolah dari tabel..');
                                    Ext.Msg.alert('Error', 'Mohon pilih salah satu dari tabel..');
                                    return;
                                }
                                // if (me.skup < 1) {
                                //     Ext.Msg.alert('Error', 'Mohon drilldown terlebih dahulu..');
                                //     return;
                                // }

                                if (selections) {
                                    
                                    var r = selections[0];
                                    //console.log(r);

                                    var comboMatpel = me.down('combo_matpel');

                                    var kode_wilayah = me.kode_wilayah;
                                    var namaSekolah = r.data.nama_rincian;
                                    var namaMatpel = comboMatpel.getRawValue();

                                    if (me.purpose == 'bk') {
                                        namaMatpel = 'BK';
                                    }
                                    
                                    var title = 'Daftar Guru ' + namaMatpel + ' di ' + namaSekolah;
                                    
                                    var win = new Ext.widget({
                                        xtype: 'window_list_ptk',
                                        title: title,
                                        width: 900,
                                        height: 500,
                                        modal: true,
                                        guru_bk: (me.purpose == 'bk') ? 1 : 0,
                                        purpose: me.purpose,
                                        status_sekolah: 1,
                                        bentuk_pendidikan_id: (me.jenjang === 'sd') ? 5 : 6,
                                        level_wilayah: me.skup + 1,
                                        sekolah_id: r.data.kode_rincian,
                                        nama_sekolah: r.data.nama_rincian,
                                        nama_matpel: namaMatpel,
                                        mata_pelajaran_id: me.mata_pelajaran_id
                                    });
                                    
                                    win.show();
                                } else {
                                    
                                }
                            }
                        }
                    },{
                        text: 'Unduh',
                        listeners: {
                            click: function(btn){

                                var selections = me.getSelectionModel().getSelection();
                                if (selections.length <= 0) {
                                    //Ext.Msg.alert('Error', 'Mohon drilldown sampai list sekolah dan pilih salah satu sekolah dari tabel..');
                                    Ext.Msg.alert('Error', 'Mohon pilih salah satu dari tabel..');
                                    return;
                                }

                                if (selections) {
                                    
                                    var r = selections[0];
                                    var skup_rincian = me.skup + 1;

                                    window.location.assign('/ExcelDataGuru/' + r.data.kode_rincian + '/' + skup_rincian + '/' + me.mata_pelajaran_id + '/' + me.jenjang + '/1' );

                                }
                            }
                        }
                    }]
                },{
                    text: 'Cetak',
                    glyph: '61487@font-awesome',
                    handler: function(btn) {

                        var win = new Ext.widget({
                            xtype: 'window_print',
                            title: 'Cetak',
                            src: '/PrintingData/' + me.per + '/' + me.purpose + '/' + me.kode_wilayah + '/' + me.skup + '/' + me.mata_pelajaran_id + '/' + me.jenjang
                        });
                        
                        win.show();
                        
                    }
                },{
                    text: 'Download',
                    // glyph: '57517@font-fileformats-icons',
                    glyph: '57413@font-icomoon',
                    handler: function(btn) {
                        
                        window.location.assign('/ExcelData/' + me.per + '/' + me.purpose + '/' + me.kode_wilayah + '/' + me.skup + '/' + me.mata_pelajaran_id + '/' + me.jenjang);

                    }
                }];

                break;

            case 'matpel':
                
                me.persen = ' %';

                me.tbar = [{
                    xtype: 'combo_wilayah',
                    width: 300,
                    listeners: {
                        select: function(combo){

                            var rec = combo.findRecordByValue(combo.getValue());
                            var nama = rec.data.nama;
                            
                            combo.up('combined_chart_kg_sd_pns_matpel').down('cartesian').setTitle('Rekap Kebutuhan Guru SD Per-Matpel (dalam % thd KG) - ' + nama);
                            
                            SimptkRasio.kode_wilayah = combo.getValue();
                            SimptkRasio.nama_wilayah = nama;

                            var store = me.store;
                            // store.getProxy().setUrl('/RekapKgMatpelSmpWilayah/' + combo.getValue());
                            // store.reload();
                            
                            store.on('beforeload', function(){
                                var proxy = store.getProxy();
                                //proxy.setExtraParam('mata_pelajaran_id', SimptkRasio.mata_pelajaran_id);
                                proxy.setExtraParam('kode_wilayah', SimptkRasio.kode_wilayah);
                            });

                            store.reload();
                        }
                    }
                },
                    me.pagingTb
                , '->',  {
                    text: 'Cetak',
                    glyph: '61487@font-awesome',
                    handler: function(btn) {

                        var win = new Ext.widget({
                            xtype: 'window_print',
                            title: 'Cetak',
                            src: '/PrintingData/' + me.per + '/' + me.purpose + '/' + me.kode_wilayah + '/' + me.skup + '/' + me.mata_pelajaran_id + '/' + me.jenjang
                        });
                        
                        win.show();
                        
                    }
                },{
                    text: 'Download',
                    // glyph: '57517@font-fileformats-icons',
                    glyph: '57413@font-icomoon',
                    handler: function(btn) {
                        
                        window.location.assign('/ExcelData/' + me.per + '/' + me.purpose + '/' + me.kode_wilayah + '/' + me.skup + '/' + me.mata_pelajaran_id + '/' + me.jenjang);

                    }
                }];
                break;


        };


        switch (me.purpose) {
            
            case 'formasi':
                me.columns = [
                {
                    text: (me.per == 'wilayah') ? ((me.skup < 3) ? 'Wilayah' : 'Sekolah') : 'Mata Pelajaran',
                    width: 200,
                    locked: true,
                    summaryType: 'count',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        //return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';
                        var namawilayah = (me.nama_wilayah == 'Indonesia') ? 'Nasional' : me.nama_wilayah;
                        return '<b>Total '+ namawilayah +'</b>';
                    },
                    dataIndex: 'nama_rincian'
                },
                {
                    text: '<b>Keb.Guru (jam)</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_kebutuhan',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: '<b>Jml.Guru (jam)</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_s1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_nons1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Honda S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_honorer_s1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return Ext.util.Format.number(Math.abs(v), '0,000') + me.persen ;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Honda < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_honorer_nons1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return Ext.util.Format.number(Math.abs(v), '0,000') + me.persen ;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'HnrLain S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_nonpns_s1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'HnrLain < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_nonpns_nons1_ada_utk_matpel',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                }];
                break;
            
            case 'sertifikasi':
                me.columns = [
                {
                    text: (me.per == 'wilayah') ? ((me.skup < 3) ? 'Wilayah' : 'Sekolah') : 'Mata Pelajaran',
                    width: 200,
                    locked: true,
                    summaryType: 'count',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';                    
                    },
                    dataIndex: 'nama_rincian'
                },
                {
                    text: '<b>Keb.Guru</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_kebutuhan',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: '<b>Jml.Guru</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_s1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_nons1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Honda S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_honorer_s1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return Ext.util.Format.number(Math.abs(v), '0,000') + me.persen ;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Honda < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_honorer_nons1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return Ext.util.Format.number(Math.abs(v), '0,000') + me.persen ;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'HnrLain S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_nonpns_s1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'HnrLain < S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                }];
                break;

            case 'bk':

                me.columns = [
                {
                    text: (me.per == 'wilayah') ? ((me.skup < 3) ? 'Wilayah' : 'Sekolah') : 'Mata Pelajaran',
                    width: 200,
                    locked: true,
                    summaryType: 'count',
                    renderer: function (v, meta, record) {
                        return v;
                    },
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        //return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';
                        //var pas = ((summaryData.data.kelebihan_guru == 0) && (summaryData.data.kelebihan_guru == 0)) ? "<font color='green'><b>(sesuai)</b></font>" : "";
                        var namawilayah = (me.nama_wilayah == 'Indonesia') ? 'Nasional' : me.nama_wilayah + ' ' + pas;
                        return '<b>Total '+ namawilayah +'</b>';
                    },
                    dataIndex: 'nama_rincian'
                },
                {
                    text: 'Jml Siswa (SMP)',
                    width: null,
                    align: 'right',
                    dataIndex: 'jml_siswarbl_total',
                    renderer: function (v, meta, record) {
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: '<b>Guru BK Minimal</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_gurubk_minimal',
                    renderer: function (v, meta, record) {
                        meta.style = "background-color:#9ACEEB;"; // Orange
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Guru BK Maksimal',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_gurubk_maksimal',
                    renderer: function (v, meta, record) {
                        meta.style = "background-color:#FFDFDF;"; // Orange
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: '<b>Guru BK Tersedia</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_gurubk',
                    renderer: function (v, meta, record) {
                        meta.style = "background-color:#EAFFEF;";  // Hijau
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') + me.persen : 0;
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Lebih',
                    width: null,
                    align: 'right',
                    dataIndex: 'kelebihan_guru',
                    renderer: function (v, meta, record) {
                        if ((record.data.kelebihan_guru === 0) && (record.data.kekurangan_guru === 0)) {
                            return "<font color='green'><b>(sesuai)</b></font>";
                        } else {
                            return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0;
                        }
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Kurang',
                    width: null,
                    align: 'right',
                    dataIndex: 'kekurangan_guru',
                    renderer: function (v, meta, record) {
                        if ((record.data.kelebihan_guru === 0) && (record.data.kekurangan_guru === 0)) {
                            return "<font color='green'><b>(sesuai)</b></font>";
                        } else {
                            return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0;
                        }
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                }];
                break;

            default:
                break;
        }

        me.listeners = {
            
            itemdblclick: function( dblGrid, record, item, index, e, eOpts ) {

                if (me.per == 'matpel') {
                    return;
                }

                // Record Last Postion
                var cp = new Ext.state.CookieProvider();
                Ext.state.Manager.setProvider(cp);

                var key = "key" + me.kode_wilayah.toString().trim();
                cp.set(key, me.store.currentPage);

                //console.log(cp.state);
                // Record end

                Ext.Ajax.request({
                    url: '/SearchWilayah/' + record.data.kode_rincian,
                    success: function(response){
                        
                        var text = response.responseText;
                        var res = Ext.JSON.decode(response.responseText);

                        me.kode_wilayah = res.kode_wilayah;
                        me.nama_wilayah = res.nama;
                        me.skup = res.id_level_wilayah;
                        me.mst_kode_wilayah = res.mst_kode_wilayah;


                        //////////// PAGING SHIT ////////////////////////
                        // Get the page of the new scope on the cookie //
                        var key = "key" + me.kode_wilayah.toString().trim();
                        var page = cp.get(key,1);
                        
                        dblGrid.getStore().currentPage = page;
                        dblGrid.getStore().loadPage(page);

                        setTimeout(function(){
                            //console.log('Doubleclick executed.');
                            //console.log('Type: ' + typeof cp.get(key));
                            //console.log('State: ' + cp.get(key));
                            //console.log('Current page ' + dblGrid.getStore().currentPage);
                            //console.log('New page ' + page);
                            //console.log(cp.state);
                        }, 2000);

                        // The component fires event. Parameters passed via "this" object 
                        me.fireEvent('changewilayah', me);

                    }
                });

                // Actions.Controller.search_wilayah ({ kode_wilayah: record.data.kode_rincian }, function(result, ev) {

                //     var res = Ext.JSON.decode(result);

                   //  me.kode_wilayah = res.kode_wilayah;
                   //  me.nama_wilayah = res.nama;
                   //  me.skup = res.id_level_wilayah;
                   //  me.mst_kode_wilayah = res.mst_kode_wilayah;

                   //  dblGrid.getStore().reload();

                   //  // The component fires event. Parameters passed via "this" object 
                   //  me.fireEvent('changewilayah', me);
                // });
                
            }
        };

        me.store.on('beforeload', me.beforeLoad, me);

        me.store.load();

        this.callParent();

        if (me.purpose == 'bk') {
            me.down('combo_matpel').hide();
            // me.down('button[text="Daftar PTK"]').hide();
            // me.down('button[text="Cetak"]').hide();
            // me.down('button[text="Download"]').hide();
        }
    }
});

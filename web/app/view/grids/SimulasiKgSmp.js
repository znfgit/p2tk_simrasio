Ext.define('SimptkRasio.view.grids.SimulasiKgSmp', {
    extend: 'Ext.Panel',
    xtype: 'simulasi_kg_smp',
    layout: 'fit',
    require: [
        'Ext.grid.feature.Summary'
    ],
    reloadSimulasiGrid: function() {
        
        me = this;
        kgGrid = me.simulasiKgGrid;

        var values = me.down('form_filter_simulasi_kg').getForm().getValues();
        values.kode_wilayah = me.down('grid_wilayah').kodeWilayah;
        values.level_wilayah = me.down('grid_wilayah').levelWilayah;

        var nominasiButton = kgGrid.down('toolbar').down('button[itemId="nominasi_mutasi"]');

        me.simulasiKgGrid.store.load({
            params: values
        });

    },

    initComponent: function() {
        
        var me = this;

        var kgStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'perhitungan_rasio_id', type: 'int'  } ,
                { name: 'mata_pelajaran_id', type: 'int'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'bentuk_pendidikan_id', type: 'int'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'tahun_ajaran_id', type: 'float'  } ,
                { name: 'mp7', type: 'float' } ,
                { name: 'k7', type: 'float' } ,
                { name: 'mp8', type: 'float' } ,
                { name: 'k8', type: 'float' } ,
                { name: 'mp9', type: 'float' } ,
                { name: 'k9', type: 'float' } ,
                { name: 'jr', type: 'float' } ,
                { name: 'jumlah_siswa', type: 'float' } ,                
                // { name: 'jtm', type: 'float' } ,
                // { name: 'rasio_gk', type: 'float' } ,
                { name: 'kg_jam', type: 'float' } ,
                { name: 'kg_org', type: 'float' } ,
                { name: 'gt_jam', type: 'float' } ,
                { name: 'gt_org', type: 'float' } ,
                { name: 'lebih_jam', type: 'float' } ,
                { name: 'kurang_jam', type: 'float' } ,
                { name: 'lebih_org', type: 'float' } ,
                { name: 'kurang_org', type: 'float' } 
            ],
            idProperty: 'perhitungan_rasio_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/PerhitunganKgSmp',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });

        var kgGrid = new Ext.grid.GridPanel({
            width: 300,
            flex: 5,
            padding: '20px 0 0 0',
            anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel(),
            store: kgStore,
            features: [{
                ftype: 'summary'
            }],
            tbar: [{
                text: 'Reload',
                glyph: '61666@font-awesome',
                // hidden: true,
                itemId: 'simulasi_kg_smp_reload',                
                listeners: {
                    
                    click: function(btn){

                        me.reloadSimulasiGrid();

                        // var values = me.down('form_filter_simulasi_kg').getForm().getValues();
                        // values.kode_wilayah = me.down('grid_wilayah').kodeWilayah;
                        // values.level_wilayah = me.down('grid_wilayah').levelWilayah;

                        // var nominasiButton = kgGrid.down('toolbar').down('button[itemId="nominasi_mutasi"]');

                        // // console.log(nominasiButton);
                        // // console.log(values.kodeWilayah);
                        // // console.log(values.levelWilayah);
                        // // console.log(values);

                        // // if (values.level_wilayah == 4){
                        // //     nominasiButton.setVisible(true);
                        // // } else {
                        // //     nominasiButton.setVisible(false);
                        // // }

                        // // console.log(values);
                        // // kgStore.on('beforeload', function(store){
                        // //  store.getProxy().setExtraParams(values);
                        // // });
                        // kgStore.load({
                        //     params: values
                        // });
                    }
                }
            },{
                text: 'Daftar PTK',
                itemId: 'daftar_ptk',
                hidden: true,
                glyph: '61632@font-awesome',
                listeners: {
                    
                    click: function(btn){

                        // var sels = kgGrid.getSelectionModel().getSelections();
                        // var sel = sels[0];

                        var selections = kgGrid.getSelectionModel().getSelection();
                        if (selections.length <= 0) {
                            Ext.Msg.alert('Error', 'Mohon pilih salah satu jenis guru di tabel bawah..');
                            return;
                        }

                        if (selections) {
                            var r = selections[0];

                            var kode_wilayah = me.down('grid_wilayah').kodeWilayah;
                            var level_wilayah = me.down('grid_wilayah').levelWilayah;
                            var namaSekolah = me.down('grid_wilayah').kodeWilayahStr;
                            var namaMatpel = r.data.nama;

                            var title = 'Daftar Guru ' + namaMatpel + ' di ' + namaSekolah;
                            
                            var win = new Ext.widget({
                                xtype: 'window_list_ptk',
                                title: title,
                                width: 900,
                                height: 500,
                                bentuk_pendidikan_id: 6,
                                level_wilayah: level_wilayah,
                                sekolah_id: kode_wilayah,
                                nama_sekolah: namaSekolah,
                                nama_matpel: namaMatpel,
                                mata_pelajaran_id: r.data.mata_pelajaran_id
                            });
                            
                            win.show();
                        } else {
                            
                        }
                    }
                }
            },{
                text: 'Nominasi Keluar',
                itemId: 'nominasi_mutasi',
                hidden: true,
                glyph: '61587@font-awesome',
                listeners: {
                    click: function(btn){

                        // var sels = kgGrid.getSelectionModel().getSelections();
                        // var sel = sels[0];

                        var selections = kgGrid.getSelectionModel().getSelection();
                        
                        if (selections.length <= 0) {
                            Ext.Msg.alert('Error', 'Mohon pilih salah satu jenis guru di tabel bawah..');
                            return;
                        }

                        if (selections) {
                            var r = selections[0];
                            console.log(r);

                            var kode_wilayah = me.down('grid_wilayah').kodeWilayah;
                            var level_wilayah = me.down('grid_wilayah').levelWilayah;
                            var namaSekolah = me.down('grid_wilayah').kodeWilayahStr;
                            var namaMatpel = r.data.nama;

                            var title = 'Nominasi Mutasi Guru ' + namaMatpel + ' di ' + namaSekolah; 
                            
                            // var win = new Ext.widget({
                            //     xtype: 'window_nominasi_mutasi',
                            //     title: title,
                            //     width: 800,
                            //     height: 500,
                            //     sekolah_id: kode_wilayah,
                            //     mata_pelajaran_id: r.data.mata_pelajaran_id
                            // });
                            
                            if (level_wilayah < 4) {
                                win = new Ext.widget({
                                    xtype: 'window_list_keluar',
                                    title: title,
                                    width: 900,
                                    height: 500,
                                    bentuk_pendidikan_id: 6,
                                    level_wilayah: level_wilayah,
                                    sekolah_id: kode_wilayah,
                                    nama_sekolah: namaSekolah,
                                    nama_matpel: namaMatpel,
                                    mata_pelajaran_id: r.data.mata_pelajaran_id
                                });
                            } else {
                                win = new Ext.widget({
                                    xtype: 'window_nominasi_mutasi',
                                    title: title,
                                    width: 900,
                                    height: 500,
                                    bentuk_pendidikan_id: 6,
                                    level_wilayah: level_wilayah,
                                    sekolah_id: kode_wilayah,
                                    nama_sekolah: namaSekolah,
                                    nama_matpel: namaMatpel,
                                    mata_pelajaran_id: r.data.mata_pelajaran_id
                                });
                            }

                            win.show();
                        } else {
                            Ext.Msg.alert('Error', 'Mohon pilih salah satu jenis guru di tabel bawah..');
                        }
                    }
                }
            },{
                text: 'Unduh',
                glyph: '57413@font-icomoon',
                itemId: 'unduhExcel',
                handler: function(btn){

                    var module = btn.up('simulasi_kg_smp');
                    me = module;
                    kgGrid = me.simulasiKgGrid;

                    var values = me.down('form_filter_simulasi_kg').getForm().getValues();
                    values.kode_wilayah = me.down('grid_wilayah').kodeWilayah;
                    values.level_wilayah = me.down('grid_wilayah').levelWilayah;

                    //var nominasiButton = kgGrid.down('toolbar').down('button[itemId="nominasi_mutasi"]');

                    console.log('Downloading..');
                    console.log(values);
                    
                    var queryString = Ext.urlEncode(values);
                    window.location.assign('UnduhPerhitunganKgSmp?' + queryString);


                }
            }],
            columns: [{
                header: 'Uraian',
                columns: [{

                    header: 'No',
                    width: 45,
                    sortable: true, 
                    hidden: true,
                    dataIndex: 'perhitungan_rasio_id'
                },{                
                    header: 'Jenis Guru',
                    width: 260,
                    sortable: true,
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return "<b>Total</b>";
                    },
                    dataIndex: 'nama'
                }]
            },{
                header: 'Lebih/Kurang',
                columns: [{
                    header: '+(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_jam'
                },{                
                    header: '+(Org)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan orang)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_org'

                },{                
                    header: '-(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kekurangan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_jam'
                },{                
                    header: '-(Org)',
                    width: 90,
                    align: 'right',
                    tooltip : 'Kekurangan Guru (dalam satuan orang)', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_org'
                }]
            },{                
                header: 'Data',
                columns: [{
                    header: 'Jam/MP',
                    width: 80,
                    tooltip : 'Jumlah Jam per Matpel sesuai kurikulum', 
                    align: 'right', 
                    sortable: true, 
                    dataIndex: 'mp7'
                },{                
                    header: 'K7',
                    width: 60,              
                    sortable: true,
                    tooltip : 'Jumlah kelas di tingkat tujuh', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    dataIndex: 'k7'
                },{                
                    header: 'MP8',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp8'

                },{                
                    header: 'K8',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat delapan', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k8'
                },{                
                    header: 'MP9',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp9'

                },{                
                    header: 'K9',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat sembilan', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k9'
                },{                
                    header: 'JR',
                    align: 'right',
                    tooltip : 'Jumlah rombel total', 
                    width: 60,
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'jr'
                }]
            },{                
                header: 'Perhitungan',
                columns: [{
                    header: 'KG(Jam)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_jam'
                },{                
                    header: 'GT(Jam)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_jam'

                },{                
                    header: 'KG(Org)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_org'
                },{                
                    header: 'GT(Org)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_org'
                }]
            }]
        });

        me.items = {
            //xtype: 'tabpanel',
            //tabPosition: 'bottom',
            xtype: 'container',

            //height: 250,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            
            items: {
                xtype: 'panel',
                title: 'Simulasi KG SMP',
                flex: 2,
                layout: {
                    type: 'vbox',
                    align: 'stretch'        
                },
                items: [{
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'        
                    },
                    items:[{
                        xtype: 'grid_wilayah',
                        jenjang: 'smp',
                        status_sekolah: 1,
                        width: 400,
                        height: 200
                    },{
                        xtype: 'form_filter_simulasi_kg',
                        height: 200,
                        width: 500
                    }]
                },
                    kgGrid
                ]
            }
            //items: [myGrid]
            //items: [myChart]
        }

        me.simulasiKgGrid = kgGrid;

        me.on('render', function(){

            var grid = me.down('grid');

            grid.getStore().on('load', function(){

                console.log('grid loaded... '); 
                console.log(me); 

                if (!me.down('form_filter_simulasi_kg')) {
                    console.log('form not found');
                    return;
                }

                console.log('getting values from form & grid wilayah');


                var values = me.down('form_filter_simulasi_kg').getForm().getValues();
                values.kode_wilayah = me.down('grid_wilayah').kodeWilayah;
                values.level_wilayah = me.down('grid_wilayah').levelWilayah;

                console.log(values.kode_wilayah);
                console.log(values.level_wilayah);

                var nominasiButton = kgGrid.down('toolbar').down('button[itemId="nominasi_mutasi"]');
                var daftarPtkButton = kgGrid.down('toolbar').down('button[itemId="daftar_ptk"]');
                var reloadButton = kgGrid.down('toolbar').down('button[itemId="simulasi_kg_smp_reload"]');

                if (values.level_wilayah > 0){
                    nominasiButton.setVisible(true);
                    daftarPtkButton.setVisible(true);
                    reloadButton.setVisible(true);
                } else {
                    nominasiButton.setVisible(false);
                    daftarPtkButton.setVisible(false);
                    reloadButton.setVisible(true);
                }
                me.reloadSimulasiGrid();
            });

            me.reloadSimulasiGrid();
            // console.log(nominasiButton);
            // console.log(values.kodeWilayah);
            // console.log(values.levelWilayah);
            // console.log(values);

            // if (values.level_wilayah == 4){
            //     nominasiButton.setVisible(true);
            // } else {
            //     nominasiButton.setVisible(false);
            // }

        });

        this.callParent();
    }
});
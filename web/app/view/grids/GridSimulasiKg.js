Ext.define('SimptkRasio.view.grids.GridSimulasiKg', {
    extend: 'Ext.Panel',
    xtype: 'grid_simulasi_kg',
    layout: 'fit',

    requires: [
        'SimptkRasio.view.components.WindowPrint',
        'Ext.grid.feature.Summary'
    ],
    
    user_kode_wilayah: null,
    user_nama_wilayah: null,
    user_mst_kode_wilayah: null,
    kode_wilayah: null,
    nama_wilayah:  null,
    mst_kode_wilayah: null,
    nama_mst_wilayah: null,
    skup: null,
    mata_pelajaran_id: null, 
    columns: null,
    tbar: null,

    // The 'global' var used no longer global, but as dynamic properties inside this component using getters
    
    // Getter for MataPelajaran Id
    getMataPelajaranId: function() {
        return this.mata_pelajaran_id;
    },

    // Getter for KodeWilayah. 
    getKodeWilayah: function() {
        return this.kode_wilayah;
    },

    // Always update extraParams to the newest values upon load. Don't forget the scope
    beforeLoad: function() {

        me = this;
        store = me.store;

        var form = me.up().down('form_filter_simulasi_kg');
        var proxy = store.getProxy();

        var values = {};

        // Formnya belum jadi, belum dirender
        if (!form) {
            
            values = {
                level_wilayah: 0,
                rsg: 1,
                bm: 24
            }

        } else {
            
            values = form.getForm().getValues();

        }

        values.kode_wilayah = me.kode_wilayah;
        values.level_wilayah = me.skup;
        values.mata_pelajaran_id = me.mata_pelajaran_id;


        // var values = me.down('form_filter_simulasi_kg').getForm().getValues();
        // values.kode_wilayah = me.down('grid_wilayah').kodeWilayah;
        // values.level_wilayah = me.down('grid_wilayah').levelWilayah;

        // var nominasiButton = kgGrid.down('toolbar').down('button[itemId="nominasi_mutasi"]');

        // // console.log(nominasiButton);
        // // console.log(values.kodeWilayah);
        // // console.log(values.levelWilayah);
        // // console.log(values);

        // if (values.level_wilayah == 4){
        //     nominasiButton.setVisible(true);
        // } else {
        //     nominasiButton.setVisible(false);
        // }

        // // console.log(values);
        // // kgStore.on('beforeload', function(store){
        // //  store.getProxy().setExtraParams(values);
        // // });
        // kgStore.load({
        //     params: values
        // });

        proxy.setExtraParams(values);

    },

    // Constructor shit
    initComponent: function() {

        me = this;

        me.addStateEvents('changewilayah');

        // Init tools
        me.columns = [];
        me.tbar = [];
        me.persen = '';

        // Init vars
        me.user_kode_wilayah = SimptkRasio.user_kode_wilayah;
        me.user_nama_wilayah = SimptkRasio.user_nama_wilayah;
        me.user_mst_kode_wilayah = SimptkRasio.user_mst_kode_wilayah;
        me.user_skup = SimptkRasio.user_skup;

        me.kode_wilayah = SimptkRasio.user_kode_wilayah;
        me.nama_wilayah =  SimptkRasio.user_nama_wilayah;
        me.mst_kode_wilayah = SimptkRasio.user_mst_kode_wilayah;
        me.nama_mst_wilayah = null;
        me.skup = SimptkRasio.user_skup;
        me.per = 'wilayah';

        me.mata_pelajaran_id = (this.jenjang == 'sd') ? 4020 : 4200; 
        me.store_url = (this.jenjang == 'sd') ? '/PerhitunganKgSdWilayah' : '/PerhitunganKgSmpWilayah'; 
        

        console.log('Skup ' + me.skup);

        if (me.jenjang == 'sd') {

            me.store = Ext.create('Ext.data.Store', {
                fields: [
                    { name: 'perhitungan_rasio_id', type: 'int'  } ,
                    { name: 'kode_wilayah', type: 'string'  } ,
                    { name: 'mst_kode_wilayah', type: 'string'  } ,
                    { name: 'mata_pelajaran_id', type: 'int'  } ,
                    { name: 'nama', type: 'string'  } ,
                    { name: 'bentuk_pendidikan_id', type: 'int'  } ,
                    { name: 'sekolah_id', type: 'string'  } ,
                    { name: 'tahun_ajaran_id', type: 'float'  } ,
                    { name: 'mp1', type: 'float' } ,
                    { name: 'k1', type: 'float' } ,
                    { name: 'mp2', type: 'float' } ,
                    { name: 'k2', type: 'float' } ,
                    { name: 'mp3', type: 'float' } ,
                    { name: 'k3', type: 'float' } ,
                    { name: 'mp4', type: 'float' } ,
                    { name: 'k4', type: 'float' } ,
                    { name: 'mp5', type: 'float' } ,
                    { name: 'k5', type: 'float' } ,
                    { name: 'mp6', type: 'float' } ,
                    { name: 'k6', type: 'float' } ,
                    { name: 'k6', type: 'float' } ,
                    { name: 'jr', type: 'float' } ,
                    { name: 'jumlah_siswa', type: 'float' } ,                
                    // { name: 'jtm', type: 'float' } ,
                    // { name: 'rasio_gk', type: 'float' } ,
                    { name: 'kg_jam', type: 'float' } ,
                    { name: 'kg_org', type: 'float' } ,
                    { name: 'gt_jam', type: 'float' } ,
                    { name: 'gt_org', type: 'float' } ,
                    { name: 'lebih_jam', type: 'float' } ,
                    { name: 'kurang_jam', type: 'float' } ,
                    { name: 'lebih_org', type: 'float' } ,
                    { name: 'kurang_org', type: 'float' } 
                ],
                idProperty: 'perhitungan_rasio_id',
                proxy: {
                    // load using HTTP
                    type: 'ajax',
                    url: me.store_url,
                    autoLoad: true,
                    reader: {
                        type: 'json',
                        rootProperty: 'rows',
                        totalProperty  : 'results',
                        successProperty: false
                    }
                }
            });
        } else {
            me.store = Ext.create('Ext.data.Store', {
                fields: [
                    { name: 'perhitungan_rasio_id', type: 'int'  } ,
                    { name: 'kode_wilayah', type: 'string'  } ,
                    { name: 'mst_kode_wilayah', type: 'string'  } ,
                    { name: 'mata_pelajaran_id', type: 'int'  } ,
                    { name: 'nama', type: 'string'  } ,
                    { name: 'bentuk_pendidikan_id', type: 'int'  } ,
                    { name: 'sekolah_id', type: 'string'  } ,
                    { name: 'tahun_ajaran_id', type: 'float'  } ,
                    { name: 'mp7', type: 'float' } ,
                    { name: 'k7', type: 'float' } ,
                    { name: 'mp8', type: 'float' } ,
                    { name: 'k8', type: 'float' } ,
                    { name: 'mp9', type: 'float' } ,
                    { name: 'k9', type: 'float' } ,
                    { name: 'jr', type: 'float' } ,
                    { name: 'jumlah_siswa', type: 'float' } ,                
                    // { name: 'jtm', type: 'float' } ,
                    // { name: 'rasio_gk', type: 'float' } ,
                    { name: 'kg_jam', type: 'float' } ,
                    { name: 'kg_org', type: 'float' } ,
                    { name: 'gt_jam', type: 'float' } ,
                    { name: 'gt_org', type: 'float' } ,
                    { name: 'lebih_jam', type: 'float' } ,
                    { name: 'kurang_jam', type: 'float' } ,
                    { name: 'lebih_org', type: 'float' } ,
                    { name: 'kurang_org', type: 'float' } 
                ],
                idProperty: 'perhitungan_rasio_id',
                proxy: {
                    // load using HTTP
                    type: 'ajax',
                    url: me.store_url,
                    autoLoad: true,
                    reader: {
                        type: 'json',
                        rootProperty: 'rows',
                        totalProperty  : 'results',
                        successProperty: false
                    }
                }
            });
        }

        me.toolbar = [{
            xtype: 'button',
            text: 'Wilayah Induk',                
            //cls: 'addbutton',
            glyph: '61714@font-awesome',
            scope: this,
            action: 'add',
            handler: function(btn) {
                
                if (me.mst_kode_wilayah == '') {
                    return;
                }

                if (me.mst_kode_wilayah == me.user_mst_kode_wilayah) {
                    return;
                }

                Ext.Ajax.request({
                    url: '/SearchWilayah/' + me.mst_kode_wilayah,
                    success: function(response){
                        
                        var text = response.responseText;
                        var res = Ext.JSON.decode(response.responseText);

                        me.kode_wilayah = res.kode_wilayah;
                        me.nama_wilayah = res.nama;
                        me.skup = res.id_level_wilayah;
                        me.mst_kode_wilayah = res.mst_kode_wilayah;

                        me.store.reload();

                        // The component fires event. Parameters passed via "this" object 
                        me.fireEvent('changewilayah', me);

                    }
                });

                // Actions.Controller.search_wilayah ({ kode_wilayah: me.mst_kode_wilayah}, function(result, ev) {

                //     var res = Ext.JSON.decode(result);
                //     me.kode_wilayah = res.kode_wilayah;
                //     me.nama_wilayah = res.nama;
                //     me.skup = res.id_level_wilayah;
                //     me.mst_kode_wilayah = res.mst_kode_wilayah;

                //     me.store.reload();

                //     // The component fires event. Parameters passed via "this" object 
                //     me.fireEvent('changewilayah', me);
                // });

            }
        },{
            xtype: 'combo_matpel',
            width: 200,
            initialValue: me.mata_pelajaran_id,
            jenjang: me.jenjang,
            listeners: {
                select: function(combo) {
                    
                    me.mata_pelajaran_id = combo.getValue();
                    me.store.reload();

                }
            }
        },
            Ext.create('Ext.PagingToolbar', {
                store: me.store,
                displayInfo: true,
                displayMsg: 'Data #{0} - {1} dari {2}',
                emptyMsg: "Tidak ada data"
            })
        , '->',  {
            text: 'Cetak',
            glyph: '61487@font-awesome',
            handler: function(btn) {
                
                var form = me.up().down('form_filter_simulasi_kg');
                var values = {};
                values = form.getForm().getValues();

                values.kode_wilayah = me.kode_wilayah;
                values.nama_wilayah = me.nama_wilayah;
                values.level_wilayah = me.skup;
                values.per = me.per;
                values.mata_pelajaran_id = me.mata_pelajaran_id;

                var route = (me.jenjang == 'sd' ? '/PrintingSimulasiSdPerWilayah?' : '/PrintingSimulasiSmpPerWilayah?')
                var reportUrl =  route + Ext.Object.toQueryString(values);
                console.log(reportUrl);

                var win = new Ext.widget({
                    xtype: 'window_print',
                    title: 'Cetak',
                    src: reportUrl
                });
                
                win.show();
                
            }
        },{
            text: 'Download',
            // glyph: '57517@font-fileformats-icons',
            glyph: '57413@font-icomoon',
            handler: function(btn) {
                
                var form = me.up().down('form_filter_simulasi_kg');
                var values = {};
                values = form.getForm().getValues();

                values.kode_wilayah = me.kode_wilayah;
                values.nama_wilayah = me.nama_wilayah;
                values.level_wilayah = me.skup;
                values.per = me.per;
                values.mata_pelajaran_id = me.mata_pelajaran_id;

                var route = (me.jenjang == 'sd' ? '/ExcelSimulasiSdPerWilayah?' : '/ExcelSimulasiSmpPerWilayah?')
                var reportUrl =  route + Ext.Object.toQueryString(values);
                
                window.location.assign(reportUrl);

            }
        }];

        if (me.jenjang == 'sd') {
            me.columns = [{
                header: 'Uraian',
                columns: [{
                    header: 'No',
                    width: 45,
                    sortable: true, 
                    hidden: true,
                    dataIndex: 'perhitungan_rasio_id'
                },{ 
                    header: (me.skup < 3) ? 'Wilayah' : 'Sekolah',
                    width: 260,
                    sortable: true,
                    summaryType: 'count',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';                    
                    },
                    dataIndex: 'nama'
                }]
            },{
                header: 'Lebih/Kurang',
                columns: [{
                    header: '+(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_jam'
                },{                
                    header: '+(Org)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan orang)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_org'

                },{                
                    header: '-(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kekurangan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_jam'
                },{                
                    header: '-(Org)',
                    width: 90,
                    align: 'right',
                    tooltip : 'Kekurangan Guru (dalam satuan orang)', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_org'
                }]
            },{                
                header: 'Data',
                columns: [{
                    header: 'Jam/MP',
                    width: 80,
                    tooltip : 'Jumlah Jam per Matpel sesuai kurikulum', 
                    align: 'right', 
                    sortable: true, 
                    dataIndex: 'mp1'
                },{                
                    header: 'K1',
                    width: 60,              
                    sortable: true,
                    tooltip : 'Jumlah kelas di tingkat satu', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    dataIndex: 'k1'
                },{                
                    header: 'MP2',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp2'
                },{                
                    header: 'K2',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat dua', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k2'
                },{                
                    header: 'MP3',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp3'
                },{                
                    header: 'K3',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat tiga', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k3'
                },{                
                    header: 'MP4',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp4'
                },{                
                    header: 'K4',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat empat', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k4'
                },{                
                    header: 'MP5',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp5'
                },{                
                    header: 'K5',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat lima', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k5'
                },{                
                    header: 'MP6',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp6'
                },{                
                    header: 'K6',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat enam', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k6'

                },{                
                    header: 'JR',
                    align: 'right',
                    tooltip : 'Jumlah rombel total', 
                    width: 60,
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'jr'
                }]
            },{                
                header: 'Perhitungan',
                columns: [{
                    header: 'KG(Jam)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_jam'
                },{                
                    header: 'GT(Jam)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_jam'

                },{                
                    header: 'KG(Org)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_org'
                },{                
                    header: 'GT(Org)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_org'
                }]
            }];
        
        } else {

            me.columns = [{
                header: 'Uraian',
                columns: [{
                    header: 'No',
                    width: 45,
                    sortable: true, 
                    hidden: true,
                    dataIndex: 'perhitungan_rasio_id'
                },{ 
                    header: (me.skup < 3) ? 'Wilayah' : 'Sekolah',
                    width: 260,
                    sortable: true, 
                    summaryType: 'count',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';
                    },                    
                    dataIndex: 'nama'
                }]
            },{
                header: 'Lebih/Kurang',
                columns: [{
                    header: '+(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_jam'
                },{                
                    header: '+(Org)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kelebihan Guru (dalam satuan orang)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#D0E6FF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'lebih_org'

                },{                
                    header: '-(Jam)',
                    width: 90,
                    align: 'right', 
                    tooltip : 'Kekurangan Guru (dalam satuan jam)',
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_jam'
                },{                
                    header: '-(Org)',
                    width: 90,
                    align: 'right',
                    tooltip : 'Kekurangan Guru (dalam satuan orang)', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFFF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    },
                    sortable: true, 
                    dataIndex: 'kurang_org'
                }]
            },{                
                header: 'Data',
                columns: [{
                header: 'Jam/MP',
                    width: 80,
                    tooltip : 'Jumlah Jam per Matpel sesuai kurikulum', 
                    align: 'right', 
                    sortable: true, 
                    dataIndex: 'mp7'
                },{                
                    header: 'K7',
                    width: 60,              
                    sortable: true,
                    tooltip : 'Jumlah kelas di tingkat tujuh', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    dataIndex: 'k7'
                },{                
                    header: 'MP8',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp8'

                },{                
                    header: 'K8',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat delapan', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k8'
                },{                
                    header: 'MP9',
                    width: 60,
                    hidden: true,
                    sortable: true, 
                    dataIndex: 'mp9'

                },{                
                    header: 'K9',
                    width: 60,
                    tooltip : 'Jumlah kelas di tingkat sembilan', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'k9'
                },{                
                    header: 'JR',
                    align: 'right',
                    tooltip : 'Jumlah rombel total', 
                    width: 60,
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'jr'
                }]
            },{                
                header: 'Perhitungan',
                columns: [{
                    header: 'KG(Jam)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_jam'
                },{                
                    header: 'GT(Jam)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan jam)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_jam'

                },{                
                    header: 'KG(Org)',
                    width: 90,
                    tooltip : 'Kebutuhan Guru (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#FFDFDF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'kg_org'
                },{                
                    header: 'GT(Org)',
                    width: 90,
                    tooltip : 'Guru Tersedia (dalam satuan orang)', 
                    align: 'right', 
                    renderer: function (v, meta, record) { 
                        meta.style = "background-color:#EAFFEF;";
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    sortable: true, 
                    dataIndex: 'gt_org'
                }]
            }];

        }  

        me.items = {
            
            xtype: 'grid',
            flex: 3,
            width: '100%',
            autoScroll: true,
            store: me.store,
            features: [{
                ftype: 'summary'
            }],
            defaults: {
                sortable: true
            },
            listeners: {
                
                itemdblclick: function( grid, record, item, index, e, eOpts ) {
                    
                    console.log(record.data); //return;
                    console.log(record.data.kode_wilayah); //return;
                    console.log('/SearchWilayah/' + record.data.kode_wilayah); //return;
                    

                    Ext.Ajax.request({
                        url: '/SearchWilayah/' + record.data.kode_wilayah,
                        success: function(response){

                            var text = response.responseText;
                            var res = Ext.JSON.decode(response.responseText);

                            me.kode_wilayah = res.kode_wilayah;
                            me.nama_wilayah = res.nama;
                            me.skup = res.id_level_wilayah;
                            me.mst_kode_wilayah = res.mst_kode_wilayah;

                            me.store.reload();

                            // The component fires event. Parameters passed via "this" object 
                            me.fireEvent('changewilayah', me);

                        }
                    });
                    
                    
                    // Actions.Controller.search_wilayah ({ kode_wilayah: record.data.kode_wilayah }, function(result, ev) {

                    //     var res = Ext.JSON.decode(result);

                    //     me.kode_wilayah = res.kode_wilayah;
                    //     me.nama_wilayah = res.nama;
                    //     me.skup = res.id_level_wilayah;
                    //     me.mst_kode_wilayah = res.mst_kode_wilayah;

                    //     me.store.reload();

                    //     // The component fires event. Parameters passed via "this" object 
                    //     me.fireEvent('changewilayah', me);
                    // });
                    


                }
            },
            tbar: me.toolbar,
            columns: me.columns
        };

        me.store.on('beforeload', me.beforeLoad, me); 

        me.store.load();

        this.callParent();
    }
});

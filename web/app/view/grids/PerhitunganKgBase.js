Ext.define('SimptkRasio.view.grids.PerhitunganKgBase', {
    extend: 'Ext.Panel',

    /* Standard configs */
    title: 'Perhitungan Kebutuhan Guru SD',
    //border: true,
    //bodyStyle: 'padding: 5px;',
    //layout: 'vbox',
    //align:'stretch',
    layout: {
        type:'vbox',
        align:'stretch'
    },
    //width: 1000,
    //height: 500,
    //modal: true,
    viewConfig: {forceFit: true},
    
    /* Custom configs */
    gridPerhitunganRasio: null, 
    ds: null,
    cm: null,
    rsg_menu: null,
    bm_menu: null,
    rsgbk_menu: null,
    
    //jenjang: null,
    sekolah_selected: null,
    kabkota_selected_nama: null,
    propinsi_selected_nama: null,
    rsg_selected: "Eksisting",
    bk_selected: "24",
    rsgbk_selected: "150",
    smt: "2",
    tahun: "2013",
    filter_pns: 1,
    filter_pensiun: 1,
    //updateInfoPanel();
    

    initComponent: function(){      
        
        var me = this;

        this.jenjang_id = "";
        this.rsg_menu = "";
        this.bm_menu = "";
        this.rsgbk_menu = "";
        
        switch (me.jenjang) {
            case 'sd':
                me.jenjang_id = 5;
                break;
            case 'smp':
                me.jenjang_id = 6;
                break;              
        }
        
        // //Ext.apply(this, config);
        
        // RecordSetBidangStudi = [
        //     { name: 'bidang_studi_id', type: 'float'  } ,
        //     { name: 'kode', type: 'string'  } ,
        //     { name: 'nama', type: 'string'  } ,
        //     { name: 'tingkat', type: 'string'  }     
        // ];

        // RecordObjBidangStudi = Ext.data.Record.create(RecordSetBidangStudi);

        // StoreBidangStudi = new Ext.data.Store({    
        //     reader: new Ext.data.JsonReader({
        //         id: 'BidangStudiId',
        //         root: 'rows',
        //         totalProperty: 'results',
        //         fields: RecordSetBidangStudi     
        //     }),
        //     //url: './BidangStudi/fetchBidangStudi.json',
        //     autoLoad: false,        
        //     url: './data/BidangStudi.json',         
        //     sortInfo: {field: 'BidangStudiId', direction: "ASC"}
        // });

        // ///////////////////////////
        // RecordSetSearchSekolah = [
        //     { name: 'sekolah_id', type: 'string'  } ,
        //     { name: 'Nama', type: 'string'  } 
        // ];
           
        // RecordObjSearchSekolah = Ext.data.Record.create(RecordSetSearchSekolah);

        // StoreSearchSekolah = new Ext.data.Store({
        //     reader: new Ext.data.JsonReader({
        //         id: 'sekolah_id',
        //         root: 'rows',
        //         totalProperty: 'results',
        //         fields: RecordSetSearchSekolah       
        //     }),
        //     //url: './Sekolah/fetchSekolah.json',
        //     autoLoad: false,        
        //     url: './data/Sekolah.json',         
        //     sortInfo: {field: 'sekolah_id', direction: "ASC"}
        // });     
        
        // function onItemCheck(item, checked){
        //     //Ext.example.msg('Item Check', 'You {1} the "{0}" menu item.', item.text, checked ? 'checked' : 'unchecked');
        // }

        var menuCetak = new Ext.menu.Menu({                     
            items: [{
                text: 'Cetak Format 1',
                group: 'theme',
                iconCls: 'files',
                handler: function(){                                
                     var cetakVerWin = new Xond.PrintPreview({
                      title : 'Cetak Format I',
                      previewUrl : './PerhitunganRasio/process.php/preview',
                      printUrl : './PerhitunganRasio/process.php/print',
                      downloadUrl : './PerhitunganRasio/process.pdf/print'
                     });                     
                     cetakVerWin.show();                
                }
            },{
                text: 'Cetak Rincian Perhitungan',
                group: 'files',
                iconCls: 'files',
                handler: function(){
                    var page = 1;
                    id = page;
                    
                    selected = grid.selModel.getSelectedCell();
                    if (!(selected)) {
                        Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data..');
                        return false;
                    }
                    rec = grid.getStore().getAt(selected[0]);                   
                    var monthYear = Ext.util.Format.date(rec.data.TanggalSelesai, 'mY');
                    
                    var cetakRekap = new Xond.MultipagePrintPreview({
                        title : 'Rekap Pajak',
                        startingPage : 1,
                        limitPerPage: 10,
                        pageCountUrl : './PencetakanPajak/printRekap.php/count/' + monthYear,
                        navigationUrl : './PencetakanPajak/printRekap.php/[pagenumber]/' + monthYear,
                        previewUrl : './PencetakanPajak/printRekap.php/1/' +monthYear+ '/preview',
                        printUrl : './PencetakanPajak/printRekap.php/[pagenumber]',
                        downloadUrl : './PencetakanPajak/printRekap.pdf/[pagenumber]'
                    });
                    cetakRekap.show();
                    
                }
            }]
        });

        var menuUnduh = new Ext.menu.Menu({                     
            items: [{
                text: 'Unduh Format 1',
                iconCls: 'xls',
                handler: function(){                                

                }
            },{
                text: 'Unduh Rincian Perhitungan',
                iconCls: 'xls',
                handler: function(){
                    
                }
            }]
        });
        
        function reloadKg(hitung) {
            
            //console.log(me.sekolah_selected.data.sekolah_id);
            console.log(me.gridPerhitunganRasio);

            me.gridPerhitunganRasio.getStore().reload({
                params: {
                    sekolah_id: me.sekolah_selected.data.sekolah_id,
                    smt: me.smt,
                    tahun: me.tahun,
                    rsg: me.rsg_selected,
                    bk: me.bk_selected,
                    rsgbk: me.rsgbk_selected,
                    hitung: me.hitung,
                    filter_pns: me.filter_pns,
                    filter_pensiun: me.filter_pensiun
                }
            });
        }
        
        function onItemCheckRSG(item, checked){

            //Ext.Msg.alert('Info', item.text);
            
            // IF SD
            if (me.jenjang_id == 5) {
                
                switch (item.text) {
                    case 'Eksisting' :
                        rsg_selected = "Eksisting";
                        break;
                    case 'PP 74 - 2008' : 
                        rsg_selected = "20";
                        break;
                    case 'SNP' :
                        rsg_selected = "28";
                        break;
                    case 'SPM' : 
                        rsg_selected = "32";
                        break;
                }
            
            // ELSE IF SMP
            } else if (me.jenjang_id == 6) {
                
                switch (item.text) {
                    case 'Eksisting' :
                        rsg_selected = "Eksisting";
                        break;
                    case 'PP 74 - 2008' : 
                        rsg_selected = "20";
                        break;
                    case 'SNP' :
                        rsg_selected = "32";
                        break;
                    case 'SPM' : 
                        rsg_selected = "36";
                        break;
                }
            }
            
            //Ext.Msg.alert('Info', rsg_selected);
            updateInfo(false, false, false, rsg_selected, false, false, false, false, false, false);
        }
        
        function onItemCheckBK(item, checked){
            //Ext.example.msg('Item Check', 'You {1} the "{0}" menu item.', item.text, checked ? 'checked' : 'unchecked');
            //Ext.Msg.alert('Info', item.text);
            switch (item.text) {
                case 'Ideal 24 Jam' :
                    bk_selected = "24";
                    break;
                case 'Medium 32 Jam' : 
                    bk_selected = "32";
                    break;
                case 'Maksimum 40 Jam' :
                    bk_selected = "40";
                    break;
            }
            //Ext.Msg.alert('Info', bk_selected);
            updateInfo(false, false, false, false, bk_selected, false, false, false, false, false);
        }

        function onItemCheckRSGBK(item, checked){

            switch (item.text) {
                //case 'Eksisting' :
                //  rsgbk_selected = "Eksisting";
                //  break;
                case 'Ideal' : 
                    rsgbk_selected = "150";
                    break;
                case 'Minimal' :
                    rsgbk_selected = "200";
                    break;
            }
            updateInfo(false, false, false, false, false, false, false, bk_selected, false, false);
        }
        ///////////////////

        // function updateInfo(sekolah, nama_kabkota, nama_propinsi, rsg, bk, smt, tahun, rsg_rata, rsgbk, pns, pensiun) {
        function updateInfo(sekolah, nama_wilayah, rsg, bk, smt, tahun, rsg_rata, rsgbk, pns, pensiun) {
            if (!me.sekolah_selected) {
                Ext.Msg.alert("Error", "Mohon pilih dulu sekolah sebelum melakukan setting RSG dll");
                return;
            }

            me.sekolah_selected = sekolah ? sekolah : me.sekolah_selected;
            me.wilayah_selected_nama = nama_wilayah ? nama_wilayah : SimptkRasio.nama_wilayah;
            // me.kabkota_selected_nama = nama_kabkota ? nama_kabkota : kabkota_selected_nama;
            // me.propinsi_selected_nama = nama_propinsi ? nama_propinsi : propinsi_selected_nama ;
            me.rsg_selected = rsg ? rsg : me.rsg_selected;
            me.bk_selected = bk ? bk : me.bk_selected;
            me.rsgbk_selected = rsgbk ? rsgbk : me.rsgbk_selected;
            me.smt = smt ? smt : me.smt;
            me.tahun = tahun ? tahun : me.tahun;
            me.filter_pns = pns ? pns : me.filter_pns;
            me.filter_pensiun = pensiun ? pensiun : me.filter_pensiun;
            updateInfoPanel();
        }
        
        function updateInfoPanel() {            

            var text_filter_pns = (me.filter_pns == 2) ? "Hanya menghitung guru PNS" : "Menghitung semua Guru (PNS maupun bukan)";
            var text_filter_pensiun = (me.filter_pensiun == 2) ? "tidak menghitung guru akan pensiun" : "menghitung semua guru (akan pensiun maupun belum)";

            me.infoPanel.update('<div style="padding:10px 10px 10px 10px"><table border=0 style="font-size: 9pt;">'
              + '<tr><td>Sekolah</td><td> :&nbsp;&nbsp;</td><td><b>' + me.sekolah_selected.data.nama + '</b></td></tr>'
              // + '<tr><td>Kab/Kota </td><td> :&nbsp;&nbsp;</td><td><b>' + me.sekolah_selected.data.NamaKabupatenKota + ", " + me.sekolah_selected.data.NamaPropinsi + '</b></td></tr>'   
              + '<tr><td>Wilayah </td><td> :&nbsp;&nbsp;</td><td><b>' + me.wilayah_selected_nama
              + '<tr><td>Rasio Siswa Guru </td><td> :&nbsp;&nbsp;</td><td><b>' + me.rsg_selected + ' </b></td></tr>'
              + '<tr><td>Beban Kerja </td><td> :&nbsp;&nbsp;</td><td><b>' + me.bk_selected + ' jam </b></td></tr>'
              + '<tr><td>Rasio Siswa Guru BK</td><td> :&nbsp;&nbsp;</td><td><b>' + me.rsgbk_selected + ' </b></td></tr>'
              + '<tr><td>Semester/Th.Ajaran&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td><b>'+ me.smt + '/' + me.tahun +  '</b></td></tr>'
              + '<tr><td>Filter&nbsp;&nbsp;&nbsp;&nbsp;</td><td> : </td><td>'+ text_filter_pns + ' dan ' + text_filter_pensiun +  '</td></tr>'
              + '</table></div>')
        }


        var infoPanel = new Ext.Panel({
            flex: 2,
            anchor: '100%',
            html: '<div style="padding:10px 10px 10px 10px">'                 
                  + 'Mohon pilih sekolah dulu melalui tombol "Cari Sekolah"'
                  + '</div>',           
            tbar: [{
                text: 'Pilih Sekolah',
                itemId: 'pilih_sekolah_menu',
                glyph: 61442,
                tooltip: 'Klik untuk memilih Sekolah',
                menu: {
                    items: [{
                        xtype: 'combo_wilayah',
                        width: 300,
                        listeners: {
                            select: function(combo,records,ops){ 
                                //Simptk.kode_wilayah = combo.getValue();
                                var cmbSekolah = combo.up('panel').down('combo_sekolah');
                                // console.log(records);
                                me.wilayah_selected_nama = records[0].data.nama;
                                // console.log(me.wilayah_selected_nama);
                                cmbSekolah.getStore().on('beforeload',function(store, ops){
                                    var proxy = store.getProxy();
                                    proxy.setExtraParam('kode_wilayah', combo.getValue());
                                });
                            }
                        }
                    },{
                        xtype: 'combo_sekolah',
                        width: 300,
                        listeners: {
                            select: function(combo,records,ops){ 
                                //SimptkRasio.kode_wilayah = combo.getValue();
                                //console.log(records[0]);
                                me.sekolah_selected = records[0];
                                combo.up('button[itemId="pilih_sekolah_menu"]').hideMenu();
                                updateInfoPanel();
                            }
                        }

                    }]
                        // handler: function () {
                        //     var win = new CariSekolah({
                        //         jenjang: me.jenjang,
                        //         jenjang_id: me.jenjang_id,
                        //         listeners: {
                        //             sekolahselected: function(win, sekolah){                        
                        //                 updateInfo(sekolah, sekolah.data.NamaKabupatenKota, sekolah.data.NamaPropinsi, false, false, false, false, false);
                        //             },
                        //             rasioloaded: function(win){                     
                        //                 //updateRasio();
                        //                 //console.log(win);
                        //                 //console.log('Called rasioloaded on client');
                        //                 //console.log(window.rsg_menu);
                        //                 //console.log(window.bm_menu);
                        //                 //console.log(window.rsgbk_menu);
                        //                 console.log(rsg_selected);
                                        
                                        
                        //                 var i = 1;
                        //                 me.rsg_menu.menu.items.each(function(item){
                        //                     //console.log(i);
                        //                     //console.log(item);
                        //                     switch (i) {
                        //                         case 1:
                        //                             if (rsg_selected == 'Eksisting') {
                        //                                 item.setChecked(true);
                        //                             }
                        //                             break;
                        //                         case 2:
                        //                             if (rsg_selected == 20) {
                        //                                 console.log('Select 20!');
                        //                                 item.setChecked(true);
                        //                             }                                       
                        //                             break;
                        //                         case 3:
                        //                             if (rsg_selected == 28 || rsg_selected == 32) {
                        //                                 item.setChecked(true);
                        //                             }
                        //                             break;
                        //                         case 4:
                        //                             if (rsg_selected == 32 || rsg_selected == 36) {
                        //                                 item.setChecked(true);
                        //                             }                                                                                       
                        //                             break;
                        //                     }
                        //                     i++;
                        //                 });
                        //                 console.log(bk_selected);
                                        
                        //                 var j = 1;
                        //                 me.bm_menu.menu.items.each(function(item){
                        //                     //console.log(j);
                        //                     //console.log(item);

                        //                     switch (j) {
                        //                         case 1:
                        //                             if (bk_selected == 24) {
                        //                                 item.setChecked(true);
                        //                             }
                        //                             break;
                        //                         case 2:
                        //                             if (bk_selected == 32) {
                        //                                 item.setChecked(true);
                        //                             }                                       
                        //                             break;
                        //                         case 3:
                        //                             if (bk_selected == 40) {
                        //                                 item.setChecked(true);
                        //                             }                                       
                        //                             break;
                        //                     }
                        //                     j++;
                        //                 });
                        //                 console.log(rsgbk_selected);
                        //                 var k = 1;
                                        
                        //                 me.rsgbk_menu.menu.items.each(function(item){
                        //                     //console.log(k);
                        //                     //console.log(item);

                        //                     switch (k) {
                        //                         case 1:
                        //                             if (rsgbk_selected == 150) {
                        //                                 item.setChecked(true);
                        //                             }
                        //                             break;
                        //                         case 2:
                        //                             if (rsgbk_selected == 200) {
                        //                                 item.setChecked(true);
                        //                             }
                        //                             break;
                        //                     }
                        //                     k++;
                        //                 });
                        //             }
                        //         }
                        //     });
                        //     /*
                        //     win.on('sekolahselected', function(win, sekolah){                       
                        //         updateInfo(sekolah, sekolah.data.NamaKabupatenKota, sekolah.data.NamaPropinsi, false, false, false, false, false);
                        //     });
                        //     win.on('rasioloaded', function(win){                        
                        //         //updateRasio();
                        //         console.log(win);
                        //         console.log('Nendang rasioloaded');
                        //         console.log(window.rsg_menu);
                        //         console.log(window.bm_menu);
                        //         console.log(window.rsgbk_menu);
                        //     });             
                        //     */
                            
                        //     win.show();
                        // },
                        // scope: this,
                        // iconCls: 'zoom'

                }
            },'-',{
                text: 'Filter',
                iconCls: 'filter',
                glyph: 61616,
                menu: {
                    items: [{
                        text: 'Formasi CPNS',
                        checked: false,
                        checkHandler: function( checkItem, checked ){
                            filter_pns = checked ? 2 : 1;
                            updateInfo(false, false, false, false, false, false, false, false, false, filter_pns, false);
                        }
                    },{
                        text: 'Saring guru yang akan pensiun dalam 1 thn',
                        checked: false,
                        checkHandler: function( checkItem, checked ){
                            filter_pensiun = checked ? 2 : 1;
                            updateInfo(false, false, false, false, false, false, false, false, false, false, filter_pensiun);
                            
                        }
                    }]
                }
            },'-',{
                text: 'Muat Data',
                glyph: 61473,
                tooltip: 'Klik untuk memuat data dari Aplikasi Dikdas',
                handler: function () {
                    if (!me.sekolah_selected) {
                        Ext.Msg.alert('Error', 'Mohon cari dan pilih dulu sekolah yang akan diolah');
                    } else {                        
                        reloadKg(2);
                        //StoreTPerhitunganRasio.load();
                    }
                },
                scope: this,
                iconCls: 'refresh'
            },'-',{
                text: 'RSG', 
                glyph: 61447,               
                tooltip: 'Tekan untuk memilih Rasio Siswa Guru',
                group: 'pilihrsg',
                iconCls: 'kuser-3',
                menu: {        // <-- submenu by nested config object
                    items: [
                        {
                            text: 'Eksisting',
                            checked: true,
                            group: 'pilihrsg',
                            checkHandler: onItemCheckRSG
                        }, {
                            text: 'PP 74 - 2008',
                            checked: false,
                            group: 'pilihrsg',
                            checkHandler: onItemCheckRSG
                        }, {
                            text: 'SNP',
                            checked: false,
                            group: 'pilihrsg',
                            checkHandler: onItemCheckRSG
                        }, {
                            text: 'SPM',
                            checked: false,
                            group: 'pilihrsg',
                            checkHandler: onItemCheckRSG
                        }
                    ]
                }
            },'-',{
                text: 'BM',             
                group: 'pilihbk',
                glyph: 61463,
                tooltip: 'Tekan untuk memilih Beban Mengajar tiap Guru',
                iconCls: 'status-away',
                menu: {
                    items: [
                        {
                            text: 'Ideal 24 Jam',
                            checked: true,
                            group: 'pilihbk',
                            checkHandler: onItemCheckBK
                        }, {
                            text: 'Medium 32 Jam',
                            checked: false,
                            group: 'pilihbk',
                            checkHandler: onItemCheckBK
                        }, {
                            text: 'Maksimum 40 Jam',
                            checked: false,
                            group: 'pilihbk',
                            checkHandler: onItemCheckBK
                        }
                    ]
                }
            },'-',{
                text: 'RSG BK',
                glyph: 61632,
                tooltip: 'Tekan untuk memilih Rasio Siswa Guru BK',
                group: 'pilihrsg',
                iconCls: 'user-suit',
                menu: {        // <-- submenu by nested config object
                    items: [
                        {
                            /*text: 'Eksisting',
                            checked: true,
                            group: 'pilihrsgbk',
                            checkHandler: onItemCheckRSGBK
                        }, {
                            */
                            text: 'Ideal',
                            checked: false,
                            group: 'pilihrsgbk',
                            checkHandler: onItemCheckRSGBK
                        }, {
                            text: 'Minimal',
                            checked: false,
                            group: 'pilihrsgbk',
                            checkHandler: onItemCheckRSGBK
                        }
                    ]
                }
            },'-',{
                text: 'Hitung KG',
                glyph: 61705,
                tooltip: 'Klik untuk menghitung Kebutuhan Guru',
                handler: function () {
                    
                    if (!me.sekolah_selected) {
                        Ext.Msg.alert('Error', 'Mohon cari dan pilih dulu sekolah yang akan diolah');
                    } else {                        
                        reloadKg(1);
                        //StoreTPerhitunganRasio.load();
                    }
                },
                scope: this,
                iconCls: 'calculator'
            },'-',{
                text: 'Rekap',
                glyph: 61474,
                tooltip: 'Klik untuk merekap Kebutuhan Guru',
                iconCls: 'icon-grid',
                handler: function () {
                    
                    //console.log(me.jenjang);
                    var rekapwin = new PerhitunganKgRekap({
                        jenjang: me.jenjang
                    });
                    rekapwin.show();
                    rekapwin.loadRekap();
                }
            },'-',{
                text: 'Simpan',
                glyph: 61639,
                tooltip: 'Klik untuk menyimpan pengaturan Rasio',
                iconCls: 'save',
                handler: function () {
                    
                    Ext.Ajax.request({
                        waitMsg: 'Menyimpan...',                        
                        url: 'PerhitunganRasio/saveRasio.json',                     
                        method: 'POST',     
                        params: {
                            sekolah_id: me.sekolah_selected.data.sekolah_id,
                            rasio_siswa_guru: me.rsg_selected,
                            beban_mengajar: me.bk_selected,
                            rasio_siswa_guru_bk: me.rsgbk_selected,
                            tahun_ajaran_id: me.tahun
                        },  
                        failure:function(response,options){                        
                            Ext.Msg.alert('Warning','Response : ' + response );
                        },
                        success: function(response,options){
                            var json = Ext.util.JSON.decode(response.responseText);
                            if (json.success) {                             
                                Ext.Msg.alert('OK', 'Rasio tersimpan.');                                
                            } else if (json.success == false){
                                Ext.Msg.alert('Error', json.message);
                            }
                        }               
                    });

                }           
            /*
            },'-',{
                text: 'Cetak',
                iconCls: 'print',
                menu: menuCetak
            },'-',{
                text: 'Unduh',
                iconCls: 'xls',
                menu: menuUnduh             
            },'-',{
                text: 'Submit',
                tooltip: 'Klik untuk mengirim Laporan Perhitungan KG',
                iconCls: 'up',
                handler: function () {
                    
                }
            */
            },'->',{
                text: '',
                tooltip: 'Klik untuk bantuan',
                iconCls: 'help',
                handler: function () {
                    var help = new HelpBase({
                        html: '<b>Keterangan Istilah</b><br/><br/>' +
                            '<table style="font-size:10pt;"><tr><td width=40>RSG</td><td>:&nbsp;</td><td>Rasio Siswa Guru</td></tr>' +
                            '<tr><td>BM</td><td>:&nbsp;</td><td>Beban Mengajar (Guru)</td></tr>' +
                            '<tr><td>JTM</td><td>:&nbsp;</td><td>Jam Tatap Muka (Total Per Minggu)</td></tr>' +
                            '<tr><td>RGK</td><td>:&nbsp;</td><td>Rasio Guru Kelas</td></tr>' +
                            '<tr><td>JS</td><td>:&nbsp;</td><td>Jumlah Siswa</td></tr>' +                           
                            '<tr><td>KG</td><td>:&nbsp;</td><td>Kebutuhan Guru</td></tr>' +
                            '<tr><td>GT</td><td>:&nbsp;</td><td>Guru Tersedia</td></tr>' +
                            '<tr><td>JR</td><td>:&nbsp;</td><td>Jumlah Rombel</td></tr>' + 
                            '<tr><td>MPn</td><td>:&nbsp;</td><td>Mata Pelajaran Tingkat n</td></tr>' + 
                            '<tr><td>Kn</td><td>:&nbsp;</td><td>Jumlah Kelas/Rombel Tingkat n</td></tr>' +
                            '</table>'
                    });
                    help.show();
                }
            }]
        });
        

        // var menu = infoPanel.getTopToolbar().items.each(function(item){
        //     //console.log(item.text);
        //     if (typeof item.text != 'undefined') {
        //         switch (item.text) {
        //             case 'RSG':
        //                 //console.log(item);
        //                 me.rsg_menu = item;
        //                 break;
        //             case 'BM':
        //                 //console.log(item);
        //                 me.bm_menu = item;
        //                 break;
        //             case 'RSG BK':
        //                 //console.log(item);
        //                 me.rsgbk_menu = item;
        //                 break;
        //         }
        //     }
        // });

        //console.log(this.cm);

        var store = Ext.create('Ext.data.Store', {
            // pageSize: 17,
            fields: [
                { name: 'perhitungan_rasio_id', type: 'int'  } ,
                { name: 'mata_pelajaran_id', type: 'int'  } ,
                { name: 'nama', type: 'string'  } ,
                { name: 'bentuk_pendidikan_id', type: 'int'  } ,
                { name: 'sekolah_id', type: 'string'  } ,
                { name: 'tahun_ajaran_id', type: 'float'  } ,
                { name: 'mp1', type: 'float' } ,
                { name: 'k1', type: 'float' } ,
                { name: 'mp2', type: 'float' } ,
                { name: 'k2', type: 'float' } ,
                { name: 'mp3', type: 'float' } ,
                { name: 'k3', type: 'float' } ,
                { name: 'mp4', type: 'float' } ,
                { name: 'k4', type: 'float' } ,
                { name: 'mp5', type: 'float' } ,
                { name: 'k5', type: 'float' } ,
                { name: 'mp6', type: 'float' } ,
                { name: 'k6', type: 'float' } ,
                { name: 'k6', type: 'float' } ,
                { name: 'jr', type: 'float' } ,
                { name: 'jtm', type: 'float' } ,
                { name: 'rasio_gk', type: 'float' } ,
                { name: 'jumlah_siswa', type: 'float' } ,
                { name: 'kg', type: 'float' } 
            ],
            idProperty: 'perhitungan_rasio_id',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/PerhitunganKgSd',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results',
                    successProperty: false
                }
            }
        });

        var gridPerhitunganRasio = new Ext.grid.GridPanel({
            flex: 3,
            anchor: '100%',
            emptyText: 'Tidak ada data',
            selModel: new Ext.selection.RowModel(),
            // sm: new Ext.grid.RowSelectionModel(),
            store: store,
            columns: this.cm
        });
        

        me.infoPanel = infoPanel;
        me.gridPerhitunganRasio = gridPerhitunganRasio;

        // me.items = [
        //     infoPanel, 
        //     gridPerhitunganRasio
        // ];
        
        me.items = {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [ 
                infoPanel, 
                gridPerhitunganRasio 
            ]
        }
        
        this.callParent();
    }
});
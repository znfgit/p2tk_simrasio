Ext.define('SimptkRasio.view.charts.CombinedChartKgSdDashboard', {
    extend: 'Ext.Panel',
    xtype: 'combined_chart_kg_sd_dashboard',
    layout: 'fit',
    requires: [
        // 'Ext.chart.Chart'
    ],
    initComponent: function() {
        var me = this;
        var skup = (SimptkRasio.kode_wilayah != "") ? SimptkRasio.kode_wilayah : '000000';

        this.myDataStore = Ext.create('Ext.data.Store', {
            pageSize: 7,
            fields: [
                { name: 'kode_rincian', type: 'string'  },
                { name: 'nama_rincian', type: 'string'  },
                { name: 'jumlah_kebutuhan', type: 'int'  }, 
                { name: 'jumlah_ptk_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_honorer_s1_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_pns_nons1_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_pns_s1_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_sertifikasi_s1_ada_utk_matpel', type: 'int'  }
            ],
            idProperty: 'kode_rincian',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/KebutuhanGuruSdNasional',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        this.myDataStore.load();

        var myChart = {    
            xtype: 'cartesian',
            flex: 1,
            width: '100%',
            height: 310,
            style: 'background: #fff;',
            legend: {
                docked: 'right'
            },
            store: this.myDataStore,
            insetPadding: {
                // top: 40,
                // left: 40,
                // right: 40,
                // bottom: 20
                top: 20,
                left: 20,
                right: 30,
                bottom: 20
            },
            

            axes: [{
                type: 'numeric',
                position: 'left',
                grid: true,
                fields: ['jumlah_kebutuhan', 'jumlah_ptk_pns_s1_ada_utk_matpel', 'jumlah_ptk_pns_nons1_ada_utk_matpel', 'jumlah_ptk_honorer_s1_ada_utk_matpel'],
                //renderer: function (v) { return v.toFixed(v < 10 ? 1: 0) + '%'; },
                label: {
                    fontWeight: 300
                },
                renderer: function (v) { return Ext.util.Format.number(v, '0,000'); },
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['nama_rincian'],
                label: {
                    // fontFamily: "Raleway",
                    fontWeight: 300,
                    rotate: {
                        degrees: -25
                    }
                }
            }],


            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 'PNS S1', 'PNS non S1', 'Honorer S1' ],
                xField: 'nama_rincian',
                yField: [ 'jumlah_ptk_pns_s1_ada_utk_matpel', 'jumlah_ptk_pns_nons1_ada_utk_matpel', 'jumlah_ptk_honorer_s1_ada_utk_matpel' ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: '#000',
                    lineWidth: 1,
                    strokeStyle: '#fff'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var browser = item.series.title[item.series.yField.indexOf(item.yField)];
                        this.setTitle(browser + ' for ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.yField) + '%');
                    }
                }
            },{

                type: 'scatter',
                axis: 'left',
                title: '<b>Keb. Guru</b>',
                xField: 'nama_rincian',
                yField: 'jumlah_kebutuhan',
                marker: {
                    radius: 4
                },
                highlight: {
                    fill: '#000',
                    radius: 3,
                    'stroke-width': 2,
                    stroke: '#ccc'
                },
                tips: {
                    trackMouse: true,
                    style: 'background: #FFF',
                    height: 20,
                    renderer: function(storeItem, item) {
                        var title = item.series.title;
                        this.setTitle(title + ' untuk ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.series.yField) );
                    }
                }

                // type: 'line',
                // axis: 'left',
                // title: 'KG',
                // xField: 'nama_rincian',
                // yField: 'jumlah_kebutuhan',
                // style: {
                //     stroke: '#444444',
                //     'stroke-width': 2
                // },
                // markerConfig: {
                //     radius: 3
                // },
                // highlight: {
                //     fill: '#000',
                //     radius: 3,
                //     'stroke-width': 2,
                //     stroke: '#ccc'
                // },
                // tips: {
                //     trackMouse: true,
                //     style: 'background: #FFF',
                //     height: 20,
                //     renderer: function(storeItem, item) {
                //         var title = item.series.title;
                //         this.setTitle(title + ' untuk ' + storeItem.get('nama_rincian') + ': ' + storeItem.get(item.series.yField) );
                //     }
                // }

            }]
        };

        var myGrid = Ext.create('Ext.grid.Panel', {
            flex: 1,
            //title: 'Tabel',
            width: '100%',
            autoScroll: true,
            store: this.myDataStore,
            defaults: {
                sortable: true
            },
            features: [{
                ftype: 'summary',
                dock: 'top',
                remoteRoot: 'summaryData'
            }],
            listeners: {
                itemdblclick: function( grid, record, item, index, e, eOpts ) {
                    
                    grid.up('form-rekap-wilayah').close();

                    Ext.Ajax.request({
                        url: '/SearchWilayah/' + record.data.kode_wilayah,
                        success: function(response){
                            var text = response.responseText;
                            var res = Ext.JSON.decode(response.responseText);
                            SimptkRasio.kode_wilayah = res.kode_wilayah;
                            SimptkRasio.nama_wilayah = res.nama;
                            SimptkRasio.skup = res.id_level_wilayah;
                            SimptkRasio.mst_kode_wilayah = res.mst_kode_wilayah;
                            Ext.getCmp('info-app').update(res.nama);
                            
                            //me.close();
                            var winRekapWilayah = new SimptkRasio.view.forms.RekapWilayah();
                            winRekapWilayah.down('tabpanel').setActiveTab(1);
                            winRekapWilayah.show();

                        }
                    });

                }
            },
            tbar: Ext.create('Ext.PagingToolbar', {
                store: this.myDataStore,
                displayInfo: true,
                displayMsg: 'Data #{0} - {1} of {2}',
                emptyMsg: "Tidak ada data"
            }),
            /*
            tbar: [{
                xtype: 'button',
                text: 'Master Wilayah',                
                //cls: 'addbutton',
                glyph: '61768@font-awesome',                
                scope: this,
                action: 'add',
                handler: function(btn) {
                    
                    if (SimptkRasio.mst_kode_wilayah == "") {
                        return;
                    }

                    btn.up('form-rekap-wilayah').close();
                    Ext.Ajax.request({
                        url: '/SearchWilayah/' + SimptkRasio.mst_kode_wilayah,
                        success: function(response){
                            var text = response.responseText;
                            var res = Ext.JSON.decode(response.responseText);
                            SimptkRasio.kode_wilayah = res.kode_wilayah;
                            SimptkRasio.nama_wilayah = res.nama;
                            SimptkRasio.skup = res.id_level_wilayah;
                            SimptkRasio.mst_kode_wilayah = res.mst_kode_wilayah;
                            Ext.getCmp('info-app').update(res.nama);
                            
                            //me.close();
                            var winRekapWilayah = new SimptkRasio.view.forms.RekapWilayah();
                            winRekapWilayah.down('tabpanel').setActiveTab(1);
                            winRekapWilayah.show();

                        }
                    });

                }
            },{
                xtype: 'combo_matpel',
                width: 300,
                listeners: {
                    select: function(combo) {
                        
                        var rec = combo.findRecordByValue(combo.getValue());
                        var nama = rec.data.nama;
                        var skup = (SimptkRasio.kode_wilayah != "") ? SimptkRasio.kode_wilayah : '000000';

                        SimptkRasio.mata_pelajaran_id = combo.getValue();

                        var url = '/KebutuhanGuruSdPerWilayah' // + skup + "/" + SimptkRasio.mata_pelajaran_id;
                        var store = me.myDataStore;
                        store.getProxy().setUrl(url);
                        store.reload();
                    }
                }
            }],
            */
            columns: [
                {
                    text: (SimptkRasio.skup < 3) ? 'Wilayah' : 'Sekolah' ,
                    flex: 1,
                    summaryType: 'count',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        //return '<b>Total '+ v + ' ' + ((me.skup < 3) ? 'Wilayah' : 'Sekolah') + '</b>';
                        var namawilayah = (me.nama_wilayah == 'Indonesia') ? 'Nasional' : me.nama_wilayah;
                        return '<b>Total Nasional</b>';
                    },
                    dataIndex: 'nama_rincian'
                },
                {
                    text: '<b>Keb.Guru</b>',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_kebutuhan',
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_s1_ada_utk_matpel',
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'PNS Non S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_pns_nons1_ada_utk_matpel',
                    renderer: function (v, meta, record) { 
                        return (v > 0) ? Ext.util.Format.number(v, '0,000') : 0; 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                },
                {
                    text: 'Honorer S1',
                    width: null,
                    align: 'right',
                    dataIndex: 'jumlah_ptk_honorer_s1_ada_utk_matpel',
                    renderer: function (v, meta, record) { 
                        return Ext.util.Format.number(Math.abs(v), '0,000'); 
                    },
                    summaryType: 'sum',
                    summaryRenderer: function(v, summaryData, dataIndex) {
                        return '<b>' + ((v > 0) ? Ext.util.Format.number(v, '0,000') : 0).toString() + '</b>';
                    }
                }
            ]
        });

        me.items = {
            //xtype: 'tabpanel',
            //tabPosition: 'bottom',
            xtype: 'container',
            //height: 250,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            items: [myChart, myGrid]
            //items: [myChart]
        }
        
        //me.items = myChart;

        this.callParent();
    }
});
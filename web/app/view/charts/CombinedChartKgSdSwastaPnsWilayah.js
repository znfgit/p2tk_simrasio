Ext.define('SimptkRasio.view.charts.CombinedChartKgSdSwastaPnsWilayah', {
    extend: 'Ext.Panel',
    xtype: 'combined_chart_kg_sd_swasta_pns_wilayah',
    layout: 'fit',
    //frame: true,
    changewilayah: function() {


    },
    initComponent: function() {
        
        var me = this;
        
        me.store = Ext.create('Ext.data.Store', {
            pageSize: 17,
            fields: [
                { name: 'kode_rincian', type: 'string'  },
                { name: 'nama_rincian', type: 'string'  },
                { name: 'jumlah_kebutuhan', type: 'int'  },
                { name: 'jumlah_ptk_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_gty_s1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_gty_nons1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_pns_dpk_nons1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_pns_dpk_s1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_gtt_nons1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_gtt_s1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', type: 'int'  },
                { name: 'jumlah_ptk_sertifikasi_s1_ada_utk_matpel', type: 'int'  }
            ],
            idProperty: 'kode_rincian',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/KebutuhanGuruSdSwastaPerWilayah',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        var myChart = {
            xtype: 'chart_bar_and_dot_swasta',
            //title: 'Analisis Kebutuhan Guru SD Per-Matpel (dalam % thd KG) - Formasi',
            title: 'Analisis Kebutuhan Guru SD Swasta Per-Wilayah (Formasi)',
            flex: 7,
            width: '100%',
            // height: 310,
            store: me.store
        };

        var myGrid = new Ext.widget({
            xtype: 'grid_per_matpel_swasta',
            flex: 6,
            store: me.store,
            purpose: 'formasi',
            per: 'wilayah',
            jenjang: 'sd'
        });

        me.items = {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            items: [myChart, myGrid]
        }

        this.callParent();
    }
});
Ext.define('SimptkRasio.view.charts.CombinedChartKgGuruBk', {
    extend: 'Ext.Panel',
    xtype: 'combined_chart_kg_guru_bk',
    layout: 'fit',
    //frame: true,
    changewilayah: function() {


    },
    initComponent: function() {
        
        var me = this;
        
        me.store = Ext.create('Ext.data.Store', {
            pageSize: 17,
            fields: [
                { name: 'kode_rincian', type: 'string'  },
                { name: 'nama_rincian', type: 'string'  },
                { name: 'jml_siswarbl_total', type: 'int' },
                { name: 'jumlah_gurubk_minimal', type: 'int' },
                { name: 'jumlah_gurubk_maksimal', type: 'int' },
                { name: 'jumlah_gurubk', type: 'int' },
                { name: 'jumlah_gurubk_honorer_s1', type: 'int' },
                { name: 'jumlah_gurubk_honorer_nons1', type: 'int' },
                { name: 'jumlah_gurubk_pns_s1', type: 'int' },
                { name: 'jumlah_gurubk_pns_nons1', type: 'int' },
                { name: 'jumlah_gurubk_nonpns_s1', type: 'int' },
                { name: 'jumlah_gurubk_nonpns_nons1', type: 'int' },
                { name: 'jumlah_gurubk_sert', type: 'int' },
                { name: 'jumlah_gurubk_honorer_s1_sert', type: 'int' },
                { name: 'jumlah_gurubk_honorer_nons1_sert', type: 'int' },
                { name: 'jumlah_gurubk_pns_s1_sert', type: 'int' },
                { name: 'jumlah_gurubk_pns_nons1_sert', type: 'int' },
                { name: 'jumlah_gurubk_nonpns_s1_sert', type: 'int' },
                { name: 'jumlah_gurubk_nonpns_nons1_sert', type: 'int' },
                { name: 'kelebihan_guru', type: 'int' },
                { name: 'kekurangan_guru', type: 'int' }
            ],
            idProperty: 'kode_rincian',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/KebutuhanGuruBk',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        var myChart = {
            xtype: 'chart_bar_and_dot_bk',
            //title: 'Analisis Kebutuhan Guru SD Per-Matpel (dalam % thd KG) - Formasi',
            title: 'Kebutuhan Guru BK',
            flex: 7,
            width: '100%',
            // height: 310,
            store: me.store
        };

        var myGrid = new Ext.widget({
            xtype: 'grid_per_matpel',
            flex: 6,
            store: me.store,
            purpose: 'bk',
            per: 'wilayah',
            jenjang: 'smp'
        });

        me.items = {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            items: [myChart, myGrid]
        }

        this.callParent();
    }
});
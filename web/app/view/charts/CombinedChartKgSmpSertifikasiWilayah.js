Ext.define('SimptkRasio.view.charts.CombinedChartKgSmpSertifikasiWilayah', {
    extend: 'Ext.Panel',
    xtype: 'combined_chart_kg_smp_sertifikasi_wilayah',
    layout: 'fit',

    initComponent: function() {

        var me = this;

        me.store = Ext.create('Ext.data.Store', {
            storeId: 'CombinedChartKgSmpSertifikasiWilayah',
            pageSize: 17,
            fields: [
                { name: 'kode_rincian', type: 'string'  },
                { name: 'nama_rincian', type: 'string'  },
                { name: 'jumlah_kebutuhan', type: 'int'  },

                // { name: 'jumlah_ptk_ada_utk_matpel', type: 'int'  }, 
                { name: 'jumlah_ptk_honorer_s1_ada_utk_matpel_sert', type: 'int'  },
                { name: 'jumlah_ptk_honorer_nons1_ada_utk_matpel_sert', type: 'int'  },
                { name: 'jumlah_ptk_pns_nons1_ada_utk_matpel_sert', type: 'int'  },
                { name: 'jumlah_ptk_pns_s1_ada_utk_matpel_sert', type: 'int'  },
                { name: 'jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert', type: 'int'  },
                { name: 'jumlah_ptk_nonpns_s1_ada_utk_matpel_sert', type: 'int'  }
                // { name: 'jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', type: 'int'  }, 
                // { name: 'jumlah_ptk_sertifikasi_s1_ada_utk_matpel', type: 'int'  }
            ],
            idProperty: 'kode_rincian',
            proxy: {
                // load using HTTP
                type: 'ajax',
                url: '/KebutuhanGuruSmpPerWilayah',
                autoLoad: true,
                reader: {
                    type: 'json',
                    rootProperty: 'rows',
                    totalProperty  : 'results'
                }
            }
        });

        var myChart = {
            xtype: 'chart_bar_and_dot_sert',
            title: 'Analisis Kebutuhan Guru SMP Per-Wilayah (Sertifikasi)',
            flex: 7,
            width: '100%',
            store: me.store
        };

        var myGrid = new Ext.widget({
            xtype: 'grid_per_matpel',
            // id: 'CombinedChartKgSmpSertifikasiWilayah',
            flex: 6,
            store: me.store,
            purpose: 'sertifikasi',
            per: 'wilayah',
            jenjang: 'smp'
        });

        me.items = {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            activeTab: 0,
            items: [myChart, myGrid]
        }
        
        this.callParent();
    }
});
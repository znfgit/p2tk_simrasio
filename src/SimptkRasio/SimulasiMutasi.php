<?php

/**
 * SimulasiMutasi Module
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

use SimptkRasio\Model;
use SimptkRasio\Model\SekolahQuery;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\MataPelajaran;
use SimptkRasio\Model\MataPelajaranPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\KebutuhanGuruSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulumPeer;
use SimptkRasio\Model\KebutuhanGuruSmpPeer;
use SimptkRasio\Model\RombelSekolahSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulum;
use SimptkRasio\Model\RombelSekolahSd;
use SimptkRasio\Model\PtkTersediaPeer;
use SimptkRasio\Model\RefLinierBidstudiMatpelPeer;
use SimptkRasio\Model\RefLinierisasiPeer;
use SimptkRasio\Model\GuruPermatpelSdPeer;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\NominasiPindahGuruSdPeer;
use SimptkRasio\Model\NominasiPindahGuruSd;
use SimptkRasio\Model\RombelSekolahSmpPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;


class SimulasiMutasi {

    public function listNominasi (Request $request, Application $app) {
    
        $sekolahId = $request->get('sekolah_id');
        $mataPelajaranId = $request->get('mata_pelajaran_id');
        
        $bentukPendidikanId = $request->get('bentuk_pendidikan_id');
        $levelWilayah = $request->get('level_wilayah');
        $limit = $request->get('limit');
        $start = $request->get('start');
        
        if (isset($levelWilayah)) {
            
            $kodeWilayah = getSignificantDigits($sekolahId)."%";
            
            $c2 = new  \Criteria();
            $c2->add(NominasiPindahGuruSdPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
            $c2->add(NominasiPindahGuruSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $c2->setLimit($limit);
            $c2->setOffset($start);
            $nominasis = NominasiPindahGuruSdPeer::doSelect($c2);
            
            foreach ($nominasis as $n) {
                $ptkIds[] = $n->getPtkId();
                $guruNominasis[$n->getPtkId()] = $n->toArray(\BasePeer::TYPE_FIELDNAME);
            }
            
            $c3 = new \Criteria();
            $c3->add(PtkTersediaPeer::PTK_ID, $ptkIds, \Criteria::IN);
            $ptkTersedias = PtkTersediaPeer::doSelect($c3);
            
            foreach ($ptkTersedias as $ptk){
                $guruNominasis[$ptk->getPtkId()]["nama_sekolah"] = $ptk->getNamaSekolah();
                $guruNominasis[$ptk->getPtkId()]["nuptk"] = $ptk->getNuptk();
                $guruNominasis[$ptk->getPtkId()]["nrg"] = $ptk->getNrg();
                $guruNominasis[$ptk->getPtkId()]["tgl_lahir"] = $ptk->getTglLahir();
            }
            
            foreach ($guruNominasis as $g) {
                $arrOut[] = $g;
            }
            
            return tableJson($arrOut, sizeof($nominasis), array('ptk_id'));
            
        } else {
            
            $c2 = new \Criteria();
            $c2->add(NominasiPindahGuruSdPeer::SEKOLAH_ID, $sekolahId);
            $c2->add(NominasiPindahGuruSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $nominasis = NominasiPindahGuruSdPeer::doSelect($c2);
            
            return tableJson(getArray($nominasis, \BasePeer::TYPE_FIELDNAME), sizeof($nominasis), array('ptk_id'));
        }    
        
    }
    
    public function listPtkNominasi (Request $request, Application $app) {
        
        $sekolahId = $request->get('sekolah_id');
        $mataPelajaranId = $request->get('mata_pelajaran_id');
    
        $gurus = array();
        $sekolah = SekolahPeer::retrieveByPK($sekolahId);
        
        if ($sekolah->getBentukPendidikanId() == 5) {
        
            $c = new  \Criteria();
            $c->add(GuruPermatpelSdPeer::SEKOLAH_ID, $sekolahId);
            $c->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $gurus = GuruPermatpelSdPeer::doSelect($c);
        
            $c2 = new  \Criteria();
            $c2->add(NominasiPindahGuruSdPeer::SEKOLAH_ID, $sekolahId);
            $c2->add(NominasiPindahGuruSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $nominasis = NominasiPindahGuruSdPeer::doSelect($c2);
            
        } else if ($sekolah->getBentukPendidikanId() == 6) {
            
            $c = new  \Criteria();
            $c->add(GuruPermatpelSmpPeer::SEKOLAH_ID, $sekolahId);
            $c->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $gurus = GuruPermatpelSmpPeer::doSelect($c);
            
            $c2 = new  \Criteria();
            $c2->add(NominasiPindahGuruSdPeer::SEKOLAH_ID, $sekolahId);
            $c2->add(NominasiPindahGuruSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
            $nominasis = NominasiPindahGuruSdPeer::doSelect($c2);
            
        }
            
        foreach ($nominasis as $n) {
            $arrNominasis[ $n->getPtkId() ] = $n;
        }
    
        foreach ($gurus as $g) {
            if ($arrNominasis[ $g->getPtkId() ]) {
                //skip
            } else {
                $guruOut[] = $g;
            }
        }
        return tableJson(getArray($guruOut, \BasePeer::TYPE_FIELDNAME), sizeof($guruOut), array('ptk_id'));
    
    }
    
    public function saveNominasi (Request $request, Application $app) {
    
        $data = splitJsonArray($request->get('data'));
        $rowsAffected = 0;
    
        try {
    
            foreach ($data as $d){
                	
                $row = json_decode(stripslashes($d));
    
                $mata_pelajaran_id = $row->mata_pelajaran_id;
                $ptk_id = $row->ptk_id;
                $sekolah_id = $row->sekolah_id;
                $sekolah = SekolahPeer::retrieveByPK($sekolah_id);
                
                // echo $row->sekolah_id." | ". $row->mata_pelajaran_id." | ".$row->ptk_id."\r\n";
                                
                if (!empty($ptk_id)) {
    
                    //$guru = GuruPermatpelSdPeer::retrieveByPK($mata_pelajaran_id, $ptk_id);
                    /*
                    $c = new Criteria();
                    $c->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $mata_pelajaran_id);
                    $c->add(GuruPermatpelSdPeer::PTK_ID, $ptk_id);
                    $guru = GuruPermatpelSdPeer::doSelectOne($c);
                    */
                    
                    if ($sekolah->getBentukPendidikanId() == 5) {
                        
                        $c = new  \Criteria();
                        $c->add(GuruPermatpelSdPeer::SEKOLAH_ID, $sekolah_id);
                        $c->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $mata_pelajaran_id);
                        $c->add(GuruPermatpelSdPeer::PTK_ID, $ptk_id);
                        $guru = GuruPermatpelSdPeer::doSelectOne($c);
                                        
                    } else if ($sekolah->getBentukPendidikanId() == 6) {
                    
                        $c = new  \Criteria();
                        $c->add(GuruPermatpelSmpPeer::SEKOLAH_ID, $sekolah_id);
                        $c->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $mata_pelajaran_id);
                        $c->add(GuruPermatpelSmpPeer::PTK_ID, $ptk_id);
                        $guru = GuruPermatpelSmpPeer::doSelectOne($c);
                    
                    }
                    
                    
                    if (is_object($guru)) {
                        //echo "guru found \n";
                        	
                        $nominasi = NominasiPindahGuruSdPeer::retrieveByPK($ptk_id);
                        // 					print_r($nominasi);
                        	
                        	
                        if (!is_object($nominasi)) {
    
                            $nominasi = new NominasiPindahGuruSd();
                            // 						print_r($nominasi);
    
    
                            $nominasi->setPtkId($guru->getPtkId());
                            $nominasi->setMataPelajaranId($guru->getMataPelajaranId());
                            $nominasi->setSekolahId($guru->getSekolahId());
                            $nominasi->setKodeWilayah($guru->getKodeWilayah());
                            $nominasi->setNamaPtk($guru->getNamaPtk());
                            $nominasi->setJjm($guru->getJjm());
                            $nominasi->setStatusNominasi(1);
    
                            if ($nominasi->save()) {
                                $rowsAffected++;
                            }
                        }
                        	
                    }
                }
                	
            }
    
        } catch (Exception $e){
    
            $message = $e->getMessage();
            $success = 'false';
            $result = sprintf("{'success' : $s, 'affected' : '$s', 'message' : '$s' }",$success,$rowsAffected,$message);
            die($result);
    
        }
    
        //$success = "true";
        $result = sprintf("{'success' : true, 'affected' : ".$rowsAffected."}");
    
        return $result;
    
    }
    
    public function deleteNominasi (Request $request, Application $app) {
    
        $data = splitJsonArray($request->get('data'));
        $rowsAffected = 0;
    
        try {
    
            foreach ($data as $d){
    
                $row = json_decode(stripslashes($d));
    
                $mata_pelajaran_id = $row->mata_pelajaran_id;
                $ptk_id = $row->ptk_id;
    
                if (!empty($ptk_id)) {
    
                    $nominasi = NominasiPindahGuruSdPeer::retrieveByPK($ptk_id);
    
                    if (is_object($nominasi)) {
    
                        $nominasi->delete();
                        $rowsAffected++;
                        	
                    }
                }
    
            }
    
        } catch (Exception $e){
    
            $message = $e->getMessage();
            $success = 'false';
            $result = sprintf("{'success' : $s, 'affected' : '$s', 'message' : '$s' }",$success,$rowsAffected,$message);
            die($result);
    
        }
    
        //$success = "true";
        $result = sprintf("{'success' : true, 'affected' : ".$rowsAffected."}");
        return $result;
    
    }
    
        
    
    
    
}

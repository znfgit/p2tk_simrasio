<?php

/**
 * DataSd Class
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

use SimptkRasio\Model;
use SimptkRasio\Model\SekolahQuery;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\MataPelajaran;
use SimptkRasio\Model\MataPelajaranPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\KebutuhanGuruSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulumPeer;
use SimptkRasio\Model\KebutuhanGuruSmpPeer;
use SimptkRasio\Model\RombelSekolahSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulum;
use SimptkRasio\Model\RombelSekolahSd;
use SimptkRasio\Model\PtkTersediaPeer;
use SimptkRasio\Model\RefLinierBidstudiMatpelPeer;
use SimptkRasio\Model\RefLinierisasiPeer;
use SimptkRasio\Model\GuruPermatpelSdPeer;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\NominasiPindahGuruSdPeer;
use SimptkRasio\Model\NominasiPindahGuruSd;
use SimptkRasio\Model\RombelSekolahSmpPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;
use SimptkRasio\Model\GuruPermatpelSd;


class DataSd {

    public function kebutuhanGuruNasional (Request $request, Application $app) {
        
        $outJson = DataSd::getKebutuhanGuru($request, 'wilayah', $app, true);
        return $outJson;
        
    }

    public function kebutuhanGuruPerWilayah (Request $request, Application $app) {
    
    	$outJson = DataSd::getKebutuhanGuru($request, 'wilayah', $app);
    	return $outJson;
            
    }
    
    public function kebutuhanGuruPerMatpel (Request $request, Application $app) {
    
        $outJson = DataSd::getKebutuhanGuru($request, 'matpel', $app);
        return $outJson;
        
    }
    
    public function getKebutuhanGuru($request, $per, $app, $is_nasional=false, $format='json', $skipLimits=false) {
    
        // 	Assumptions:
        // 	= Semua dihitung dengan asumsi jam kapasitas guru mengajar di sekolah induk 24 jam, tidak bisa simulasi
        //	= Kebutuhan rombel dihitung dari jumlah rombel aktual, tidak bisa simulasi
        //	= Jumlah siswa per rombel aktual, tidak bisa simulasi
    
        //  Parameters:
        // 	= kode_wilayah: dari dropdown filter / drilldown
        // 	= mata_pelajaran_id: dari dropdown filter
    
        $kodeWilayah = $request->get('kode_wilayah');
        $start = $request->get('start');
        $limit = $request->get('limit');
        $matpelId = $request->get('mata_pelajaran_id');
        $satuan = $request->get('satuan');
    
        // 	Cleaning and default value for debugging purposes
        $start = $start ?: 0;
        $limit = $limit ?: 12;
        //$limit = 12;
        //$kodeWilayah = $kodeWilayah ?: '000000';
        //$kodeWilayah = $kodeWilayah ?: '026017';
        if ($is_nasional) {
            $kodeWilayah = '000000 ';
        } else if (!$kodeWilayah) {
            //$kodeWilayah = $app['session']->get('kode_wilayah') ?: '000000 ';
            $kodeWilayah = $kodeWilayah ?: '000000';
        } else if (strtoupper($kodeWilayah) == 'NULL') {
            $kodeWilayah = '000000';
        }
    
        $matpelId = $matpelId ?: '4020';
        if (strtoupper($matpelId) == 'NULL') {
            $matpelId = '4020';
        }
        
        // Debug
        //echo "$per $matpelId $kodeWilayah $start $limit $satuanJam"; die;
        
        // Satuan angka KG, jam atau count guru
        $satuanJam = ($satuan == 'jam') ? true : false;
        $perWilayah = ($per == 'wilayah') ? true : false;
        
        // Cari object wilayah dulu 
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah);
        $objWil = MstWilayahPeer::doSelectOne($cwil);

        if (!is_object($objWil)) {
            die ("$kodeWilayah not found");
        }

        // Cek Levelnya, kalau2 yang dicari sekolah
        $levelWil = $objWil->getIdLevelWilayah();
    
        // Yang dicari sekolah, gak usah digroup
        if ($levelWil == '5') {
    
            $c1 = new \Criteria();
            $c1->add(KebutuhanGuruSdPeer::KODE_WILAYAH, $kodeWilayah);
            $kg = KebutuhanGuruSdPeer::doSelect($c1);
    
            foreach ($kg as $k) {
                $arr = $k->toArray(BasePeer::TYPE_FIELDNAME);
                // 			$arrKg['kode_wilayah'] = $k->getSekolahId();
                //$arr['nama'] = $k->getNamaSekolah();
                $arrKg[] = $arr;
            }
            
            if ($format == 'json') {
                return tableJson($arrKg , sizeof($arrKg), array('kebutuhan_guru_sd_id'));
            } else if ($format == 'array') {
                return $arrKg;
            }
            
        // Yang dicari wilayah, group sesuai dengan tingkat masing2
        } else {
        
            switch ($levelWil) {
                case '0':
                    $groupNasional=1;
                    break;
                case '1':
                    $groupPropinsi=1;
                    break;
                case '2':
                    $groupKabkota=1;
                    break;
                case '3':
                    $groupKecamatan=1;
                    break;
                default:
                    $noGroup = 1;
                    break;
            }
    
            // 	Start query
            $c = new \Criteria();
            $c->clearSelectColumns();
    
            // 	Set divider / multiplier for calculation by jam/ptk
            $jamDivider = $satuanJam ? " " : " / 24 ";
            $jamMultiplier = $satuanJam ? " * 24 " : " ";
    
            // 	Filter by matpel if list per wilayah
            if ($perWilayah)
            {
                $c->add(KebutuhanGuruSdPeer::MATA_PELAJARAN_ID, $matpelId);
            }
            // 	Group by matpel, set rinci matpel if list per matpel
            else
            {
                $c->addAsColumn('kode_rincian', KebutuhanGuruSdPeer::MATA_PELAJARAN_ID);
                $c->addAsColumn('nama_rincian', KebutuhanGuruSdPeer::NAMA_MATPEL);
                	
                $c->addGroupByColumn(KebutuhanGuruSdPeer::MATA_PELAJARAN_ID);
                $c->addGroupByColumn(KebutuhanGuruSdPeer::NAMA_MATPEL);
                $c->addDescendingOrderByColumn(KebutuhanGuruSdPeer::MATA_PELAJARAN_ID);
            }
            	
            // 	Filter, Detil Column addition and Grouping by Level Wilayah
            if ($groupNasional) {
                	
                $c->add(KebutuhanGuruSdPeer::KODE_WILAYAH_PROPINSI, '000000 ', \Criteria::NOT_LIKE);
                	
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSdPeer::KODE_WILAYAH_PROPINSI);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSdPeer::NAMA_PROPINSI);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSdPeer::KODE_WILAYAH_PROPINSI);
                    	
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::KODE_WILAYAH_PROPINSI);
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::NAMA_PROPINSI);
                }
            }
    
            if ($groupPropinsi) {
                	
                //Where
                $c->add(KebutuhanGuruSdPeer::KODE_WILAYAH_PROPINSI, $kodeWilayah);
                	
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSdPeer::KODE_WILAYAH_KABKOTA);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSdPeer::NAMA_KABKOTA);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSdPeer::NAMA_KABKOTA);
                    	
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::KODE_WILAYAH_KABKOTA);
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::NAMA_KABKOTA);
                }
            }
    
            if ($groupKabkota) {
                	
                //Where
                $c->add(KebutuhanGuruSdPeer::KODE_WILAYAH_KABKOTA, $kodeWilayah);
                	
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSdPeer::KODE_WILAYAH);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSdPeer::NAMA_KECAMATAN);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSdPeer::NAMA_KECAMATAN);
                    	
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::KODE_WILAYAH);
                    $c->addGroupByColumn(KebutuhanGuruSdPeer::NAMA_KECAMATAN);
                }
            }
    
            if ($groupKecamatan) {
                	
                //Where
                $c->add(KebutuhanGuruSdPeer::KODE_WILAYAH, $kodeWilayah);
                	
                //Columns
                if ($perWilayah) {
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSdPeer::SEKOLAH_ID);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSdPeer::NAMA_SEKOLAH);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSdPeer::NAMA_SEKOLAH);
                }
    
            }
    
            // 	Add data for grouping
            if ($groupNasional || $groupPropinsi || $groupKabkota) {
                $c->addAsColumn('jumlah_kebutuhan', 'sum('.KebutuhanGuruSdPeer::TOTAL_JAM_DIBUTUHKAN.') '. $jamDivider);
                $c->addAsColumn('jumlah_ptk_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);

                //$c->addAsColumn('jumlah_ptk_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                
            
            }
    
            if ($groupKecamatan) {

                $c->addAsColumn('jumlah_kebutuhan', '('.KebutuhanGuruSdPeer::TOTAL_JAM_DIBUTUHKAN.')'. $jamDivider);
                $c->addAsColumn('jumlah_ptk_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_s1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_nons1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_nons1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_s1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_nons1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_s1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                //$c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                //$c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);

                //$c->addAsColumn('jumlah_ptk_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_honorer_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_HONORER_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_PNS_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_nonpns_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_NONPNS_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                //$c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                //$c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSdPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                                
            }
    
            // 	Count all data first
            $count = KebutuhanGuruSdPeer::doCount($c);

            //  Calculate all first. For remoteSummary support
            $stmtSummary = KebutuhanGuruSdPeer::doSelectStmt($c);
            $resSummary = $stmtSummary->fetchAll(\PDO::FETCH_ASSOC);
            
            $arr = array();
            $arr['jumlah_kebutuhan'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] = 0;
            
            foreach ($resSummary as $r) {
                
                /* Formasi */
                $arr['jumlah_kebutuhan'] += $r['jumlah_kebutuhan'];
                $arr['jumlah_ptk_ada_utk_matpel'] += 
                    $r['jumlah_ptk_honorer_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
                
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
                
                /* Sertifikasi */
                $arr['jumlah_ptk_ada_utk_matpel_sert'] +=
                    $r['jumlah_ptk_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
                    
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
            }
            
            $summaryArr = $arr;
            
            // 	Set limit for paging
            if (!$skipLimits) {
                $c->setLimit($limit);
                $c->setOffset($start);
            }
            
            //print_r($c); die;
            $stmt = KebutuhanGuruSdPeer::doSelectStmt($c);
            //print_r($stmt); die;
            //echo $this->interpolateQuery($stmt->queryString, array("p1"=> "'$matpelId'", "p2"=> "'$kodeWilayah'"));
            //die;
    
    
            $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            $out = "";
    
            //print_r($res); die;
            //$out .= "nama_rincian | jumlah_kebutuhan | jumlah_ptk_ada_utk_matpel | jumlah_ptk_honorer_s1_ada_utk_matpel | jumlah_ptk_pns_nons1_ada_utk_matpel | <br>\n";
    
            if (!$perWilayah) {
                
                foreach ($res as $r) {
                    $arr = $r;
    		 		$arr['jumlah_kebutuhan'] = 100;
    		 		$arr['jumlah_ptk_pns_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_pns_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_pns_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_honorer_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];

    		 		$arr['jumlah_ptk_ada_utk_matpel'] =
                        $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] +
                        $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] +
                        $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] +
                        $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] +
                        $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] +
                        $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
    		 		
    		 		$arr['jumlah_ptk_ada_utk_matpel_sert'] =
        		 		$arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] +
        		 		$arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] +
        		 		$arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] +
        		 		$arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] +
        		 		$arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] +
        		 		$arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
    		 		
    		 		$outArr[] = $arr;
    		 		//$arr = $r->toArray(BasePeer::TYPE_FIELDNAME);
    		 		//$out .= $arr['nama_rincian']." | ".$arr['jumlah_kebutuhan']." | ".$arr['jumlah_ptk_ada_utk_matpel']." | ".$arr['jumlah_ptk_honorer_s1_ada_utk_matpel']." | ".$arr['jumlah_ptk_pns_nons1_ada_utk_matpel']." | ". "<br>\n";
    		 		//$out .= $r->getNamaSekolah()." | ". $r->getNamaMatpel() . " | ". $r->getKekuranganGuru() . " | ". $r->getKelebihanGuru(). "<br>\n";
                }
                $res = $outArr;
            }
            //print_r($res); die;
            //return $out;
    		
            // Ringkas nama rincian
            foreach ($res as $r) {
                
                $nama = $r["nama_rincian"];
                
                $r["nama_rincian"] = str_replace("Muatan Lokal", "Mulok", $nama);
                
                if ($perWilayah) {

                    $r['jumlah_ptk_ada_utk_matpel'] =
                        $r['jumlah_ptk_honorer_s1_ada_utk_matpel'] +
                        $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'] +
                        $r['jumlah_ptk_pns_nons1_ada_utk_matpel'] +
                        $r['jumlah_ptk_pns_s1_ada_utk_matpel'] +
                        $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] +
                        $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
                        
                    $r['jumlah_ptk_ada_utk_matpel_sert'] = 
                        $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] +
                        $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] +
                        $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] +
                        $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] +
                        $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] +
                        $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
                }
                
                $outArr[] = $r;
            }
            
            $res = $outArr;
            
    		if ($format == 'json') {
                
    		    return $this->tableToJson($res, $count, $summaryArr, array('kebutuhan_guru_sd_id'), $start, $limit);
                
    		} else if ($format == 'array') { 
    		    
    		    return $res;
    		    
    		}
            
    	}
    }

    public function unduhPerhitunganKgSd (Request $request, Application $app) {
        
        $data = $this->getPerhitunganKgSd($request, $app, "array");
        //print_r($data); die;
        
        //error_reporting(E_ALL);
        // Set filters from request
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        
        $c = new \Criteria();
        $c->add(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah."%", \Criteria::LIKE);
        $wil = MstWilayahPeer::doSelectOne($c);
        $wilName = $wil->getNama();
        
        $rsg = $request->get('rsg');
        $rsgArr = array("", "Existing", "PP 74", "SNP", "SPM");
        $rsgStr = $rsgArr[$rsg];
        
        $bm = $request->get('bm');
        //$bmArr = array("24 jam", "36 jam", "40 jam");
        $bmStr = $bm." jam";
                
        $rsgbk = $request->get('rsgbk');
        
        $filterPns = $request->get('is_pns');
        $filterPnsStr = ($filterPns) ? "Formasi CPNS dihitung" : "Formasi CPNS tidak Dihitung";
        
        $filterPensiun = $request->get('is_pensiun');
        $filterPensiunStr = ($filterPensiun) ? "Guru Pensiun tidak Dihitung" : "Guru Pensiun Dihitung";
        
        // Set titles
        $title = "Simulasi Perhitungan KG SD";        
        $subtitle = "Wilayah: $wilName, Rasio siswa/guru: $rsgStr, Beban mengajar: $bm, $filterPnsStr, $filterPensiunStr";
        //$subtitle = "Semester: $smt, Tahun: $tahun";
        
        $file = __DIR__."/templates/simulasi_sd.xlsx";
        $namaFile = "simulasi_sd.xlsx";
        
        try {
            // I/O
            $reader = \PHPExcel_IOFactory::createReaderForFile($file);
            $excelFile = new \PHPExcel();
            $excelFile = $reader->load($file);
             
            $ds = $excelFile->getSheet(0);
            //$ds = $excelFile->getSheetByName("data");
            $ds->getCell("A1")->setValue($title);
            $ds->getCell("A2")->setValue($subtitle);
             
            $alphas = range("A", "Z");
            
            $i = 0;

            $styleArrayHeader = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                            ),
                    )
            );
            
            $styleArray = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'right' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'left' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_NONE,
                            ),
                    )
            );
            
            $styleArrayFooter = array(
                'alignment' => array(
                    'indent' => 1
                ),
                'font' => array(
                    'name' => 'Calibri',
                    'size' => '12'
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'argb' => '00000000',
                        )
                    ),
                    'bottom' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ),
                )
            );
            
            $lastColumn = 'R';
            
            foreach (range('A', $lastColumn) as $alphabet) {
                //$ds->duplicateStyleArray($styleArrayHeader, $alphabet."4");
            }
            
            $row = 6;
            $i = 1;
            
            /*
             *             [perhitungan_rasio_id] => 1
            [bentuk_pendidikan_id] => 5
            [mata_pelajaran_id] => 3500
            [nama] => Mulok Bhs Daerah
            [mp1] => 2
            [mp2] => 2
            [mp3] => 2
            [mp4] => 2
            [mp5] => 2
            [mp6] => 2
            [gt_org] => 52063
            [gt_jam] => 1249512
            [k1] => 171285
            [k2] => 170451
            [k3] => 166449
            [k4] => 164445
            [k5] => 163884
            [k6] => 161746
            [jr] => 998260
            [kg_jam] => 1996520
            [kg_org] => 83188.333333333
            [lebih_jam] => 0
            [kurang_jam] => 747008
            [lebih_org] => 0
            [kurang_org] => 31125.333333333
            [sekolah_id] => 
            [tahun_ajaran_id] => 
            [rasio_gk] => 0
            [jumlah_siswa] => 0
            [kg] => 0
             */
            foreach ($data as $d)
            {
                $ds->getCell("A".$row)->setValue($i++);
                $ds->getCell("B".$row)->setValue($d["nama"]);
                $ds->getCell("C".$row)->setValue($d["mp1"]);
                $ds->getCell("D".$row)->setValue($d["k1"]);
                $ds->getCell("E".$row)->setValue($d["k2"]);
                $ds->getCell("F".$row)->setValue($d["k3"]);
                $ds->getCell("G".$row)->setValue($d["k4"]);
                $ds->getCell("H".$row)->setValue($d["k5"]);
                $ds->getCell("I".$row)->setValue($d["k6"]);
                $ds->getCell("J".$row)->setValue($d["jr"]);
                $ds->getCell("K".$row)->setValue($d["kg_jam"]);
                $ds->getCell("L".$row)->setValue($d["gt_jam"]);
                $ds->getCell("M".$row)->setValue($d["kg_org"]);
                $ds->getCell("N".$row)->setValue($d["gt_org"]);
                $ds->getCell("O".$row)->setValue($d["lebih_jam"]);
                $ds->getCell("P".$row)->setValue($d["kurang_jam"]);
                $ds->getCell("Q".$row)->setValue($d["lebih_org"]);
                $ds->getCell("R".$row)->setValue($d["kurang_org"]);
                
                foreach (range('C', $lastColumn) as $alphabet) {
                    $ds->getStyle($alphabet.$row)->getNumberFormat()->setFormatCode('#,##');
                }
                
                $row++;
            }            
        
            
        } catch (Exception $e){
            
        }
        
        $filename = $namaFile;
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
        $objWriter->save('php://output'); die;
        
        
    }            
    
    public function perhitunganKgSd (Request $request, Application $app) {
        
        return $this->getPerhitunganKgSd($request, $app, "json");
    }
    
    
    public function getPerhitunganKgSd (Request $request, Application $app, $outputType) {
        
        // Set filters from request
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
        
        // Listing matpels for main reference
        $mps = MataPelajaranPeer::doSelect(new \Criteria());
        foreach ($mps as $m) {
            $arrMp[$m->getPrimaryKey()] = $m->getNama();
        }
        
        // Listing kurikulum to get hours per matpel
        $cmpk = new \Criteria();
        $cmpk->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, 3);
        $mpks = MataPelajaranKurikulumPeer::doSelect($cmpk);
    
        foreach ($mpks as $m) {
    
            $tingkatId = $m->getTingkatPendidikanId();
            $mpJenjang = "mp".$tingkatId;
    
            $arrMpk[$m->getMataPelajaranId()]['perhitungan_rasio_id'] = 0;
            $arrMpk[$m->getMataPelajaranId()]['bentuk_pendidikan_id'] = 5;
            $arrMpk[$m->getMataPelajaranId()]['mata_pelajaran_id'] = $m->getMataPelajaranId();
            $arrMpk[$m->getMataPelajaranId()]['nama'] = $arrMp[$m->getMataPelajaranId()];
            $arrMpk[$m->getMataPelajaranId()][$mpJenjang] = $m->getJumlahJamMaksimum();
        }
    
        // Membaca Data Rombel
        // Di sini criteria diatur, kalau searchnya per sekolah, keluar yg mana, kalo per wilayah bagaimana.

        // Grouping berdasar level wilayah
        if ($levelWilayah < 3) {
    
            $crbl = new \Criteria();
            $crbl->clearSelectColumns();
    
            //$crbl->addAsColumn('kode_wilayah', RombelSekolahSdPeer::KODE_WILAYAH);
    
            $crbl->addAsColumn('jml_rbl_ktsp_1', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_1.')');
            $crbl->addAsColumn('jml_rbl_ktsp_2', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_2.')');
            $crbl->addAsColumn('jml_rbl_ktsp_3', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_3.')');
            $crbl->addAsColumn('jml_rbl_ktsp_4', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_4.')');
            $crbl->addAsColumn('jml_rbl_ktsp_5', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_5.')');
            $crbl->addAsColumn('jml_rbl_ktsp_6', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_6.')');
    
            $crbl->addAsColumn('jml_rbl_2013_1', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_1.')');
            $crbl->addAsColumn('jml_rbl_2013_2', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_2.')');
            $crbl->addAsColumn('jml_rbl_2013_3', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_3.')');
            $crbl->addAsColumn('jml_rbl_2013_4', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_4.')');
            $crbl->addAsColumn('jml_rbl_2013_5', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_5.')');
            $crbl->addAsColumn('jml_rbl_2013_6', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_6.')');
    
            $crbl->addAsColumn('jml_siswarbl_ktsp_1', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_1.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_2', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_2.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_3', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_3.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_4', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_4.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_5', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_5.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_6', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_6.')');
    
            $crbl->addAsColumn('jml_siswarbl_2013_1', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_1.')');
            $crbl->addAsColumn('jml_siswarbl_2013_2', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_2.')');
            $crbl->addAsColumn('jml_siswarbl_2013_3', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_3.')');
            $crbl->addAsColumn('jml_siswarbl_2013_4', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_4.')');
            $crbl->addAsColumn('jml_siswarbl_2013_5', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_5.')');
            $crbl->addAsColumn('jml_siswarbl_2013_6', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_6.')');
            
            // Compare kode wilayah with the school's kode wilayah. 
            // Filtering done by cleaning code wilayah. Misal: Jabar: 020000 --> 02. 
            // Sehingga semua sekolah dengan kepala 02 bisa kena. Jadi ga usah grouping terlalu repot
            
            $crbl->add(RombelSekolahSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addGroupByColumn(RombelSekolahSdPeer::KODE_WILAYAH);
            //$crbl->add(RombelSekolahSdPeer::SEMESTER_ID, $tahun.$smt);
    
            $stmt = RombelSekolahSdPeer::doSelectStmt($crbl);
            //print_r($stmt);
    
            try {
                $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                //print_r($res);
            } catch(\Exception $e) {
                die ($e->getMessages());
            }
            
            // Hasilnya memang cuman satu baris. Semua dijumlah
            $r = $res[0];
    
            // Utk RSG = 1, jumlah rombel dihitung dari keadaan rombel sesuai data
            if ($rsg == 1) {
                	
                $arr['k1'] = $r['jml_rbl_ktsp_1'] + $r['jml_rbl_2013_1'];
                $arr['k2'] = $r['jml_rbl_ktsp_2'] + $r['jml_rbl_2013_2'];
                $arr['k3'] = $r['jml_rbl_ktsp_3'] + $r['jml_rbl_2013_3'];
                $arr['k4'] = $r['jml_rbl_ktsp_4'] + $r['jml_rbl_2013_4'];
                $arr['k5'] = $r['jml_rbl_ktsp_5'] + $r['jml_rbl_2013_5'];
                $arr['k6'] = $r['jml_rbl_ktsp_6'] + $r['jml_rbl_2013_6'];

            // Untuk lainnya, dihitung dari rasio terpilih, kalkulasi dari jumlah siswa    
            } else {
    
                $arr['k1'] = ($r['jml_siswarbl_ktsp_1'] + $r['jml_siswarbl_2013_1'])/$rsg;
                $arr['k2'] = ($r['jml_siswarbl_ktsp_2'] + $r['jml_siswarbl_2013_2'])/$rsg;
                $arr['k3'] = ($r['jml_siswarbl_ktsp_3'] + $r['jml_siswarbl_2013_3'])/$rsg;
                $arr['k4'] = ($r['jml_siswarbl_ktsp_4'] + $r['jml_siswarbl_2013_4'])/$rsg;
                $arr['k5'] = ($r['jml_siswarbl_ktsp_5'] + $r['jml_siswarbl_2013_5'])/$rsg;
                $arr['k6'] = ($r['jml_siswarbl_ktsp_6'] + $r['jml_siswarbl_2013_6'])/$rsg;
                	
                // 			$arr['k1'] = $r['jml_siswarbl_ktsp_1']/$rsg;
                // 			$arr['k2'] = $r['jml_siswarbl_ktsp_2']/$rsg;
                // 			$arr['k3'] = $r['jml_siswarbl_ktsp_3']/$rsg;
                // 			$arr['k4'] = $r['jml_siswarbl_ktsp_4']/$rsg;
                // 			$arr['k5'] = $r['jml_siswarbl_ktsp_5']/$rsg;
                // 			$arr['k6'] = $r['jml_siswarbl_ktsp_6']/$rsg;
                	
            }
    
        // Grouping per sekolah
        } else {
    
            $crbl = new \Criteria();
            $crbl->add(RombelSekolahSdPeer::SEKOLAH_ID, $kodeWilayah);
    
            $r = RombelSekolahSdPeer::doSelectOne($crbl);
    
            if ($rsg == 1) {
                	
                $arr['k1'] = $r->getJmlRblKtsp1() + $r->getJmlRbl20131();
                $arr['k2'] = $r->getJmlRblKtsp2() + $r->getJmlRbl20132();
                $arr['k3'] = $r->getJmlRblKtsp3() + $r->getJmlRbl20133();
                $arr['k4'] = $r->getJmlRblKtsp4() + $r->getJmlRbl20134();
                $arr['k5'] = $r->getJmlRblKtsp5() + $r->getJmlRbl20135();
                $arr['k6'] = $r->getJmlRblKtsp6() + $r->getJmlRbl20136();
                	
            } else {
    
                $arr['k1'] = ($r->getJmlSiswarbl20131() + $r->getJmlSiswarblKtsp1())/$rsg;
                $arr['k2'] = ($r->getJmlSiswarbl20132() + $r->getJmlSiswarblKtsp2())/$rsg;
                $arr['k3'] = ($r->getJmlSiswarbl20133() + $r->getJmlSiswarblKtsp3())/$rsg;
                $arr['k4'] = ($r->getJmlSiswarbl20134() + $r->getJmlSiswarblKtsp4())/$rsg;
                $arr['k5'] = ($r->getJmlSiswarbl20135() + $r->getJmlSiswarblKtsp5())/$rsg;
                $arr['k6'] = ($r->getJmlSiswarbl20136() + $r->getJmlSiswarblKtsp6())/$rsg;
                	
            }
        }
        $arrRombel = $arr;
    
        // Membaca data Guru Tersedia (GT)
        // Dari PTK Tersedia
        // Digroup per wilayah atau dihitung per sekolah
        
        
        // Group per wilayah
        if ($levelWilayah < 3) {
    
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSdPeer::PTK_ID.')');
            $cgt->add(GuruPermatpelSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            
            // Filter PNS
            if ($filterPns) {
                $cgt->add(GuruPermatpelSdPeer::IS_PNS, 1, \Criteria::EQUAL);
            }
            // Pensiun
            if ($filterPensiun) {
                $cgt->add(GuruPermatpelSdPeer::TGL_LAHIR, strval(date("Y")+1-60)."-01-01", \Criteria::GREATER_EQUAL);
            }
            
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
    
            $stmt = GuruPermatpelSdPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            //print_r($gts); die;
    
            foreach ($gts as $g) {
                if ($arrMpk[ $g['mata_pelajaran_id'] ]) {
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_org'] = $g['jumlah_gt'];
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_jam'] = $g['jumlah_gt'] * $bm;
                }
            }
    
        // Hitung per sekolah
        } else {
    
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSdPeer::PTK_ID.')');
            $cgt->add(GuruPermatpelSdPeer::SEKOLAH_ID, $kodeWilayah, \Criteria::LIKE);
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
    
            $stmt = GuruPermatpelSdPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            //print_r($gts); die;
    
            foreach ($gts as $g) {
                if ($arrMpk[ $g['mata_pelajaran_id'] ]) {
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_org'] = $g['jumlah_gt'];
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_jam'] = $g['jumlah_gt'] * $bm;
                }
            }
    
        }
    
    
        $z = 1;
        
        // Kombinasi dan kalkulasi semua
        foreach ($arrMpk as $a) {
    
            $jtm = 0;
    
            for ($i = 1; $i <= 6; $i++) {
                $a["k$i"] = $arrRombel["k$i"];
                $jr += $arrRombel["k$i"];
                $jtm += $arrRombel["k$i"] * $a["mp$i"];
            }
    
            $a["jr"] = $jr;
            $a["kg_jam"] = $jtm;
            $a["kg_org"] = $jtm/$bm;
    
            $a["lebih_jam"] = ($a["gt_jam"] > $a["kg_jam"]) ? $a["gt_jam"] - $a["kg_jam"] : 0 ;
            $a["kurang_jam"] = ($a["kg_jam"] > $a["gt_jam"]) ? $a["kg_jam"] - $a["gt_jam"] : 0;
    
            $a["lebih_org"] = ($a["gt_org"] > $a["kg_org"]) ? $a["gt_org"] - $a["kg_org"] : 0 ;
            $a["kurang_org"] = ($a["kg_org"] > $a["gt_org"]) ? $a["kg_org"] - $a["gt_org"] : 0;
    
    
            //$a["perhitungan_rasio_id"] = $a["mata_pelajaran_id"];
            $a["sekolah_id"] = $sekolahId;
            $a["tahun_ajaran_id"] = $tahun;
            $a["rasio_gk"] = 0;
            $a["jumlah_siswa"] = 0;
            $a["kg"] = 0;
    
            $jr = 0;
    
            if (!stripos($a['nama'], "Agama")) {
                $a["perhitungan_rasio_id"] = $z++;
                $arrOut[] = $a;
            }
    
        }
        
        switch ($outputType) {
            case "json":
                return tableJson($arrOut, sizeof($arrOut), array('perhitungan_rasio_id'));
                break;
            case "array":
                return $arrOut;
                break;
            default:
                return tableJson($arrOut, sizeof($arrOut), array('perhitungan_rasio_id'));
                break;
        }
        
    
    }

    
    public function perhitunganKgSdWilayah (Request $request, Application $app) {
        return $this->hitungKgSdWilayah($request, $app, 'json');
    }
    
    public function hitungKgSdWilayah (Request $request, Application $app, $format) {
    
        $matpelId = $request->get('mata_pelajaran_id');
        $kodeWilayah = trim($request->get('kode_wilayah'));
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
        
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::MST_KODE_WILAYAH, $kodeWilayah."%", \Criteria::LIKE);
        $wilayahs = MstWilayahPeer::doSelect($cwil);
        
        // Buat Array Nama Wilayah
        foreach($wilayahs as $w) {
            $arrNamaWilayah[$w->getKodeWilayah()]['nama'] =  $w->getNama();
            $arrNamaWilayah[$w->getKodeWilayah()]['kode_wilayah'] =  $w->getKodeWilayah();
            $arrNamaWilayah[$w->getKodeWilayah()]['mst_kode_wilayah'] =  $w->getMstKodeWilayah();
        }
        
        // Listing kurikulum to get hours per matpel
        $cmpk = new \Criteria();
        
        //// WOY WOY SETTING KURIKULUM YANG DIPAKE NGITUNG DI SINI WOY
        $cmpk->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, 3);
        $mpks = MataPelajaranKurikulumPeer::doSelect($cmpk);
        
        foreach ($mpks as $m) {
            $tingkatId = $m->getTingkatPendidikanId();
            $mpJenjang = "mp".$tingkatId;
            $arrMpk[$m->getMataPelajaranId()][$mpJenjang] = $m->getJumlahJamMaksimum();
        }
                        
        // Grouping berdasar level wilayah
        // Updated to correct each
        $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
        
        if ($levelWilayah < 3) {
        // if (false) {
            
            $crbl = new \Criteria();
            $crbl->clearSelectColumns();
            
            $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
            $crbl->addAsColumn('kode_wilayah_sub', 'left('.RombelSekolahSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            
            $crbl->addAsColumn('jml_rbl_ktsp_1', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_1.')');
            $crbl->addAsColumn('jml_rbl_ktsp_2', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_2.')');
            $crbl->addAsColumn('jml_rbl_ktsp_3', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_3.')');
            $crbl->addAsColumn('jml_rbl_ktsp_4', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_4.')');
            $crbl->addAsColumn('jml_rbl_ktsp_5', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_5.')');
            $crbl->addAsColumn('jml_rbl_ktsp_6', 'sum('.RombelSekolahSdPeer::JML_RBL_KTSP_6.')');
        
            $crbl->addAsColumn('jml_rbl_2013_1', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_1.')');
            $crbl->addAsColumn('jml_rbl_2013_2', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_2.')');
            $crbl->addAsColumn('jml_rbl_2013_3', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_3.')');
            $crbl->addAsColumn('jml_rbl_2013_4', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_4.')');
            $crbl->addAsColumn('jml_rbl_2013_5', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_5.')');
            $crbl->addAsColumn('jml_rbl_2013_6', 'sum('.RombelSekolahSdPeer::JML_RBL_2013_6.')');
        
            $crbl->addAsColumn('jml_siswarbl_ktsp_1', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_1.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_2', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_2.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_3', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_3.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_4', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_4.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_5', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_5.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_6', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_KTSP_6.')');
        
            $crbl->addAsColumn('jml_siswarbl_2013_1', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_1.')');
            $crbl->addAsColumn('jml_siswarbl_2013_2', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_2.')');
            $crbl->addAsColumn('jml_siswarbl_2013_3', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_3.')');
            $crbl->addAsColumn('jml_siswarbl_2013_4', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_4.')');
            $crbl->addAsColumn('jml_siswarbl_2013_5', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_5.')');
            $crbl->addAsColumn('jml_siswarbl_2013_6', 'sum('.RombelSekolahSdPeer::JML_SISWARBL_2013_6.')');
        
            // Compare kode wilayah with the school's kode wilayah.
            // Filtering done by cleaning code wilayah. Misal: Jabar: 020000 --> 02.
            // Sehingga semua sekolah dengan kepala 02 bisa kena. Jadi ga usah grouping terlalu repot
            
            $crbl->add(RombelSekolahSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addGroupByColumn(RombelSekolahSdPeer::KODE_WILAYAH);
            //$crbl->add(RombelSekolahSdPeer::SEMESTER_ID, $tahun.$smt);
            
            // Bedanya dengan yang di atas, di sini ada grouping
            $crbl->addGroupByColumn('left('.RombelSekolahSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            
            // Ordering nya juga jangan lupa biar rapi datanya
            $crbl->addAscendingOrderByColumn('left('.RombelSekolahSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            
            $stmt = RombelSekolahSdPeer::doSelectStmt($crbl);
            
            try {
                $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                //print_r($res);
            } catch(\Exception $e) {
                die ($e->getMessages());
            }
            
            // Sehingga res nya tidak cuman satu 
            //$r = $res[0];
            
            // Tapi banyak. Kita loop
            //print_r($res); die;
            
            foreach ($res as $r) {
                
                // Identitas dulu
                $kode_wilayah = str_pad($r['kode_wilayah_sub'], 6, "0")." ";
                $arr['nama'] = $arrNamaWilayah[$kode_wilayah]['nama'];
                $arr['kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['mst_kode_wilayah'];
                
                
                // Utk RSG = 1, jumlah rombel dihitung dari keadaan rombel sesuai data
                if ($rsg == 1) {
                 
                    $arr['k1'] = $r['jml_rbl_ktsp_1'] + $r['jml_rbl_2013_1'];
                    $arr['k2'] = $r['jml_rbl_ktsp_2'] + $r['jml_rbl_2013_2'];
                    $arr['k3'] = $r['jml_rbl_ktsp_3'] + $r['jml_rbl_2013_3'];
                    $arr['k4'] = $r['jml_rbl_ktsp_4'] + $r['jml_rbl_2013_4'];
                    $arr['k5'] = $r['jml_rbl_ktsp_5'] + $r['jml_rbl_2013_5'];
                    $arr['k6'] = $r['jml_rbl_ktsp_6'] + $r['jml_rbl_2013_6'];
                    
                    
                // Untuk lainnya, dihitung dari rasio terpilih, kalkulasi dari jumlah siswa
                } else {
            
                    $arr['k1'] = ($r['jml_siswarbl_ktsp_1'] + $r['jml_siswarbl_2013_1'])/$rsg;
                    $arr['k2'] = ($r['jml_siswarbl_ktsp_2'] + $r['jml_siswarbl_2013_2'])/$rsg;
                    $arr['k3'] = ($r['jml_siswarbl_ktsp_3'] + $r['jml_siswarbl_2013_3'])/$rsg;
                    $arr['k4'] = ($r['jml_siswarbl_ktsp_4'] + $r['jml_siswarbl_2013_4'])/$rsg;
                    $arr['k5'] = ($r['jml_siswarbl_ktsp_5'] + $r['jml_siswarbl_2013_5'])/$rsg;
                    $arr['k6'] = ($r['jml_siswarbl_ktsp_6'] + $r['jml_siswarbl_2013_6'])/$rsg;
                    
                }
                
                // Kita buat list associative dengan index kode wilayah
                $arrRombel[$kode_wilayah] = $arr;
            }  
            
        } // end if ($levelWilayah < 3)
        
        else 
        
        {
            $crbl = new \Criteria();
            // $crbl->add(RombelSekolahSdPeer::KODE_WILAYAH, $kodeWilayah);
            $crbl->add(RombelSekolahSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addAsColumn('kode_wilayah_sub', 'left('.RombelSekolahSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            
            $res = RombelSekolahSdPeer::doSelect($crbl);
            
            foreach ($res as $r) {
            
                // Identitas dulu
                $kode_wilayah = $r->getSekolahId();
                $arr['nama'] = $r->getNama();
                $arr['kode_wilayah'] = $r->getSekolahId();
                $arr['mst_kode_wilayah'] = $r->getKodeWilayah();
                
                if ($rsg == 1) {
                     
                    $arr['k1'] = $r->getJmlRblKtsp1() + $r->getJmlRbl20131();
                    $arr['k2'] = $r->getJmlRblKtsp2() + $r->getJmlRbl20132();
                    $arr['k3'] = $r->getJmlRblKtsp3() + $r->getJmlRbl20133();
                    $arr['k4'] = $r->getJmlRblKtsp4() + $r->getJmlRbl20134();
                    $arr['k5'] = $r->getJmlRblKtsp5() + $r->getJmlRbl20135();
                    $arr['k6'] = $r->getJmlRblKtsp6() + $r->getJmlRbl20136();
                     
                } else {
            
                    $arr['k1'] = ($r->getJmlSiswarbl20131() + $r->getJmlSiswarblKtsp1())/$rsg;
                    $arr['k2'] = ($r->getJmlSiswarbl20132() + $r->getJmlSiswarblKtsp2())/$rsg;
                    $arr['k3'] = ($r->getJmlSiswarbl20133() + $r->getJmlSiswarblKtsp3())/$rsg;
                    $arr['k4'] = ($r->getJmlSiswarbl20134() + $r->getJmlSiswarblKtsp4())/$rsg;
                    $arr['k5'] = ($r->getJmlSiswarbl20135() + $r->getJmlSiswarblKtsp5())/$rsg;
                    $arr['k6'] = ($r->getJmlSiswarbl20136() + $r->getJmlSiswarblKtsp6())/$rsg;
                     
                }
                
                $arrRombel[$kode_wilayah] = $arr;
                
            }
            
        }
        
        // Membaca data Guru Tersedia (GT)
        // Dari PTK Tersedia
        // Digroup per wilayah atau dihitung per sekolah
        
        
        // Group per wilayah
        if ($levelWilayah < 3) {
        // if (false) {
            
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
                        
            // The identity columns
            $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
            $cgt->addAsColumn('kode_wilayah_sub', 'left('.GuruPermatpelSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSdPeer::PTK_ID.')');

            // Where
            
            // Per Matpel brur
            $cgt->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $matpelId);
            
            // Parent
            $cgt->add(GuruPermatpelSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);

            // Filter PNS
            if ($filterPns) {
                $cgt->add(GuruPermatpelSdPeer::IS_PNS, '1');
            }
            // Pensiun
            if ($filterPensiun) {
                $cgt->add(GuruPermatpelSdPeer::TGL_LAHIR, strval(date("Y")+1-60)."-01-01", \Criteria::GREATER_EQUAL);
            }
            
            // Grouping
            $cgt->addGroupByColumn('left('.GuruPermatpelSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            
            // Ordering
            $cgt->addAscendingOrderByColumn('left('.GuruPermatpelSdPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            
            // RUN it
            $stmt = GuruPermatpelSdPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($gts as $r) {
            
                // Identitas dulu
                $arr = null;
                $kode_wilayah = str_pad($r['kode_wilayah_sub'], 6, "0")." ";
                
                $arr['nama'] = $arrNamaWilayah[$kode_wilayah]['nama'];
                $arr['kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['mst_kode_wilayah'];
                $arr['mata_pelajaran_id'] = $r['mata_pelajaran_id'];
                $arr['gt_org'] = $r['jumlah_gt'];
                $arr['gt_jam'] = $r['jumlah_gt'] * $bm;
                
                // Array Guru Tersedia di Wilayah 
                $arrWil[$kode_wilayah] = $arr;
            }
            
            //print_r($arrWil); die;
        } 
        
        
        // Hitung per sekolah
        else 
        {   
            $cgt = new \Criteria();
            
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSdPeer::PTK_ID.')');
            $cgt->clearSelectColumns();
            
            $cgt->addAsColumn('nama', GuruPermatpelSdPeer::NAMA_SEKOLAH);
            $cgt->addAsColumn('kode_wilayah', GuruPermatpelSdPeer::SEKOLAH_ID);
            $cgt->addAsColumn('mst_kode_wilayah', GuruPermatpelSdPeer::KODE_WILAYAH);
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSdPeer::PTK_ID.')');
            
            //$cgt->add(GuruPermatpelSdPeer::KODE_WILAYAH, $kodeWilayah);
            $cgt->add(GuruPermatpelSdPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            $cgt->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $matpelId);            
            
            // Filter PNS
            if ($filterPns) {
                $cgt->add(GuruPermatpelSdPeer::IS_PNS, 1, \Criteria::EQUAL);
            }
            // Pensiun
            if ($filterPensiun) {
                $cgt->add(GuruPermatpelSdPeer::TGL_LAHIR, strval(date("Y")+1-60)."-01-01", \Criteria::GREATER_EQUAL);
            }
            
            // Grouping
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::NAMA_SEKOLAH);
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::SEKOLAH_ID);
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::KODE_WILAYAH);
            $cgt->addGroupByColumn(GuruPermatpelSdPeer::MATA_PELAJARAN_ID);
            
            $stmt = GuruPermatpelSdPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            //echo "$kodeWilayah $matpelId"; die;
            
            //print_r($gts); die;
            
            foreach ($gts as $r) {
            
                // Identitas dulu
                $arr = null;
                $kode_wilayah = $r['kode_wilayah'];
                $arr['nama'] = $r['nama'];
                $arr['kode_wilayah'] = $r['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $r['mst_kode_wilayah'];
                $arr['mata_pelajaran_id'] = $r['mata_pelajaran_id'];
                $arr['gt_org'] = $r['jumlah_gt'];
                $arr['gt_jam'] = $r['jumlah_gt'] * $bm;
            
                // Array Guru Tersedia di Wilayah
                $arrWil[$kode_wilayah] = $arr;
            }
        }
        
        //echo $arr['nama']."<br>";
        //print_r($arrMpk); die;
        //print_r($arrWil); die;
        
        
        $z = 1;
        
        foreach($arrWil as $kw => $a) {
            
            $jtm = 0;
            
            
            for ($i = 1; $i <= 6; $i++) {
                $a["k$i"] = $arrRombel[$kw]["k$i"];
                
                // Masukin nilai Jam per MP
                $matpelId = $a['mata_pelajaran_id'];
                $a["mp$i"] = $arrMpk[ $matpelId ]["mp$i"];
                $jr += $arrRombel[$kw]["k$i"];
                $jtm += $arrRombel[$kw]["k$i"] * $a["mp$i"];
            }

            $a["jr"] = $jr;
            $a["kg_jam"] = $jtm;
            $a["kg_org"] = $jtm/$bm;
            
            $a["lebih_jam"] = ($a["gt_jam"] > $a["kg_jam"]) ? $a["gt_jam"] - $a["kg_jam"] : 0 ;
            $a["kurang_jam"] = ($a["kg_jam"] > $a["gt_jam"]) ? $a["kg_jam"] - $a["gt_jam"] : 0;
            
            $a["lebih_org"] = ($a["gt_org"] > $a["kg_org"]) ? $a["gt_org"] - $a["kg_org"] : 0 ;
            $a["kurang_org"] = ($a["kg_org"] > $a["gt_org"]) ? $a["kg_org"] - $a["gt_org"] : 0;
            
            $a["perhitungan_rasio_id"] = $z++;
            
            if ($a['nama'] != '') {
                $arrOut[] = $a;
            }
            
        }
        //print_r($arrOut); die;
        //echo sizeof($arrOut); die;    
        // Ok. now data listed in $arrOut consisted of sekolahs within the $kodeWilayah.
        // What to do now is group it into shit, if ($levelWilayah < 3) {
        
        
        if ($levelWilayah < 3) {
            
        
            // Lets get list of wilayah under $kodeWilayah
            $c = new \Criteria();
            $c->add(MstWilayahPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            $c->add(MstWilayahPeer::ID_LEVEL_WILAYAH, $levelWilayah + 1);
            $wilayahs = MstWilayahPeer::doSelect($c);
            
            //print_r($wilayahs); die;
            
            
            foreach ($arrOut as $a) {
                
                //echo $a['nama']."<br>";
                 
                foreach ($wilayahs as $w) {
                    
                    //echo "= comparing ".substr($a['mst_kode_wilayah'], 0, $wilayahChildDigitCount)." with ". substr_replace(Util::cleanKodeWilayah($w->getKodeWilayah()), "", -1) ."<br>";
                    
                    //echo "= comparing ".substr($a['kode_wilayah'], 0, $wilayahChildDigitCount)." with ". substr_replace(Util::cleanKodeWilayah($w->getKodeWilayah()), "", -1) ."<br>";
                    
                    if ( substr($a['kode_wilayah'], 0, $wilayahChildDigitCount) ==  trim(str_replace("%", "", Util::cleanKodeWilayah($w->getKodeWilayah()))) ) {
                        //echo "&nbsp;&nbsp;&nbsp;&nbsp;FOUND<br>\r\n";
                        $wilArr[$w->getKodeWilayah()]['nama'] = $w->getNama();
                        $wilArr[$w->getKodeWilayah()]['kode_wilayah'] = $w->getKodeWilayah();
                        $wilArr[$w->getKodeWilayah()]['lebih_jam'] += $a['lebih_jam'];
                        $wilArr[$w->getKodeWilayah()]['kurang_jam'] += $a['kurang_jam'];
                        $wilArr[$w->getKodeWilayah()]['lebih_org'] += $a['lebih_org'];
                        $wilArr[$w->getKodeWilayah()]['kurang_org'] += $a['kurang_org'];
                    }
                    
                }
                
            }
            
            //return sizeof($wilArr); die;
            $l = 1;
            
            foreach ($wilArr as $kode_wilayah => $arr) {
                $arr['perhitungan_rasio_id'] = $l++;
                $out[] = $arr;
            }
            
            //print_r($out); die;
            
        } else {
            $out = $arrOut;
        }
        

        
        //return sizeof($out); die;
        //print_r($arrOut); die;
        //return tableJson($arrOut, sizeof($arrOut), array('perhitungan_rasio_id'));
        //$outStr = "";
        //foreach ($arrWil as $key => $val) {
        //   $outStr .= "$key: ". $val['k1']. " | " .$val['k2']. " | " .$val['k3']. " | " .$val['k4']. " | " .$val['k5']. " | " . $val['k6']. "<br>";
        //}
        //return $outStr;
        if ($format == 'json') {
            return tableJson($out, sizeof($out), array('perhitungan_rasio_id')); 
        } else {
            return $out;
        }
        
    }
    
    
    public function printing (Request $request, Application $app) {
        $outJson = DataSd::getKebutuhanGuru($request, 'wilayah', $app, true);
        return $outJson;
    }

    public function printingSimulasiPerWilayah(Request $request, Application $app) {
        
        $data  = $this->hitungKgSdWilayah($request, $app, 'array');
        $matpelId = $request->get('mata_pelajaran_id');
        $per = $request->get('per');
        $namaWilayah = $request->get('nama_wilayah');
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
        
        
        $objWilayah = Util::getObjWilayah($kodeWilayah);
        $objMatpel = Util::getObjMatpel($matpelId);
        
        $subTitle = ($per == 'wilayah') ?
        ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
        ('Wilayah: '.$objWilayah->getNama());
        
        $outStr = Printing::page(TEMPLATEROOT."grid_simulasi.twig", array(
            "title" => "Simulasi KG SD per Wilayah",
            "subtitle" => $subTitle,
            "data" => $data
        ));
        return $outStr;
        
    }
    
    public function excelSimulasiPerWilayah(Request $request, Application $app) {
        
        $data  = $this->hitungKgSdWilayah($request, $app, 'array');
        
        $matpelId = $request->get('mata_pelajaran_id');
        $per = $request->get('per');
        $namaWilayah = $request->get('nama_wilayah');
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
        
        
        $objWilayah = Util::getObjWilayah($kodeWilayah);
        $objMatpel = Util::getObjMatpel($matpelId);
        
        $title = "Simulasi Kelebihan/Kekurangan KG SD Per Wilayah";
        
        $subTitle = ($per == 'wilayah') ?
        ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
        ('Wilayah: '.$objWilayah->getNama());
        
        $filename = "simulasikg-{$per}-".stripslashes(strtolower(underscoreCapitalize($objWilayah->getNama()))."-matpel-".strtolower(underscoreCapitalize($objMatpel->getNama()))).".xls";
        
        Excel::excelSimulasiPerWilayah(TEMPLATEROOT."grid_per_matpel.xlsx", array(
                "per" => $per,
                "skup" => $skup,
                "filename" => $filename,
                "title" => $title,
                "subtitle" => $subTitle,
                "wilayah" => $objWilayah->getNama(),
                "matpel" => $objMatpel->getNama(),
                "headers" => array("Nama", "+ (jam)", "+ (org)", "- (jam)", "- (org)"), 
                "data" => $data
        ));
        
    }

    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     * @return string The interpolated query
     */
    public static function interpolateQuery($query, $params) {
        $keys = array();
                
        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }
        
        $query = preg_replace($keys, $params, $query, 1, $count);
    
        #trigger_error('replaced '.$count.' keys');
        
        return $query;
    }
    
    public function tableToJson($array, $rownum, $summaryArray, $id, $start="0", $limit="20") {
        //print_r($array); die();
        $rows = sizeof($array) > 0 ? json_encode($array) : "[]";
        $summaryData = sizeof($summaryArray) > 0 ? json_encode($summaryArray) : "{}";
        $result = sprintf("{ 'results' : %s, 'id' : '%s', 'start': %s, 'limit': %s, 'rows' : %s, 'summaryData': %s }", $rownum, $id[0], $start, $limit, $rows, $summaryData);
        //print_r($summaryData);die;
        return $result;
    }
    
}
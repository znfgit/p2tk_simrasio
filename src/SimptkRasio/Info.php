<?php

/**
 * DataSd Class
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\PtkSdAdaPeer;
use SimptkRasio\Model\PtkSmpAdaPeer;
use SimptkRasio\Model\PtkTersediaPeer;

class Info {
    
    public function listPtkSekolah (Request $request, Application $app) {
    
        $bentukPendidikanId = $request->get('bentuk_pendidikan_id');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $mataPelajaranId = $request->get('mata_pelajaran_id');
        $guruBk = $request->get('guru_bk');
        $statusSekolah = $request->get('status_sekolah');
        $purpose = $request->get('purpose');
        $limit = $request->get('limit');
        $start = $request->get('start');
        
        $gurus = array();
        
        if ($levelWilayah < 4) {
            
            //$kodeWilayah = getSignificantDigits($sekolahId)."%";

            $sig = getSignificantDigits($sekolahId);
            if (strlen($sig) % 2 == 0) {
                // if even, do nothin
            } else {
                // if odd, means the significant digits still holds a significant number. i.e 2.
                $sig .= "0";
            }
            
            $kodeWilayah = $sig."%";
            
            if (!$guruBk) {
                
                if ($bentukPendidikanId == 5) {
                    
                    $c = new  \Criteria();

                    $c->add(PtkSdAdaPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
                    $c->add(PtkSdAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                    $c->add(PtkSdAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                    $c->add(PtkSdAdaPeer::NAMA_SEKOLAH, "SD%", \Criteria::LIKE);
                    if ($purpose == "sertifikasi") {
                        $c->add(PtkSdAdaPeer::IS_SERTIFIKASI, 1);
                    }
                    $count = PtkSdAdaPeer::doCount($c);
                    
                    $c->setLimit($limit);
                    $c->setOffset($start);
                    $gurus = PtkSdAdaPeer::doSelect($c);
// =======
//                     $c->add(GuruPermatpelSdPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
//                     $c->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                    
//                     $count = GuruPermatpelSdPeer::doCount($c);
                    
//                     $c->setLimit($limit);
//                     $c->setOffset($start);
//                     $gurus = GuruPermatpelSdPeer::doSelect($c);
// >>>>>>> 5e74d564ba36ad39ef2edbfce55b4f3cbc9163b7
                    
                } else if ($bentukPendidikanId == 6) {
                    
                    $c = new  \Criteria();

                    $c->add(PtkSmpAdaPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
                    $c->add(PtkSmpAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                    $c->add(PtkSmpAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                    if ($purpose == "sertifikasi") {
                        $c->add(PtkSmpAdaPeer::IS_SERTIFIKASI, 1);
                    }                    
                    $count = PtkSmpAdaPeer::doCount($c);
                    
                    $c->setLimit($limit);
                    $c->setOffset($start);
                    $gurus = PtkSmpAdaPeer::doSelect($c);
// =======
//                     $c->add(GuruPermatpelSmpPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
//                     $c->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
//                     $count = GuruPermatpelSmpPeer::doCount($c);
                    
//                     $c->setLimit($limit);
//                     $c->setOffset($start);
//                     $gurus = GuruPermatpelSmpPeer::doSelect($c);
// >>>>>>> 5e74d564ba36ad39ef2edbfce55b4f3cbc9163b7
                    
                }
                
            } else {
                $c = new \Criteria();
                $c->add(PtkTersediaPeer::KODE_WILAYAH, $sekolahId);
                $c->add(PtkTersediaPeer::IS_GURU_BK, 1);
                $gurus = PtkTersediaPeer::doSelect($c);
            }
            
        } else {
        
            $sekolah = SekolahPeer::retrieveByPK($sekolahId);
            
            if (!$guruBk) {
                
                if ($sekolah->getBentukPendidikanId() == 5) {
            
                    $c = new  \Criteria();

                    $c->add(PtkSdAdaPeer::SEKOLAH_ID, $sekolahId);
                    $c->add(PtkSdAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                    $c->add(PtkSdAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                    if ($purpose == "sertifikasi") {
                        $c->add(PtkSdAdaPeer::IS_SERTIFIKASI, 1);
                    }
                    $gurus = PtkSdAdaPeer::doSelect($c);
                    
// =======
//                     $c->add(GuruPermatpelSdPeer::SEKOLAH_ID, $sekolahId);
//                     $c->add(GuruPermatpelSdPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
//                     $gurus = GuruPermatpelSdPeer::doSelect($c);
// >>>>>>> 5e74d564ba36ad39ef2edbfce55b4f3cbc9163b7
            
                } else if ($sekolah->getBentukPendidikanId() == 6) {
                
                    $c = new  \Criteria();

                    $c->add(PtkSmpAdaPeer::SEKOLAH_ID, $sekolahId);
                    $c->add(PtkSmpAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                    $c->add(PtkSmpAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                    if ($purpose == "sertifikasi") {
                        $c->add(PtkSmpAdaPeer::IS_SERTIFIKASI, 1);
                    }                    
                    $gurus = PtkSmpAdaPeer::doSelect($c);
// =======
//                     $c->add(GuruPermatpelSmpPeer::SEKOLAH_ID, $sekolahId);
//                     $c->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
//                     $gurus = GuruPermatpelSmpPeer::doSelect($c);
// >>>>>>> 5e74d564ba36ad39ef2edbfce55b4f3cbc9163b7
                }
            } else {
                $c = new \Criteria();
                $c->add(PtkTersediaPeer::SEKOLAH_ID, $sekolahId);
                $c->add(PtkTersediaPeer::IS_GURU_BK, 1);
                $gurus = PtkTersediaPeer::doSelect($c);
            }
            
            $count = sizeof($gurus);
            
        }
        
        return tableJson(getArray($gurus, \BasePeer::TYPE_FIELDNAME), $count, array('ptk_id'));
    }
}
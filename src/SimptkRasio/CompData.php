<?php

/**
 * CompData Module
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

use SimptkRasio\Model;
use SimptkRasio\Model\SekolahQuery;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\MataPelajaran;
use SimptkRasio\Model\MataPelajaranPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\KebutuhanGuruSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulumPeer;
use SimptkRasio\Model\KebutuhanGuruSmpPeer;
use SimptkRasio\Model\RombelSekolahSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulum;
use SimptkRasio\Model\RombelSekolahSd;
use SimptkRasio\Model\PtkTersediaPeer;
use SimptkRasio\Model\RefLinierBidstudiMatpelPeer;
use SimptkRasio\Model\RefLinierisasiPeer;
use SimptkRasio\Model\GuruPermatpelSdPeer;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\NominasiPindahGuruSdPeer;
use SimptkRasio\Model\NominasiPindahGuruSd;
use SimptkRasio\Model\RombelSekolahSmpPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;


class CompData {

    public function cariMatpelCombo (Request $request, Application $app) {    
    
        $query = $request->get('query');
    
        $c = new \Criteria();
        if ($query != '') {
            $c->add(MataPelajaranPeer::NAMA, "%$query%", \Criteria::LIKE);
        } else {
            $c->add(MataPelajaranPeer::NAMA, "%Agama%", \Criteria::NOT_LIKE);
        }
    
        $matpels = MataPelajaranPeer::doSelect($c);
    
        $c2 = new \Criteria();
        $c2->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, array(3), \Criteria::IN);
        $kurikulums = MataPelajaranKurikulumPeer::doSelect($c2);
    
        foreach ($matpels as $m) {
    
            foreach ($kurikulums as $k) {
                if ($m->getPrimaryKey() == $k->getMataPelajaranId()) {
                    $outArr[] = $m;
                    break;
                }
            }
        }
        
        return tableJson(getArray($outArr, \BasePeer::TYPE_FIELDNAME), sizeof($matpels), array('mata_pelajaran_id'));
    
    }
    
    public function cariMatpelComboSmp (Request $request, Application $app) {
        
        $query = $request->get('query');
    
        $c = new \Criteria();
        if ($query != '') {
            $c->add(MataPelajaranPeer::NAMA, "%$query%", \Criteria::LIKE);
        } else {
            $c->add(MataPelajaranPeer::NAMA, "%Agama%", \Criteria::NOT_LIKE);
        }
    
        $matpels = MataPelajaranPeer::doSelect($c);
    
        $c2 = new \Criteria();
        $c2->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, array(7,8), \Criteria::IN);
        $kurikulums = MataPelajaranKurikulumPeer::doSelect($c2);
    
        foreach ($matpels as $m) {
    
            foreach ($kurikulums as $k) {
                if ($m->getPrimaryKey() == $k->getMataPelajaranId()) {
                    $outArr[] = $m;
                    break;
                }
            }
        }
        return tableJson(getArray($outArr, \BasePeer::TYPE_FIELDNAME), sizeof($matpels), array('mata_pelajaran_id'));
    
    }
    
    public function search_wilayah (Request $request, Application $app) {
    
        $kode_wilayah = trim($request->get('kode_wilayah'));
    
        $c = new \Criteria();
        $c->add(MstWilayahPeer::KODE_WILAYAH, $kode_wilayah."%", \Criteria::LIKE);
        $obj_wilayah = MstWilayahPeer::doSelectOne($c);
    
        return json_encode($obj_wilayah->toArray(\BasePeer::TYPE_FIELDNAME));
    
    }
    
    public function cariWilayahCombo (Request $request, Application $app) {
    
        $query = $request->get('query');
        $c = new \Criteria();
        $c->add(MstWilayahPeer::NAMA, "%$query%", \Criteria::LIKE);
        $wilayahs = MstWilayahPeer::doSelect($c);
    
        //     return json_encode($obj_wilayah->toArray(\BasePeer::TYPE_FIELDNAME));
        return tableJson(getArray($wilayahs, \BasePeer::TYPE_FIELDNAME), sizeof($wilayahs), array('kode_wilayah'));
    
        //     $sql = "select kode_wilayah, nama, id_level_wilayah, mst_kode_wilayah from ref.mst_wilayah
        //         where nama like '%$query%'";
    
        //     $data = getDataBySql($sql);
        //     return tableJson($data, sizeof($data), array('kode_wilayah'));
    
    }
    
    public function gridWilayah (Request $request, Application $app) {
    
        $kodeWilayah = $request->get('kode_wilayah');
        $jenjang = $request->get('jenjang');
        $statusSekolah = $request->get('status_sekolah');
        
        if ($jenjang == 'sd') {
            $bentukPendidikan = 5;
        } else if ($jenjang == 'smp') {
            $bentukPendidikan = 6;
        }
    
        $kodeWilayah = $kodeWilayah ?: "000000";
    
        $c = new \Criteria();
        $c->add(MstWilayahPeer::MST_KODE_WILAYAH, "$kodeWilayah%", \Criteria::LIKE);
        $c->addAscendingOrderByColumn(MstWilayahPeer::KODE_WILAYAH);
        
        $wilayahs = MstWilayahPeer::doSelect($c);
    
        if (sizeof($wilayahs) == 0) {
    
            $c2 = new \Criteria();
            $c2->add(SekolahPeer::KODE_WILAYAH, "$kodeWilayah%", \Criteria::LIKE);
            $c2->add(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikan);
            $c2->add(SekolahPeer::STATUS_SEKOLAH, $statusSekolah);
    
            $sekolahs = SekolahPeer::doSelect($c2);
    
            foreach ($sekolahs as $s) {
                $arr['kode_wilayah'] = $s->getSekolahId();
                $arr['nama'] = $s->getNama();
                $arr['mst_kode_wilayah'] =  $s->getKodeWilayah();
                $arr['id_level_wilayah'] =  4;
                $arrOut[] = $arr;
            }
            return tableJson($arrOut, sizeof($arrOut), array('kode_wilayah'));
    
        } else {
    
            return tableJson(getArray($wilayahs, \BasePeer::TYPE_FIELDNAME), sizeof($wilayahs), array('kode_wilayah'));
    
        }
    }
    
    public function cariSekolahCombo (Request $request, Application $app) {
    
        $query = $request->get('query') ?: false;
        $kodeWilayah = $request->get('kode_wilayah') ?: false;
        $start = $request->get('start') ?: 0;
        $limit = $request->get('limit') ?: 10;
    
        $c = new \Criteria();
        $query = str_replace(" ", "%", $query);
    
        $c->add(SekolahPeer::NAMA, "%$query%", \Criteria::LIKE);
    
        if ($kodeWilayah) {
    
            if (substr($kodeWilayah, 4, 2) == '00'){
                $kodeWilayah = substr($kodeWilayah, 0, 4);
            } else if (substr($kodeWilayah, 2, 2) == '00'){
                $kodeWilayah = substr($kodeWilayah, 0, 2);
            }
            $c->add(SekolahPeer::KODE_WILAYAH, "$kodeWilayah%", \Criteria::LIKE);
        }
    
        $count = SekolahPeer::doCount($c);
    
        $c->setLimit($limit);
        $c->setOffset($start);
    
        $sekolahs = SekolahPeer::doSelect($c);
    
        return tableJson(getArray($sekolahs, \BasePeer::TYPE_FIELDNAME), sizeof($sekolahs), array('sekolah_id'));
    
    }
    
    
}
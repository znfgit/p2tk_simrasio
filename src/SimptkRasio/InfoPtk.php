<?php

/**
 * InfoPTK Class
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\GuruPermatpelSdPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;

class InfoPtk {
    
    function get(Request $request, Application $app) {
		
		$which = $request->get('which') ? $request->get('which') : '137481893002';

		chdir(realpath(__DIR__."/../../scrap/"));
    
        $os = getOs();
        
        if ($os == "Windows") {
            $cmd = 'set_path.bat && casperjs read.js '.$which.'';
        } else {
            $cmd ='casperjs read.js '.$which.'';
        }
        //return $cmd;
        //execute($cmd, null, $out, $out, 3000);
		$out = shell_exec($cmd);
		
		$out = str_replace('href="', 'href="http://223.27.144.195:8081/', $out);
		$out = str_replace('src="', 'src="http://223.27.144.195:8081/', $out);
		$out = str_replace('"data/', 'http://223.27.144.195:8081/data/', $out);		
        return $out;
        
    }
    
}

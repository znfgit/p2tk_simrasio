<?php

/**
 * DataSmpSwasta Class
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

use SimptkRasio\Model;
use SimptkRasio\Model\SekolahQuery;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\MataPelajaran;
use SimptkRasio\Model\MataPelajaranPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\MataPelajaranKurikulumPeer;
use SimptkRasio\Model\KebutuhanGuruSmpSwastaPeer;
use SimptkRasio\Model\MataPelajaranKurikulum;
use SimptkRasio\Model\PtkTersediaPeer;
use SimptkRasio\Model\RefLinierBidstudiMatpelPeer;
use SimptkRasio\Model\RefLinierisasiPeer;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\NominasiPindahGuruSmpPeer;
use SimptkRasio\Model\NominasiPindahGuruSmp;
use SimptkRasio\Model\RombelSekolahSmpPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;
use SimptkRasio\Model\KebutuhanGuruBkPeer;

class DataSmpSwasta {

    public function kebutuhanGuruNasional (Request $request, Application $app) {

        $outJson = DataSmpSwasta::getKebutuhanGuru($request, 'wilayah', $app, true);
        return $outJson;

    }

    public function kebutuhanGuruPerWilayah (Request $request, Application $app) {

        $outJson = DataSmpSwasta::getKebutuhanGuru($request, 'wilayah', $app);
        return $outJson;

    }

    public function kebutuhanGuruPerMatpel (Request $request, Application $app) {

        $outJson = DataSmpSwasta::getKebutuhanGuru($request, 'matpel', $app);
        return $outJson;

    }
    
    public function getKebutuhanGuru($request, $per, $app, $is_nasional=false, $format='json', $skipLimits=false) {
    
        // 	Assumptions:
        // 	= Semua dihitung dengan asumsi jam kapasitas guru mengajar di sekolah induk 24 jam, tidak bisa simulasi
        //	= Kebutuhan rombel dihitung dari jumlah rombel aktual, tidak bisa simulasi
        //	= Jumlah siswa per rombel aktual, tidak bisa simulasi
    
        //  Parameters:
        // 	= kode_wilayah: dari dropdown filter / drilldown
        // 	= mata_pelajaran_id: dari dropdown filter
    
        $kodeWilayah = $request->get('kode_wilayah');
        $start = $request->get('start');
        $limit = $request->get('limit');
        $matpelId = $request->get('mata_pelajaran_id');
        $satuan = $request->get('satuan');
    
        // 	Cleaning and default value for debugging purposes
        $start = $start ?: 0;
        $limit = $limit ?: 12;
        //$limit = 12;
        //$kodeWilayah = $kodeWilayah ?: '000000';
        //$kodeWilayah = $kodeWilayah ?: '026017';
        if ($is_nasional) {
            $kodeWilayah = '000000 ';
        } else if (!$kodeWilayah) {
            //$kodeWilayah = $app['session']->get('kode_wilayah') ?: '000000 ';
            $kodeWilayah = $kodeWilayah ?: '000000';
        } else if (strtoupper($kodeWilayah) == 'NULL') {
            $kodeWilayah = '000000';
        }
    
        $matpelId = $matpelId ?: '4020';
        if (strtoupper($matpelId) == 'NULL') {
            $matpelId = '4020';
        }
    
        // Debug
        //echo "$per $matpelId $kodeWilayah $start $limit $satuanJam"; die;
    
        // Satuan angka KG, jam atau count guru
        $satuanJam = ($satuan == 'jam') ? true : false;
        $perWilayah = ($per == 'wilayah') ? true : false;
    
        // Cari object wilayah dulu
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah);
        $objWil = MstWilayahPeer::doSelectOne($cwil);
    
        if (!is_object($objWil)) {
            die ("$kodeWilayah not found");
        }
    
        // Cek Levelnya, kalau2 yang dicari sekolah
        $levelWil = $objWil->getIdLevelWilayah();
    
        // Yang dicari sekolah, gak usah digroup
        if ($levelWil == '5') {
    
            $c1 = new \Criteria();
            $c1->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH, $kodeWilayah);
            $kg = KebutuhanGuruSmpSwastaPeer::doSelect($c1);
    
            foreach ($kg as $k) {
                $arr = $k->toArray(BasePeer::TYPE_FIELDNAME);
                // 			$arrKg['kode_wilayah'] = $k->getSekolahId();
                //$arr['nama'] = $k->getNamaSekolah();
                $arrKg[] = $arr;
            }
    
            if ($format == 'json') {
                return tableJson($arrKg , sizeof($arrKg), array('kebutuhan_guru_smp_id'));
            } else if ($format == 'array') {
                return $arrKg;
            }
    
        // Yang dicari wilayah, group sesuai dengan tingkat masing2
        } else {
    
            switch ($levelWil) {
                case '0':
                    $groupNasional=1;
                    break;
                case '1':
                    $groupPropinsi=1;
                    break;
                case '2':
                    $groupKabkota=1;
                    break;
                case '3':
                    $groupKecamatan=1;
                    break;
                default:
                    $noGroup = 1;
                    break;
            }
    
            // 	Start query
            $c = new \Criteria();
            $c->clearSelectColumns();
    
            // 	Set divider / multiplier for calculation by jam/ptk
            $jamDivider = $satuanJam ? " " : " / 24 ";
            $jamMultiplier = $satuanJam ? " * 24 " : " ";
    
            // 	Filter by matpel if list per wilayah
            if ($perWilayah)
            {
                $c->add(KebutuhanGuruSmpSwastaPeer::MATA_PELAJARAN_ID, $matpelId);
            }
            // 	Group by matpel, set rinci matpel if list per matpel
            else
            {
                $c->addAsColumn('kode_rincian', KebutuhanGuruSmpSwastaPeer::MATA_PELAJARAN_ID);
                $c->addAsColumn('nama_rincian', KebutuhanGuruSmpSwastaPeer::NAMA_MATPEL);
                 
                $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::MATA_PELAJARAN_ID);
                $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_MATPEL);
                $c->addDescendingOrderByColumn(KebutuhanGuruSmpSwastaPeer::MATA_PELAJARAN_ID);
            }
             
            // 	Filter, Detil Column addition and Grouping by Level Wilayah
            if ($groupNasional) {
                 
                $c->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_PROPINSI, '000000 ', \Criteria::NOT_LIKE);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_PROPINSI);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSmpSwastaPeer::NAMA_PROPINSI);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_PROPINSI);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_PROPINSI);
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_PROPINSI);
                }
            }
    
            if ($groupPropinsi) {
                 
                //Where
                $c->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_PROPINSI, $kodeWilayah);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_KABKOTA);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSmpSwastaPeer::NAMA_KABKOTA);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_KABKOTA);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_KABKOTA);
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_KABKOTA);
                }
            }
    
            if ($groupKabkota) {
                 
                //Where
                $c->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH_KABKOTA, $kodeWilayah);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSmpSwastaPeer::NAMA_KECAMATAN);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_KECAMATAN);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH);
                    $c->addGroupByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_KECAMATAN);
                }
            }
    
            if ($groupKecamatan) {
                 
                //Where
                $c->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH, $kodeWilayah);
                 
                //Columns
                if ($perWilayah) {
                    $c->addAsColumn('kode_rincian', KebutuhanGuruSmpSwastaPeer::SEKOLAH_ID);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruSmpSwastaPeer::NAMA_SEKOLAH);
                    $c->addAscendingOrderByColumn(KebutuhanGuruSmpSwastaPeer::NAMA_SEKOLAH);
                }
    
            }
    
            // 	Add data for grouping
            if ($groupNasional || $groupPropinsi || $groupKabkota) {

                $c->addAsColumn('jumlah_kebutuhan', 'sum('.KebutuhanGuruSmpSwastaPeer::TOTAL_JAM_DIBUTUHKAN.') '. $jamDivider);
                $c->addAsColumn('jumlah_ptk_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_ADA_UTK_MATPEL.') '.$jamMultiplier);

                $c->addAsColumn('jumlah_ptk_gty_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gty_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);

//                 $c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL.') '.$jamMultiplier);
//                 $c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL.') '.$jamMultiplier);

                $c->addAsColumn('jumlah_ptk_gty_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gty_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);

//                 $c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
//                 $c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel_sert', 'sum('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL_SERT.') '.$jamMultiplier);
                
            }
    
            if ($groupKecamatan) {
                 
                $c->addAsColumn('jumlah_kebutuhan', '('.KebutuhanGuruSmpSwastaPeer::TOTAL_JAM_DIBUTUHKAN.')'. $jamDivider);
                $c->addAsColumn('jumlah_ptk_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_ADA_UTK_MATPEL.')'.$jamMultiplier);

                $c->addAsColumn('jumlah_ptk_gty_s1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gty_nons1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_nons1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_s1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_nons1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_s1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);
                
//                 $c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL.')'.$jamMultiplier);
//                 $c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL.')'.$jamMultiplier);

                $c->addAsColumn('jumlah_ptk_gty_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gty_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTY_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_PNS_DPK_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_ptk_gtt_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_GTT_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);

//                 $c->addAsColumn('jumlah_ptk_sertifikasi_nons1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_NONS1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
//                 $c->addAsColumn('jumlah_ptk_sertifikasi_s1_ada_utk_matpel_sert', '('.KebutuhanGuruSmpSwastaPeer::JUMLAH_PTK_SERTIFIKASI_S1_ADA_UTK_MATPEL_SERT.')'.$jamMultiplier);
                                
            }
    
            // 	Count all data first
            $count = KebutuhanGuruSmpSwastaPeer::doCount($c);
    
            

            //  Calculate all first. For remoteSummary support
            $stmtSummary = KebutuhanGuruSmpSwastaPeer::doSelectStmt($c);
            $resSummary = $stmtSummary->fetchAll(\PDO::FETCH_ASSOC);
            
            $arr = array();
            $arr['jumlah_kebutuhan'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_gty_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_gty_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_gtt_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_gtt_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_gty_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_gty_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_gtt_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_gtt_s1_ada_utk_matpel_sert'] = 0;
            
            foreach ($resSummary as $r) {
                 
                /* Formasi */
                $arr['jumlah_kebutuhan'] += $r['jumlah_kebutuhan'];
                $arr['jumlah_ptk_ada_utk_matpel'] += 
                    $r['jumlah_ptk_gty_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gty_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gtt_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gtt_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
                
                $arr['jumlah_ptk_gty_s1_ada_utk_matpel'] += $r['jumlah_ptk_gty_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_gty_nons1_ada_utk_matpel'] += $r['jumlah_ptk_gty_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] += $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] += $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_gtt_nons1_ada_utk_matpel'] += $r['jumlah_ptk_gtt_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_gtt_s1_ada_utk_matpel'] += $r['jumlah_ptk_gtt_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
                
                /* Sertifikasi */
                $arr['jumlah_ptk_ada_utk_matpel_sert'] +=
                    $r['jumlah_ptk_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gty_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gty_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gtt_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gtt_s1_ada_utk_matpel_sert'];
                    
                $arr['jumlah_ptk_gty_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_gty_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_gty_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_gty_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_gtt_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_gtt_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_gtt_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_gtt_s1_ada_utk_matpel_sert'];
            }
            
            $summaryArr = $arr;
            
            
            
            
            
            // 	Set limit for paging
    
            if (!$skipLimits) {
                $c->setLimit($limit);
                $c->setOffset($start);
            }
    
            //print_r($c); die;
            $stmt = KebutuhanGuruSmpSwastaPeer::doSelectStmt($c);
    
    
            $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            $out = "";
    
            //print_r($res); die;
            //$out .= "nama_rincian | jumlah_kebutuhan | jumlah_ptk_ada_utk_matpel | jumlah_ptk_gty_s1_ada_utk_matpel | jumlah_ptk_pns_dpk_nons1_ada_utk_matpel | <br>\n";
    
            if (!$perWilayah) {
                
                foreach ($res as $r) {
                    $arr = $r;
                    $arr['jumlah_kebutuhan'] = 100;
    		 		$arr['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_gty_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_gty_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_gty_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_gty_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_gtt_s1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_gtt_s1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
    		 		$arr['jumlah_ptk_gtt_nons1_ada_utk_matpel'] = 100 * $r['jumlah_ptk_gtt_nons1_ada_utk_matpel'] / $r['jumlah_kebutuhan'];
                        
                    $outArr[] = $arr;
                    //$arr = $r->toArray(BasePeer::TYPE_FIELDNAME);
                    //$out .= $arr['nama_rincian']." | ".$arr['jumlah_kebutuhan']." | ".$arr['jumlah_ptk_ada_utk_matpel']." | ".$arr['jumlah_ptk_gty_s1_ada_utk_matpel']." | ".$arr['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel']." | ". "<br>\n";
                    //$out .= $r->getNamaSekolah()." | ". $r->getNamaMatpel() . " | ". $r->getKekuranganGuru() . " | ". $r->getKelebihanGuru(). "<br>\n";
                }
                $res = $outArr;
            }
            //return $out;
    
            // Ringkas nama rincian            
            foreach ($res as $r) {
                $r["nama_rincian"] = str_ireplace("Muatan Lokal", "Mulok", $r["nama_rincian"]);
                $r["nama_rincian"] = str_ireplace("Bahasa", "Bhs.", $r["nama_rincian"]);
                //echo $r["nama_rincian"]."<br>\r\n";
                $r['jumlah_ptk_ada_utk_matpel'] =
                    $r['jumlah_ptk_gty_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gty_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gtt_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_gtt_s1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] +
                    $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
                
                $r['jumlah_ptk_ada_utk_matpel_sert'] =
                    $r['jumlah_ptk_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gty_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gty_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_dpk_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_pns_dpk_s1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gtt_nons1_ada_utk_matpel_sert'] +
                    $r['jumlah_ptk_gtt_s1_ada_utk_matpel_sert'];
                
                $outArrz[] = $r;
            }
            
            $res = $outArrz;
            
            if ($format == 'json') {
                return $this->tableToJson($res, $count, $summaryArr, array('kebutuhan_guru_smp_id'), $start, $limit);
    		} else if ($format == 'array') {
    		    return $res;
    		}

    	}
    }
    
    public function perhitunganKgSmp (Request $request, Application $app) {    
    
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
    
        $mps = MataPelajaranPeer::doSelect(new \Criteria());
        foreach ($mps as $m) {
            $arrMp[$m->getPrimaryKey()] = $m->getNama();
        }
    
        $cmpk = new \Criteria();
        $cmpk->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, 8);
        $mpks = MataPelajaranKurikulumPeer::doSelect($cmpk);
    
        foreach ($mpks as $m) {
    
            $tingkatId = $m->getTingkatPendidikanId();
            $mpJenjang = "mp".$tingkatId;
    
            $arrMpk[$m->getMataPelajaranId()]['perhitungan_rasio_id'] = 0;
            $arrMpk[$m->getMataPelajaranId()]['bentuk_pendidikan_id'] = 6;
            $arrMpk[$m->getMataPelajaranId()]['mata_pelajaran_id'] = $m->getMataPelajaranId();
            $arrMpk[$m->getMataPelajaranId()]['nama'] = $arrMp[$m->getMataPelajaranId()];
            $arrMpk[$m->getMataPelajaranId()][$mpJenjang] = $m->getJumlahJamMaksimum();
        }
    
        // Membaca Data Rombel
        // Di sini criteria diatur, kalau searchnya per sekolah, keluar yg mana, kalo per wilayah bagaimana.
        // Bisa lah insya Alloh.
        if ($levelWilayah <= 3) {
    
            $crbl = new \Criteria();
            $crbl->clearSelectColumns();
    
            //$crbl->addAsColumn('kode_wilayah', RombelSekolahSmpPeer::KODE_WILAYAH);
    
            $crbl->addAsColumn('jml_rbl_ktsp_7', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_7.')');
            $crbl->addAsColumn('jml_rbl_ktsp_8', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_8.')');
            $crbl->addAsColumn('jml_rbl_ktsp_9', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_9.')');
    
            $crbl->addAsColumn('jml_rbl_2013_7', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_7.')');
            $crbl->addAsColumn('jml_rbl_2013_8', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_8.')');
            $crbl->addAsColumn('jml_rbl_2013_9', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_9.')');
    
            $crbl->addAsColumn('jml_siswarbl_ktsp_7', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_7.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_8', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_8.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_9', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_9.')');
    
            $crbl->addAsColumn('jml_siswarbl_2013_7', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_7.')');
            $crbl->addAsColumn('jml_siswarbl_2013_8', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_8.')');
            $crbl->addAsColumn('jml_siswarbl_2013_9', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_9.')');
    
            $crbl->add(RombelSekolahSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addGroupByColumn(RombelSekolahSmpPeer::KODE_WILAYAH);
            //$crbl->add(RombelSekolahSmpPeer::SEMESTER_ID, $tahun.$smt);
    
            $stmt = RombelSekolahSmpPeer::doSelectStmt($crbl);
            //print_r($stmt);
    
            try {
                $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                //print_r($res);
            } catch(\Exception $e) {
                die ($e->getMessages());
            }
            $r = $res[0];
    
            if ($rsg == 1) {
    
                $arr['k7'] = $r['jml_rbl_ktsp_7'] + $r['jml_rbl_2013_7'];
                $arr['k8'] = $r['jml_rbl_ktsp_8'] + $r['jml_rbl_2013_8'];
                $arr['k9'] = $r['jml_rbl_ktsp_9'] + $r['jml_rbl_2013_9'];
    
            } else {
    
                $arr['k7'] = ($r['jml_siswarbl_ktsp_7'] + $r['jml_siswarbl_2013_7'])/$rsg;
                $arr['k8'] = ($r['jml_siswarbl_ktsp_8'] + $r['jml_siswarbl_2013_8'])/$rsg;
                $arr['k9'] = ($r['jml_siswarbl_ktsp_9'] + $r['jml_siswarbl_2013_9'])/$rsg;
    
                // 			$arr['k1'] = $r['jml_siswarbl_ktsp_1']/$rsg;
                // 			$arr['k2'] = $r['jml_siswarbl_ktsp_2']/$rsg;
                // 			$arr['k3'] = $r['jml_siswarbl_ktsp_3']/$rsg;
                // 			$arr['k4'] = $r['jml_siswarbl_ktsp_4']/$rsg;
                // 			$arr['k5'] = $r['jml_siswarbl_ktsp_5']/$rsg;
                // 			$arr['k6'] = $r['jml_siswarbl_ktsp_6']/$rsg;
    
            }
    
        } else {
    
            $crbl = new \Criteria();
            $crbl->add(RombelSekolahSmpPeer::SEKOLAH_ID, $kodeWilayah);
    
            $r = RombelSekolahSmpPeer::doSelectOne($crbl);
    
            if ($rsg == 1) {
    
                $arr['k7'] = $r->getJmlRblKtsp7() + $r->getJmlRbl20137();
                $arr['k8'] = $r->getJmlRblKtsp8() + $r->getJmlRbl20138();
                $arr['k9'] = $r->getJmlRblKtsp9() + $r->getJmlRbl20139();
    
            } else {
    
                $arr['k7'] = ($r->getJmlSiswarbl20137() + $r->getJmlSiswarblKtsp7())/$rsg;
                $arr['k8'] = ($r->getJmlSiswarbl20138() + $r->getJmlSiswarblKtsp8())/$rsg;
                $arr['k9'] = ($r->getJmlSiswarbl20139() + $r->getJmlSiswarblKtsp9())/$rsg;
    
            }
        }
        
        $arrRombel = $arr;
    
        // Membaca data Guru Tersedia (GT)
        // Dari PTK Tersedia
        // Digroup per wilayah atau dihitung per sekolah
    
        if ($levelWilayah <= 3) {
    
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSmpPeer::PTK_ID.')');
            $cgt->add(GuruPermatpelSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            
            // Filter PNS
            if ($filterPns) {
                $cgt->add(GuruPermatpelSmpPeer::IS_PNS, 1, \Criteria::EQUAL);
            }
            // Pensiun
            if ($filterPensiun) {
                $cgt->add(GuruPermatpelSmpPeer::TGL_LAHIR, strval(date("Y")+1-60)."-01-01", \Criteria::GREATER_EQUAL);
            }
            
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
    
            $stmt = GuruPermatpelSmpPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            //print_r($gts); die;
    
            foreach ($gts as $g) {
                if ($arrMpk[ $g['mata_pelajaran_id'] ]) {
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_org'] = $g['jumlah_gt'];
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_jam'] = $g['jumlah_gt'] * $bm;
                }
            }
    
        } else {
    
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSmpPeer::PTK_ID.')');
            $cgt->add(GuruPermatpelSmpPeer::SEKOLAH_ID, $kodeWilayah, \Criteria::LIKE);
            // Filter PNS
            if ($filterPns) {
                $cgt->add(GuruPermatpelSmpPeer::IS_PNS, 1, \Criteria::EQUAL);
            }
            // Pensiun
            if ($filterPensiun) {
                $cgt->add(GuruPermatpelSmpPeer::TGL_LAHIR, strval(date("Y")+1-60)."-01-01", \Criteria::GREATER_EQUAL);
            }
            
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
    
            $stmt = GuruPermatpelSmpPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            //print_r($gts); die;
    
            foreach ($gts as $g) {
                if ($arrMpk[ $g['mata_pelajaran_id'] ]) {
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_org'] = $g['jumlah_gt'];
                    $arrMpk[ $g['mata_pelajaran_id'] ]['gt_jam'] = $g['jumlah_gt'] * $bm;
                }
            }
    
        }
    
    
        $z = 1;
    
        foreach ($arrMpk as $a) {
    
            $jtm = 0;
    
            for ($i = 7; $i <= 9; $i++) {
                $a["k$i"] = $arrRombel["k$i"];
                $jr += $arrRombel["k$i"];
                $jtm += $arrRombel["k$i"] * $a["mp$i"];
            }
    
            $a["jr"] = $jr;
            $a["kg_jam"] = $jtm;
            $a["kg_org"] = $jtm/$bm;
    
            $a["lebih_jam"] = ($a["gt_jam"] > $a["kg_jam"]) ? $a["gt_jam"] - $a["kg_jam"] : 0 ;
            $a["kurang_jam"] = ($a["kg_jam"] > $a["gt_jam"]) ? $a["kg_jam"] - $a["gt_jam"] : 0;
    
            $a["lebih_org"] = ($a["gt_org"] > $a["kg_org"]) ? $a["gt_org"] - $a["kg_org"] : 0 ;
            $a["kurang_org"] = ($a["kg_org"] > $a["gt_org"]) ? $a["kg_org"] - $a["gt_org"] : 0;
    
    
            //$a["perhitungan_rasio_id"] = $a["mata_pelajaran_id"];
            $a["sekolah_id"] = $sekolahId;
            $a["tahun_ajaran_id"] = $tahun;
            $a["rasio_gk"] = 0;
            $a["jumlah_siswa"] = 0;
            $a["kg"] = 0;
    
            $jr = 0;
    
            if (!stripos($a['nama'], "Agama")) {
                $a["perhitungan_rasio_id"] = $z++;
                $arrOut[] = $a;
            }
    
        }
    
        return tableJson($arrOut, sizeof($arrOut), array('perhitungan_rasio_id'));
    
    
    }
    
    
    public function perhitunganKgSmpWilayah (Request $request, Application $app) {
    
        return $this->hitungKgSmpWilayah($request, $app, 'json');
    
    }
    
    public function hitungKgSmpWilayah (Request $request, Application $app, $format) {
    
        $matpelId = $request->get('mata_pelajaran_id');
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
    
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::MST_KODE_WILAYAH, $kodeWilayah);
        $wilayahs = MstWilayahPeer::doSelect($cwil);
    
        // Buat Array Nama Wilayah
        foreach($wilayahs as $w) {
            $arrNamaWilayah[$w->getKodeWilayah()]['nama'] =  $w->getNama();
            $arrNamaWilayah[$w->getKodeWilayah()]['kode_wilayah'] =  $w->getKodeWilayah();
            $arrNamaWilayah[$w->getKodeWilayah()]['mst_kode_wilayah'] =  $w->getMstKodeWilayah();
        }
    
        // Listing kurikulum to get hours per matpel
        $cmpk = new \Criteria();
        
        //// WOY WOY SETTING KURIKULUM YANG DIPAKE NGITUNG DI SINI WOY
        $cmpk->add(MataPelajaranKurikulumPeer::KURIKULUM_ID, 7);
        $mpks = MataPelajaranKurikulumPeer::doSelect($cmpk);
    
        foreach ($mpks as $m) {
            $tingkatId = $m->getTingkatPendidikanId();
            $mpJenjang = "mp".$tingkatId;
            $arrMpk[$m->getMataPelajaranId()][$mpJenjang] = $m->getJumlahJamMaksimum();
        }
    
        // Grouping berdasar level wilayah
        // Updated to correct each
        $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
    
        if ($levelWilayah < 3) {
        // if (false) {
    
            $crbl = new \Criteria();
            $crbl->clearSelectColumns();
    
            $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
            $crbl->addAsColumn('kode_wilayah_sub', 'left('.RombelSekolahSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
    
            $crbl->addAsColumn('jml_rbl_ktsp_7', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_7.')');
            $crbl->addAsColumn('jml_rbl_ktsp_8', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_8.')');
            $crbl->addAsColumn('jml_rbl_ktsp_9', 'sum('.RombelSekolahSmpPeer::JML_RBL_KTSP_9.')');
            
            $crbl->addAsColumn('jml_rbl_2013_7', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_7.')');
            $crbl->addAsColumn('jml_rbl_2013_8', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_8.')');
            $crbl->addAsColumn('jml_rbl_2013_9', 'sum('.RombelSekolahSmpPeer::JML_RBL_2013_9.')');
            
            $crbl->addAsColumn('jml_siswarbl_ktsp_7', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_7.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_8', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_8.')');
            $crbl->addAsColumn('jml_siswarbl_ktsp_9', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_KTSP_9.')');
            
            $crbl->addAsColumn('jml_siswarbl_2013_7', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_7.')');
            $crbl->addAsColumn('jml_siswarbl_2013_8', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_8.')');
            $crbl->addAsColumn('jml_siswarbl_2013_9', 'sum('.RombelSekolahSmpPeer::JML_SISWARBL_2013_9.')');
            // Compare kode wilayah with the school's kode wilayah.
            // Filtering done by cleaning code wilayah. Misal: Jabar: 020000 --> 02.
            // Sehingga semua sekolah dengan kepala 02 bisa kena. Jadi ga usah grouping terlalu repot
    
            $crbl->add(RombelSekolahSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addGroupByColumn(RombelSekolahSmpPeer::KODE_WILAYAH);
            //$crbl->add(RombelSekolahSmpPeer::SEMESTER_ID, $tahun.$smt);
    
            // Bedanya dengan yang di atas, di sini ada grouping
            $crbl->addGroupByColumn('left('.RombelSekolahSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
    
            // Ordering nya juga jangan lupa biar rapi datanya
            $crbl->addAscendingOrderByColumn('left('.RombelSekolahSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
    
            $stmt = RombelSekolahSmpPeer::doSelectStmt($crbl);
            //print_r($stmt);
    
            try {
                $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                //print_r($res);
            } catch(\Exception $e) {
                die ($e->getMessages());
            }
    
            // Sehingga res nya tidak cuman satu
            //$r = $res[0];
    
            // Tapi banyak. Kita loop
            //print_r($res);
    
            foreach ($res as $r) {
    
                // Identitas dulu
                $kode_wilayah = str_pad($r['kode_wilayah_sub'], 6, "0")." ";
                $arr['nama'] = $arrNamaWilayah[$kode_wilayah]['nama'];
                $arr['kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['mst_kode_wilayah'];
    
    
                // Utk RSG = 1, jumlah rombel dihitung dari keadaan rombel sesuai data
                if ($rsg == 1) {
                
                    $arr['k7'] = $r['jml_rbl_ktsp_7'] + $r['jml_rbl_2013_7'];
                    $arr['k8'] = $r['jml_rbl_ktsp_8'] + $r['jml_rbl_2013_8'];
                    $arr['k9'] = $r['jml_rbl_ktsp_9'] + $r['jml_rbl_2013_9'];
                
                } else {
                
                    $arr['k7'] = ($r['jml_siswarbl_ktsp_7'] + $r['jml_siswarbl_2013_7'])/$rsg;
                    $arr['k8'] = ($r['jml_siswarbl_ktsp_8'] + $r['jml_siswarbl_2013_8'])/$rsg;
                    $arr['k9'] = ($r['jml_siswarbl_ktsp_9'] + $r['jml_siswarbl_2013_9'])/$rsg;
                    
                }
    
                // Kita buat list associative dengan index kode wilayah
                $arrRombel[$kode_wilayah] = $arr;
            }
    
        } // end if ($levelWilayah < 3)
    
        else
    
        {
            $crbl = new \Criteria();
            // $crbl->add(RombelSekolahSmpPeer::KODE_WILAYAH, $kodeWilayah);
            $crbl->add(RombelSekolahSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            //$crbl->addAsColumn('kode_wilayah_sub', 'left('.RombelSekolahSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
    
            $res = RombelSekolahSmpPeer::doSelect($crbl);
    
            foreach ($res as $r) {
    
                // Identitas dulu
                $kode_wilayah = $r->getSekolahId();
                $arr['nama'] = $r->getNama();
                $arr['kode_wilayah'] = $r->getSekolahId();
                $arr['mst_kode_wilayah'] = $r->getKodeWilayah();
    
                if ($rsg == 1) {
                
                    $arr['k7'] = $r->getJmlRblKtsp7() + $r->getJmlRbl20137();
                    $arr['k8'] = $r->getJmlRblKtsp8() + $r->getJmlRbl20138();
                    $arr['k9'] = $r->getJmlRblKtsp9() + $r->getJmlRbl20139();
                
                } else {
                
                    $arr['k7'] = ($r->getJmlSiswarbl20137() + $r->getJmlSiswarblKtsp7())/$rsg;
                    $arr['k8'] = ($r->getJmlSiswarbl20138() + $r->getJmlSiswarblKtsp8())/$rsg;
                    $arr['k9'] = ($r->getJmlSiswarbl20139() + $r->getJmlSiswarblKtsp9())/$rsg;
                
                }
                $arrRombel[$kode_wilayah] = $arr;
    
            }
    
        }
        
        //echo sizeof($arrRombel); die;
        //print_r($arrRombel); die;
        
        
        // Membaca data Guru Tersedia (GT)
        // Dari PTK Tersedia
        // Digroup per wilayah atau dihitung per sekolah
    
    
        // Group per wilayah
        if ($levelWilayah < 3) {
        // if (false) {
    
            $cgt = new \Criteria();
            $cgt->clearSelectColumns();
    
            // The identity columns
            $wilayahChildDigitCount = ($levelWilayah + 1) * 2;
            $cgt->addAsColumn('kode_wilayah_sub', 'left('.GuruPermatpelSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSmpPeer::PTK_ID.')');
    
            // Where
    
            // Per Matpel brur
            $cgt->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $matpelId);
    
            // Parent
            $cgt->add(GuruPermatpelSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
    
            // Grouping
            $cgt->addGroupByColumn('left('.GuruPermatpelSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
    
            // Ordering
            $cgt->addAscendingOrderByColumn('left('.GuruPermatpelSmpPeer::KODE_WILAYAH.", $wilayahChildDigitCount)");
    
            // RUN it
            $stmt = GuruPermatpelSmpPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            foreach ($gts as $r) {
    
                // Identitas dulu
                $arr = null;
                $kode_wilayah = str_pad($r['kode_wilayah_sub'], 6, "0")." ";
    
                $arr['nama'] = $arrNamaWilayah[$kode_wilayah]['nama'];
                $arr['kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $arrNamaWilayah[$kode_wilayah]['mst_kode_wilayah'];
                $arr['mata_pelajaran_id'] = $r['mata_pelajaran_id'];
                $arr['gt_org'] = $r['jumlah_gt'];
                $arr['gt_jam'] = $r['jumlah_gt'] * $bm;
    
                // Array Guru Tersedia di Wilayah
                $arrWil[$kode_wilayah] = $arr;
            }
    
        }
    
        // Hitung per sekolah
        else
        {
            $cgt = new \Criteria();
    
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSmpPeer::PTK_ID.')');
            $cgt->clearSelectColumns();
    
            $cgt->addAsColumn('nama', GuruPermatpelSmpPeer::NAMA_SEKOLAH);
            $cgt->addAsColumn('kode_wilayah', GuruPermatpelSmpPeer::SEKOLAH_ID);
            $cgt->addAsColumn('mst_kode_wilayah', GuruPermatpelSmpPeer::KODE_WILAYAH);
            $cgt->addAsColumn('mata_pelajaran_id', GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
            $cgt->addAsColumn('jumlah_gt', 'count('.GuruPermatpelSmpPeer::PTK_ID.')');
    
            //$cgt->add(GuruPermatpelSmpPeer::KODE_WILAYAH, $kodeWilayah);
            $cgt->add(GuruPermatpelSmpPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            $cgt->add(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID, $matpelId);
    
            // Grouping
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::NAMA_SEKOLAH);
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::SEKOLAH_ID);
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::KODE_WILAYAH);
            $cgt->addGroupByColumn(GuruPermatpelSmpPeer::MATA_PELAJARAN_ID);
    
            $stmt = GuruPermatpelSmpPeer::doSelectStmt($cgt);
            $gts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
            //echo "$kodeWilayah $matpelId"; die;
    
            //print_r($gts); die;
    
            foreach ($gts as $r) {
    
                // Identitas dulu
                $arr = null;
                $kode_wilayah = $r['kode_wilayah'];
                $arr['nama'] = $r['nama'];
                $arr['kode_wilayah'] = $r['kode_wilayah'];
                $arr['mst_kode_wilayah'] = $r['mst_kode_wilayah'];
                $arr['mata_pelajaran_id'] = $r['mata_pelajaran_id'];
                $arr['gt_org'] = $r['jumlah_gt'];
                $arr['gt_jam'] = $r['jumlah_gt'] * $bm;
    
                // Array Guru Tersedia di Wilayah
                $arrWil[$kode_wilayah] = $arr;
            }
        }
    
        //echo $arr['nama']."<br>";
        //print_r($arrMpk); die;
        //print_r($arrWil); die;
    
    
        $z = 1;
    
        foreach($arrWil as $kw => $a) {
    
            $jtm = 0;
    
    
            for ($i = 7; $i <= 9; $i++) {
                
                //echo "[$kw][k$i]: ".$arrRombel[$kw]["k$i"]."<br>";
                
                $a["k$i"] = $arrRombel[$kw]["k$i"];
    
                // Masukin nilai Jam per MP
                $matpelId = $a['mata_pelajaran_id'];
                $a["mp$i"] = $arrMpk[ $matpelId ]["mp$i"];
                $jr += $arrRombel[$kw]["k$i"];
                $jtm += $arrRombel[$kw]["k$i"] * $a["mp$i"];
            }
    
            $a["jr"] = $jr;
            $a["kg_jam"] = $jtm;
            $a["kg_org"] = $jtm/$bm;
    
            $a["lebih_jam"] = ($a["gt_jam"] > $a["kg_jam"]) ? $a["gt_jam"] - $a["kg_jam"] : 0 ;
            $a["kurang_jam"] = ($a["kg_jam"] > $a["gt_jam"]) ? $a["kg_jam"] - $a["gt_jam"] : 0;
    
            $a["lebih_org"] = ($a["gt_org"] > $a["kg_org"]) ? $a["gt_org"] - $a["kg_org"] : 0 ;
            $a["kurang_org"] = ($a["kg_org"] > $a["gt_org"]) ? $a["kg_org"] - $a["gt_org"] : 0;
    
            $a["perhitungan_rasio_id"] = $z++;
    
            if ($a['nama'] != '') {
                $arrOut[] = $a;
            }
    
        }
        //print_r($arrRombel);
        //print_r($arrOut);
        
        // Ok. now data listed in $arrOut consisted of sekolahs within the $kodeWilayah.
        // What to do now is group it into shit.
    
        if ($levelWilayah < 3) {
            
        
            // Lets get list of wilayah under $kodeWilayah
            $c = new \Criteria();
            $c->add(MstWilayahPeer::KODE_WILAYAH, Util::cleanKodeWilayah($kodeWilayah), \Criteria::LIKE);
            $c->add(MstWilayahPeer::ID_LEVEL_WILAYAH, $levelWilayah + 1);
            $wilayahs = MstWilayahPeer::doSelect($c);
            
            //print_r($wilayahs); die;
            
            
            foreach ($arrOut as $a) {
                
                //echo $a['nama']."<br>";
                 
                foreach ($wilayahs as $w) {
                    
                    //echo "= comparing ".substr($a['mst_kode_wilayah'], 0, $wilayahChildDigitCount)." with ". substr_replace(Util::cleanKodeWilayah($w->getKodeWilayah()), "", -1) ."<br>";
                    
                    //if ( substr($a['mst_kode_wilayah'], 0, $wilayahChildDigitCount) ==  trim(str_replace("%", "", Util::cleanKodeWilayah($w->getKodeWilayah()))) ) {
                    if ( substr($a['kode_wilayah'], 0, $wilayahChildDigitCount) ==  trim(str_replace("%", "", Util::cleanKodeWilayah($w->getKodeWilayah()))) ) {                        
                        //echo "&nbsp;&nbsp;&nbsp;&nbsp;FOUND";
                        $wilArr[$w->getKodeWilayah()]['nama'] = $w->getNama();
                        $wilArr[$w->getKodeWilayah()]['kode_wilayah'] = $w->getKodeWilayah();
                        $wilArr[$w->getKodeWilayah()]['lebih_jam'] += $a['lebih_jam'];
                        $wilArr[$w->getKodeWilayah()]['kurang_jam'] += $a['kurang_jam'];
                        $wilArr[$w->getKodeWilayah()]['lebih_org'] += $a['lebih_org'];
                        $wilArr[$w->getKodeWilayah()]['kurang_org'] += $a['kurang_org'];
                    }
                    
                }
                
            }
            
            //return sizeof($wilArr); die;
            $l = 1;
            
            foreach ($wilArr as $kode_wilayah => $arr) {
                $arr['perhitungan_rasio_id'] = $l++;
                $out[] = $arr;
            }
        
        } else {
            $out = $arrOut;
        }
    
        //return sizeof($out); die;
        //print_r($arrOut); die;
        //return tableJson($arrOut, sizeof($arrOut), array('perhitungan_rasio_id'));
        //$outStr = "";
        //foreach ($arrWil as $key => $val) {
        //   $outStr .= "$key: ". $val['k1']. " | " .$val['k2']. " | " .$val['k3']. " | " .$val['k4']. " | " .$val['k5']. " | " . $val['k6']. "<br>";
        //}
        //return $outStr;
        if ($format == 'json') {
            return tableJson($out, sizeof($out), array('perhitungan_rasio_id'));
        } else {
            return $out;
        }
        
        
        
    }

    public function printing (Request $request, Application $app) {

        $outJson = DataSmpSwasta::getKebutuhanGuru($request, 'wilayah', $app, true);
        return $outJson;

    }
    
    public function printingSimulasiPerWilayah(Request $request, Application $app) {
    
        $data  = $this->hitungKgSmpWilayah($request, $app, 'array');
        $matpelId = $request->get('mata_pelajaran_id');
        $per = $request->get('per');
        $namaWilayah = $request->get('nama_wilayah');
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
    
    
        $objWilayah = Util::getObjWilayah($kodeWilayah);
        $objMatpel = Util::getObjMatpel($matpelId);
    
        $subTitle = ($per == 'wilayah') ?
        ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
        ('Wilayah: '.$objWilayah->getNama());
    
        $outStr = Printing::page(TEMPLATEROOT."grid_simulasi.twig", array(
                "title" => "Simulasi KG SMP per Wilayah",
                "subtitle" => $subTitle,
                "data" => $data
        ));
        return $outStr;
    
    }
    
    public function excelSimulasiPerWilayah(Request $request, Application $app) {
    
        $data  = $this->hitungKgSmpWilayah($request, $app, 'array');
    
        $matpelId = $request->get('mata_pelajaran_id');
        $per = $request->get('per');
        $namaWilayah = $request->get('nama_wilayah');
        $kodeWilayah = $request->get('kode_wilayah');
        $levelWilayah = $request->get('level_wilayah');
        $sekolahId = $request->get('sekolah_id');
        $smt = $request->get('smt');
        $tahun = $request->get('tahun');
        $rsg = $request->get('rsg');
        $bm = $request->get('bm');
        $rsgbk = $request->get('rsgbk');
        $filterPns = $request->get('is_pns');
        $filterPensiun = $request->get('is_pensiun');
    
    
        $objWilayah = Util::getObjWilayah($kodeWilayah);
        $objMatpel = Util::getObjMatpel($matpelId);
    
        $title = "Simulasi Kelebihan/Kekurangan KG SMP Per Wilayah";
    
        $subTitle = ($per == 'wilayah') ?
        ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
        ('Wilayah: '.$objWilayah->getNama());
    
        $filename = "simulasikg-{$per}-".stripslashes(strtolower(underscoreCapitalize($objWilayah->getNama()))."-matpel-".strtolower(underscoreCapitalize($objMatpel->getNama()))).".xls";
    
        Excel::excelSimulasiPerWilayah(TEMPLATEROOT."grid_per_matpel.xlsx", array(
        "per" => $per,
        "skup" => $skup,
        "filename" => $filename,
        "title" => $title,
        "subtitle" => $subTitle,
        "wilayah" => $objWilayah->getNama(),
        "matpel" => $objMatpel->getNama(),
        "headers" => array("Nama", "+ (jam)", "+ (org)", "- (jam)", "- (org)"),
        "data" => $data
        ));
    
    }
    
    public function tableToJson($array, $rownum, $summaryArray, $id, $start="0", $limit="20") {

        $rows = sizeof($array) > 0 ? json_encode($array) : "[]";
        $summaryData = sizeof($summaryArray) > 0 ? json_encode($summaryArray) : "{}";
        $result = sprintf("{ 'results' : %s, 'id' : '%s', 'start': %s, 'limit': %s, 'rows' : %s, 'summaryData': %s }", $rownum, $id[0], $start, $limit, $rows, $summaryData);
        
        return $result;
    }
    
    
    public function kebutuhanGuruBk(Request $request, Application $app) {
        
        $outJson = DataSmpSwasta::getKebutuhanGuruBk($request, 'wilayah', $app);
        return $outJson;
        
    }
    
    public function getKebutuhanGuruBk($request, $per, $app, $is_nasional=false, $format='json', $skipLimits=false) {

        // 	Assumptions:
        // 	= Semua dihitung dengan asumsi jam kapasitas guru mengajar di sekolah induk 24 jam, tidak bisa simulasi
        //	= Kebutuhan rombel dihitung dari jumlah rombel aktual, tidak bisa simulasi
        //	= Jumlah siswa per rombel aktual, tidak bisa simulasi
        
        //  Parameters:
        // 	= kode_wilayah: dari dropdown filter / drilldown
        // 	= mata_pelajaran_id: dari dropdown filter
        
        $kodeWilayah = $request->get('kode_wilayah');
        $start = $request->get('start');
        $limit = $request->get('limit');
        $matpelId = $request->get('mata_pelajaran_id');
        $satuan = $request->get('satuan');
        
        // 	Cleaning and default value for debugging purposes
        $start = $start ?: 0;
        $limit = $limit ?: 12;
        //$limit = 12;
        //$kodeWilayah = $kodeWilayah ?: '000000';
        //$kodeWilayah = $kodeWilayah ?: '026017';
        
        if ($is_nasional) {
            $kodeWilayah = '000000 ';
        } else if (!$kodeWilayah) {
            //$kodeWilayah = $app['session']->get('kode_wilayah') ?: '000000 ';
            $kodeWilayah = $kodeWilayah ?: '000000';
        } else if (strtoupper($kodeWilayah) == 'NULL') {
            $kodeWilayah = '000000';
        }
        
        // Debug
        //echo "$per $matpelId $kodeWilayah $start $limit $satuanJam"; die;
        
        // Satuan angka KG, jam atau count guru
        $satuanJam = ($satuan == 'jam') ? true : false;
        $perWilayah = ($per == 'wilayah') ? true : false;
        
        // Cari object wilayah dulu
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah);
        $objWil = MstWilayahPeer::doSelectOne($cwil);
        
        if (!is_object($objWil)) {
            die ("$kodeWilayah not found");
        }
        
        // Cek Levelnya, kalau2 yang dicari sekolah
        $levelWil = $objWil->getIdLevelWilayah();
        
        // Yang dicari sekolah, gak usah digroup
        if ($levelWil == '5') {
        
            $c1 = new \Criteria();
            $c1->add(KebutuhanGuruSmpSwastaPeer::KODE_WILAYAH, $kodeWilayah);
            $kg = KebutuhanGuruSmpSwastaPeer::doSelect($c1);
        
            foreach ($kg as $k) {
                $arr = $k->toArray(BasePeer::TYPE_FIELDNAME);
                // 			$arrKg['kode_wilayah'] = $k->getSekolahId();
                //$arr['nama'] = $k->getNamaSekolah();
                $arrKg[] = $arr;
            }
        
            if ($format == 'json') {
                return tableJson($arrKg , sizeof($arrKg), array('kebutuhan_guru_smp_id'));
            } else if ($format == 'array') {
                return $arrKg;
            }
        
            // Yang dicari wilayah, group sesuai dengan tingkat masing2
        } else {
        
            switch ($levelWil) {
            	case '0':
            	    $groupNasional=1;
            	    break;
            	case '1':
            	    $groupPropinsi=1;
            	    break;
            	case '2':
            	    $groupKabkota=1;
            	    break;
            	case '3':
            	    $groupKecamatan=1;
            	    break;
            	default:
            	    $noGroup = 1;
            	    break;
            }
        
            // 	Start query
            $c = new \Criteria();
            $c->clearSelectColumns();
        
            // 	Set divider / multiplier for calculation by jam/ptk
            $jamDivider = "";
            $jamMultiplier = "";
             
            // 	Filter, Detil Column addition and Grouping by Level Wilayah
            if ($groupNasional) {
                 
                $c->add(KebutuhanGuruBkPeer::KODE_WILAYAH_PROPINSI, '000000 ', \Criteria::NOT_LIKE);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruBkPeer::KODE_WILAYAH_PROPINSI);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruBkPeer::NAMA_PROPINSI);
                    $c->addAscendingOrderByColumn(KebutuhanGuruBkPeer::KODE_WILAYAH_PROPINSI);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::KODE_WILAYAH_PROPINSI);
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::NAMA_PROPINSI);
                }
            }
        
            if ($groupPropinsi) {
                 
                //Where
                $c->add(KebutuhanGuruBkPeer::KODE_WILAYAH_PROPINSI, $kodeWilayah);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruBkPeer::KODE_WILAYAH_KABKOTA);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruBkPeer::NAMA_KABKOTA);
                    $c->addAscendingOrderByColumn(KebutuhanGuruBkPeer::NAMA_KABKOTA);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::KODE_WILAYAH_KABKOTA);
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::NAMA_KABKOTA);
                }
            }
        
            if ($groupKabkota) {
                 
                //Where
                $c->add(KebutuhanGuruBkPeer::KODE_WILAYAH_KABKOTA, $kodeWilayah);
                 
                if ($perWilayah) {
                    //Columns
                    $c->addAsColumn('kode_rincian', KebutuhanGuruBkPeer::KODE_WILAYAH);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruBkPeer::NAMA_KECAMATAN);
                    $c->addAscendingOrderByColumn(KebutuhanGuruBkPeer::NAMA_KECAMATAN);
                     
                    //GroupBy
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::KODE_WILAYAH);
                    $c->addGroupByColumn(KebutuhanGuruBkPeer::NAMA_KECAMATAN);
                }
            }
        
            if ($groupKecamatan) {
                 
                //Where
                $c->add(KebutuhanGuruBkPeer::KODE_WILAYAH, $kodeWilayah);
                 
                //Columns
                if ($perWilayah) {
                    $c->addAsColumn('kode_rincian', KebutuhanGuruBkPeer::SEKOLAH_ID);
                    $c->addAsColumn('nama_rincian', KebutuhanGuruBkPeer::NAMA_SEKOLAH);
                    $c->addAscendingOrderByColumn(KebutuhanGuruBkPeer::NAMA_SEKOLAH);
                }
        
            }
        
            // 	Add data for grouping
            if ($groupNasional || $groupPropinsi || $groupKabkota) {
        
                $c->addAsColumn('jml_siswarbl_total', 'sum('.KebutuhanGuruBkPeer::JML_SISWARBL_TOTAL.') '. $jamDivider);
                $c->addAsColumn('jumlah_gurubk_minimal', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_MINIMAL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_maksimal', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_MAKSIMAL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK.') '.$jamMultiplier);
                
                $c->addAsColumn('jumlah_gurubk_honorer_s1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_S1.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_honorer_nons1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_NONS1.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_nons1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_NONS1.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_s1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_S1.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_nons1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_NONS1.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_s1', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_S1.') '.$jamMultiplier);
        
                $c->addAsColumn('jumlah_gurubk_honorer_s1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_S1_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_honorer_nons1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_NONS1_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_nons1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_NONS1_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_s1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_S1_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_nons1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_NONS1_SERT.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_s1_sert', 'sum('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_S1_SERT.') '.$jamMultiplier);
                
                $c->addAsColumn('kelebihan_guru', 'sum('.KebutuhanGuruBkPeer::KELEBIHAN_GURU.') '.$jamMultiplier);
                $c->addAsColumn('kekurangan_guru', 'sum('.KebutuhanGuruBkPeer::KEKURANGAN_GURU.') '.$jamMultiplier);
                
            }
        
            if ($groupKecamatan) {
                 
                $c->addAsColumn('jml_siswarbl_total', '('.KebutuhanGuruBkPeer::JML_SISWARBL_TOTAL.')'. $jamDivider);
                $c->addAsColumn('jumlah_gurubk_minimal', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_MINIMAL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_maksimal', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_MAKSIMAL.') '.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK.') '.$jamMultiplier);
        
                $c->addAsColumn('jumlah_gurubk_honorer_s1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_S1.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_honorer_nons1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_NONS1.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_nons1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_NONS1.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_s1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_S1.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_nons1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_NONS1.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_s1', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_S1.')'.$jamMultiplier);
        
                $c->addAsColumn('jumlah_gurubk_honorer_s1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_S1_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_honorer_nons1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_HONORER_NONS1_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_nons1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_NONS1_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_pns_s1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_PNS_S1_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_nons1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_NONS1_SERT.')'.$jamMultiplier);
                $c->addAsColumn('jumlah_gurubk_nonpns_s1_sert', '('.KebutuhanGuruBkPeer::JUMLAH_GURUBK_NONPNS_S1_SERT.')'.$jamMultiplier);
        
                $c->addAsColumn('kelebihan_guru', '('.KebutuhanGuruBkPeer::KELEBIHAN_GURU.') '.$jamMultiplier);
                $c->addAsColumn('kekurangan_guru', '('.KebutuhanGuruBkPeer::KEKURANGAN_GURU.') '.$jamMultiplier);
                
            }
        
            // 	Count all data first
            $count = KebutuhanGuruBkPeer::doCount($c);
        
            //  Calculate all first. For remoteSummary support
            $stmtSummary = KebutuhanGuruBkPeer::doSelectStmt($c);
            $resSummary = $stmtSummary->fetchAll(\PDO::FETCH_ASSOC);
            
            $arr = array();
            $arr['jml_siswarbl_total'] = 0;
            $arr['jumlah_gurubk_minimal'] = 0;
            $arr['jumlah_gurubk_maksimal'] = 0;
            $arr['jumlah_gurubk'] = 0;
            $arr['jumlah_gurubk_honorer_s1'] = 0;
            $arr['jumlah_gurubk_honorer_nons1'] = 0;
            $arr['jumlah_gurubk_pns_nons1'] = 0;
            $arr['jumlah_gurubk_pns_s1'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1'] = 0;
            $arr['jumlah_gurubk_nonpns_s1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_nons1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_s1'] = 0;
            $arr['jumlah_gurubk_sert'] = 0;
            $arr['jumlah_gurubk_honorer_s1_sert'] = 0;
            $arr['jumlah_gurubk_honorer_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_s1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_s1_sert'] = 0;
            $arr['kelebihan_guru'] = 0;
            $arr['kekurangan_guru'] = 0;
            
            foreach ($resSummary as $r) {
                
                $arr['jml_siswarbl_total'] += $r['jml_siswarbl_total'];
                $arr['jumlah_gurubk_minimal'] += $r['jumlah_gurubk_minimal'];
                $arr['jumlah_gurubk_maksimal'] += $r['jumlah_gurubk_maksimal'];
                $arr['jumlah_gurubk'] += $r['jumlah_gurubk'];
                $arr['jumlah_gurubk_honorer_s1'] += $r['jumlah_gurubk_honorer_s1'];
                $arr['jumlah_gurubk_honorer_nons1'] += $r['jumlah_gurubk_honorer_nons1'];
                $arr['jumlah_gurubk_pns_nons1'] += $r['jumlah_gurubk_pns_nons1'];
                $arr['jumlah_gurubk_pns_s1'] += $r['jumlah_gurubk_pns_s1'];
                $arr['jumlah_gurubk_nonpns_nons1'] += $r['jumlah_gurubk_nonpns_nons1'];
                $arr['jumlah_gurubk_nonpns_s1'] += $r['jumlah_gurubk_nonpns_s1'];
                $arr['jumlah_gurubk_sertifikasi_nons1'] += $r['jumlah_gurubk_sertifikasi_nons1'];
                $arr['jumlah_gurubk_sertifikasi_s1'] += $r['jumlah_gurubk_sertifikasi_s1'];
        
                $arr['jumlah_gurubk_sert'] += $r['jumlah_gurubk_sert'];
                $arr['jumlah_gurubk_honorer_s1_sert'] += $r['jumlah_gurubk_honorer_s1_sert'];
                $arr['jumlah_gurubk_honorer_nons1_sert'] += $r['jumlah_gurubk_honorer_nons1_sert'];
                $arr['jumlah_gurubk_pns_nons1_sert'] += $r['jumlah_gurubk_pns_nons1_sert'];
                $arr['jumlah_gurubk_pns_s1_sert'] += $r['jumlah_gurubk_pns_s1_sert'];
                $arr['jumlah_gurubk_nonpns_nons1_sert'] += $r['jumlah_gurubk_nonpns_nons1_sert'];
                $arr['jumlah_gurubk_nonpns_s1_sert'] += $r['jumlah_gurubk_nonpns_s1_sert'];
                
            }
            $arr['kelebihan_guru'] = ($arr['jumlah_gurubk_maksimal'] < $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk'] - $arr['jumlah_gurubk_maksimal'] : 0;
            $arr['kekurangan_guru'] = ($arr['jumlah_gurubk_minimal'] > $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk_minimal'] - $arr['jumlah_gurubk'] : 0; 
            
            $summaryArr = $arr;
        
            // 	Set limit for paging
            if (!$skipLimits) {
                $c->setLimit($limit);
                $c->setOffset($start);
            }
        
            //print_r($c); die;
            $stmt = KebutuhanGuruBkPeer::doSelectStmt($c);
            $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($res as $arr) {
                $arr['kelebihan_guru'] = ($arr['jumlah_gurubk'] > $arr['jumlah_gurubk_maksimal'] ) ? ($arr['jumlah_gurubk'] - $arr['jumlah_gurubk_maksimal']) : 0;
                $arr['kekurangan_guru'] = ($arr['jumlah_gurubk_minimal'] > $arr['jumlah_gurubk']) ? ($arr['jumlah_gurubk_minimal'] - $arr['jumlah_gurubk']) : 0;
                $outArr[] = $arr;
            }
            $res = $outArr;        
            $out = "";
        
            //print_r($res); die;
            //$out .= "nama_rincian | jml_siswarbl_total | jumlah_gurubk | jumlah_gurubk_honorer_s1 | jumlah_gurubk_pns_nons1 | <br>\n";
        
            if ($format == 'json') {
                return $this->tableToJson($res, $count, $summaryArr, array('kebutuhan_guru_smp_id'), $start, $limit);
            } else if ($format == 'array') {
                return $res;
            }
        
        }
        
    }
}
<?php

/**
 * Util Module
* @author Abah
*
*/

namespace SimptkRasio;

use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\MataPelajaranPeer;
class Util {

    public function cleanKodeWilayah($kodeWilayah) {
        
        if (substr($kodeWilayah,0,2) == '00') {
            $kodeWilayah = "";
        } else if (substr($kodeWilayah, 2, 2) == '00'){
            $kodeWilayah = substr($kodeWilayah, 0, 2);
        } else 	if (substr($kodeWilayah, 4, 2) == '00'){
            $kodeWilayah = substr($kodeWilayah, 0, 4);
        }
    
        return $kodeWilayah."%";
    }
    
    public function getObjWilayah ($kodeWilayah) {
        
        $cwil = new \Criteria();
        $cwil->add(MstWilayahPeer::KODE_WILAYAH, $kodeWilayah);
        $objWil = MstWilayahPeer::doSelectOne($cwil);
        
        return $objWil;
    }
    
    public function getObjMatpel ($matpelId) {
            
        $cwil = new \Criteria();
        $cwil->add(MataPelajaranPeer::MATA_PELAJARAN_ID, $matpelId);
        $obj = MataPelajaranPeer::doSelectOne($cwil);
        return $obj;
        
    }
}
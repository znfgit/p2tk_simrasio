<?php

/**
 * Excel Module
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\PtkSdAdaPeer;
use SimptkRasio\Model\PtkSmpAdaPeer;
use SimptkRasio\Model\PtkTersediaPeer;


class Excel {

    public function data(Request $request, Application $app) {
        
        $per = $request->get('per');
        $purpose = $request->get('purpose');
        $skup = $request->get('skup');
        $jenjang = $request->get('jenjang');
        
        $kode_wilayah = $request->get('kode_wilayah');
        $kode_wilayah = (!$kode_wilayah || (strtoupper($kode_wilayah) == 'NULL')) ? '000000 ' : $kode_wilayah;
        
        $mata_pelajaran_id = $request->get('mata_pelajaran_id');
        $def_mata_pelajaran_id = ($jenjang != 'smp') ? '4020' : '';
        $mata_pelajaran_id = (!$mata_pelajaran_id || (strtoupper($mata_pelajaran_id) == 'NULL')) ? $def_mata_pelajaran_id : $mata_pelajaran_id;
        
        $objWilayah = Util::getObjWilayah($kode_wilayah);
        $objMatpel = Util::getObjMatpel($mata_pelajaran_id);
        
        if ($purpose != "bk") {
            if ($jenjang != 'smp') {
                $data = DataSd::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            } else {
                $data = DataSmp::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            }
        } else {
            $data = DataSmp::getKebutuhanGuruBk($request, $per, $app, false, 'array', true);
        }
        //echo "$request, $per, $app"; die;
        //print_r($app); die;
        $perStr = ($per == 'wilayah') ? 'Wilayah' : 'Mata Pelajaran';
        $purposeStr = ($purpose == 'formasi') ? 'Formasi' : 'Sertifikasi';
         
        $jenjangStr = ($jenjang == 'sd') ? 'SD' : 'SMP';

        // Get Summary
        if ($purpose != "bk") {

            $title = "Analisis Kebutuhan Guru $jenjang Per-$perStr ($purposeStr)";
            $subTitle = ($per == 'wilayah') ?
            ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
            ('Wilayah: '.$objWilayah->getNama());

            $arr = array();
            $arr['jumlah_kebutuhan'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] = 0;
            
            foreach ($data as $r) {
                $arr['jumlah_kebutuhan'] += $r['jumlah_kebutuhan'];
                $arr['jumlah_ptk_ada_utk_matpel'] += $r['jumlah_ptk_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
            
                $arr['jumlah_ptk_ada_utk_matpel_sert'] += $r['jumlah_ptk_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
            }
        } else {

            $title = "Analisis Kebutuhan Guru BK ";
            $subTitle = 'Wilayah: '.$objWilayah->getNama();

            $arr = array();
            $arr['jml_siswarbl_total'] = 0;
            $arr['jumlah_gurubk_minimal'] = 0;
            $arr['jumlah_gurubk_maksimal'] = 0;
            $arr['jumlah_gurubk'] = 0;
            $arr['jumlah_gurubk_honorer_s1'] = 0;
            $arr['jumlah_gurubk_honorer_nons1'] = 0;
            $arr['jumlah_gurubk_pns_nons1'] = 0;
            $arr['jumlah_gurubk_pns_s1'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1'] = 0;
            $arr['jumlah_gurubk_nonpns_s1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_nons1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_s1'] = 0;
            $arr['jumlah_gurubk_sert'] = 0;
            $arr['jumlah_gurubk_honorer_s1_sert'] = 0;
            $arr['jumlah_gurubk_honorer_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_s1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_s1_sert'] = 0;
            $arr['kelebihan_guru'] = 0;
            $arr['kekurangan_guru'] = 0;
            
            foreach ($data as $r) {
                
                $arr['jml_siswarbl_total'] += $r['jml_siswarbl_total'];
                $arr['jumlah_gurubk_minimal'] += $r['jumlah_gurubk_minimal'];
                $arr['jumlah_gurubk_maksimal'] += $r['jumlah_gurubk_maksimal'];
                $arr['jumlah_gurubk'] += $r['jumlah_gurubk'];
                $arr['jumlah_gurubk_honorer_s1'] += $r['jumlah_gurubk_honorer_s1'];
                $arr['jumlah_gurubk_honorer_nons1'] += $r['jumlah_gurubk_honorer_nons1'];
                $arr['jumlah_gurubk_pns_nons1'] += $r['jumlah_gurubk_pns_nons1'];
                $arr['jumlah_gurubk_pns_s1'] += $r['jumlah_gurubk_pns_s1'];
                $arr['jumlah_gurubk_nonpns_nons1'] += $r['jumlah_gurubk_nonpns_nons1'];
                $arr['jumlah_gurubk_nonpns_s1'] += $r['jumlah_gurubk_nonpns_s1'];
                $arr['jumlah_gurubk_sertifikasi_nons1'] += $r['jumlah_gurubk_sertifikasi_nons1'];
                $arr['jumlah_gurubk_sertifikasi_s1'] += $r['jumlah_gurubk_sertifikasi_s1'];
        
                $arr['jumlah_gurubk_sert'] += $r['jumlah_gurubk_sert'];
                $arr['jumlah_gurubk_honorer_s1_sert'] += $r['jumlah_gurubk_honorer_s1_sert'];
                $arr['jumlah_gurubk_honorer_nons1_sert'] += $r['jumlah_gurubk_honorer_nons1_sert'];
                $arr['jumlah_gurubk_pns_nons1_sert'] += $r['jumlah_gurubk_pns_nons1_sert'];
                $arr['jumlah_gurubk_pns_s1_sert'] += $r['jumlah_gurubk_pns_s1_sert'];
                $arr['jumlah_gurubk_nonpns_nons1_sert'] += $r['jumlah_gurubk_nonpns_nons1_sert'];
                $arr['jumlah_gurubk_nonpns_s1_sert'] += $r['jumlah_gurubk_nonpns_s1_sert'];
                
            }
            $arr['kelebihan_guru'] = ($arr['jumlah_gurubk_maksimal'] < $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk'] - $arr['jumlah_gurubk_maksimal'] : 0;
            $arr['kekurangan_guru'] = ($arr['jumlah_gurubk_minimal'] > $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk_minimal'] - $arr['jumlah_gurubk'] : 0;
        }

        $arr['nama_rincian'] = "Total {$objWilayah->getNama()}";
        
        $data[] = $arr;
        
        if ($purpose != "bk") {
            $filename = "kg-{$purpose}-{$per}-".strtolower(underscoreCapitalize($objWilayah->getNama()))."-matpel-".strtolower(underscoreCapitalize($objMatpel->getNama())).".xls"; 
            $this->getExcel(TEMPLATEROOT."grid_per_matpel.xlsx", array(
                "per" => $per,
                "purpose" => $purpose,
                "skup" => $skup,
                "filename" => $filename,
                "title" => $title,
                "subtitle" => $subTitle,
                "wilayah" => $objWilayah->getNama(),
                "matpel" => $objMatpel->getNama(),
                "headers" => $this->getHeaders($per, $purpose, $skup),
                "data" => $data
            ));

        } else {
            $filename = "kg-{$purpose}-{$per}-".strtolower(underscoreCapitalize($objWilayah->getNama()))."bk.xls"; 
            $this->getExcel(TEMPLATEROOT."grid_bk.xlsx", array(
                "per" => $per,
                "purpose" => $purpose,
                "skup" => $skup,
                "filename" => $filename,
                "title" => $title,
                "subtitle" => $subTitle,
                "wilayah" => $objWilayah->getNama(),
                "headers" => $this->getHeaders($per, $purpose, $skup),
                "data" => $data
            ));
        }

    }
    
    public function getExcel($sourceTpl, $data) {
    
        $file = $sourceTpl;
        $namaFile = $data["filename"];
        
        try {
            // I/O
            $reader = \PHPExcel_IOFactory::createReaderForFile($file);
            $excelFile = new \PHPExcel();
            $excelFile = $reader->load($file);
             
            $ds = $excelFile->getSheet(0);
            //$ds = $excelFile->getSheetByName("data");
            $ds->getCell("A1")->setValue($data["title"]);
            $ds->getCell("A2")->setValue($data["subtitle"]);
            
            $alphas = range("A", "Z");
            $i = 0;
            
            foreach ($data["headers"] as $h) {
                $ds->getCell($alphas[$i]."4")->setValue($h);
                $i++;
            }
            
            $styleArrayHeader = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                            ),
                    )
            );
            
            $styleArray = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'right' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'left' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_NONE,
                            ),
                    )
            );
            
            $styleArrayFooter = array(
                'alignment' => array(
                    'indent' => 1
                ),
                'font' => array(
                    'name' => 'Calibri',
                    'size' => '12'
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'argb' => '00000000',
                        )
                    ),
                    'bottom' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ),
                )
            );
                        
            if ($data["purpose"] == 'formasi') {
                $lastColumn = 'I'; 
            } else if ($data["purpose"] == 'sertifikasi') {
                $lastColumn = 'I';
            } else {
                $lastColumn = 'G';
            }

            foreach (range('A', $lastColumn) as $alphabet) {
                $ds->duplicateStyleArray($styleArrayHeader, $alphabet."4");
            }
            
            
            $row = 5;
            
            foreach ($data["data"] as $d)
            {
                // Fill in the cells
                if ($data["purpose"] == 'formasi') {
                    $ds->getCell("A".$row)->setValue($d["nama_rincian"]);
                    $ds->getCell("B".$row)->setValue($d["jumlah_kebutuhan"]);
                    $ds->getCell("C".$row)->setValue($d["jumlah_ptk_ada_utk_matpel"]);
                    $ds->getCell("D".$row)->setValue($d["jumlah_ptk_pns_s1_ada_utk_matpel"]);
                    $ds->getCell("E".$row)->setValue($d["jumlah_ptk_pns_nons1_ada_utk_matpel"]);
                    $ds->getCell("F".$row)->setValue($d["jumlah_ptk_honorer_s1_ada_utk_matpel"]);
                    $ds->getCell("G".$row)->setValue($d["jumlah_ptk_honorer_nons1_ada_utk_matpel"]);
                    $ds->getCell("H".$row)->setValue($d["jumlah_ptk_nonpns_s1_ada_utk_matpel"]);
                    $ds->getCell("I".$row)->setValue($d["jumlah_ptk_nonpns_nons1_ada_utk_matpel"]);
                } else if ($data["purpose"] == 'sertifikasi') {
                    $ds->getCell("A".$row)->setValue($d["nama_rincian"]);
                    $ds->getCell("B".$row)->setValue($d["jumlah_kebutuhan"]);
                    $ds->getCell("C".$row)->setValue($d["jumlah_ptk_ada_utk_matpel"]);
                    $ds->getCell("D".$row)->setValue($d["jumlah_ptk_pns_s1_ada_utk_matpel"]);
                    $ds->getCell("E".$row)->setValue($d["jumlah_ptk_pns_nons1_ada_utk_matpel"]);
                    $ds->getCell("F".$row)->setValue($d["jumlah_ptk_honorer_s1_ada_utk_matpel"]);
                    $ds->getCell("G".$row)->setValue($d["jumlah_ptk_honorer_nons1_ada_utk_matpel"]);
                    $ds->getCell("H".$row)->setValue($d["jumlah_ptk_nonpns_s1_ada_utk_matpel"]);
                    $ds->getCell("I".$row)->setValue($d["jumlah_ptk_nonpns_nons1_ada_utk_matpel"]);
                } else {
                    $ds->getCell("A".$row)->setValue($d["nama_rincian"]);
                    $ds->getCell("B".$row)->setValue($d["jml_siswarbl_total"]);
                    $ds->getCell("C".$row)->setValue($d["jumlah_gurubk_minimal"]);
                    $ds->getCell("D".$row)->setValue($d["jumlah_gurubk_maksimal"]);
                    $ds->getCell("E".$row)->setValue($d["jumlah_gurubk"]);

                    if ($d["kelebihan_guru"] == 0 && $d["kekurangan_guru"] == 0) {
                        $lebih = '(pas)';
                        $kurang = '(pas)';
                    } else {
                        $lebih = $d["kelebihan_guru"];
                        $kurang = $d["kekurangan_guru"];
                    }

                    $ds->getCell("F".$row)->setValue($lebih);
                    $ds->getCell("G".$row)->setValue($kurang);
                }
                //if ($data["purpose"] == 'formasi') $ds->getCell("E".$row)->setValue($d["jumlah_ptk_honorer_s1_ada_utk_matpel"]);
                
                // Add garis
                foreach (range('A', $lastColumn) as $alphabet) {
                    $ds->duplicateStyleArray($styleArray, $alphabet.$row);
                }
                
                // Add numbering format
                foreach (range('B', $lastColumn) as $alphabet) {
                    $ds->getStyle($alphabet.$row)->getNumberFormat()->setFormatCode('#,##');
                }
                
                $row++;
            }
            
            foreach (range('A', $lastColumn) as $alphabet) {
                $ds->duplicateStyleArray($styleArrayFooter, $alphabet.($row-1));
            }
            
            
            
        } catch (Exception $e){
        
        }
        
        $filename = $namaFile."-".addZeroes($id, 4).".xlsx";
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
        $objWriter->save('php://output'); die;
            
    }

    public function getHeaders($per, $purpose, $skup=1) {
    
        $isFormasi = ($purpose == "formasi");
        $isSertifikasi = ($purpose == "sertifikasi");
        $isBk = ($purpose == "bk");
    
        if ($isFormasi) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else {
                return array("Mata Pelajaran", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            }
        } else if ($isSertifikasi) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else {
                return array("Mata Pelajaran", "Keb. Guru", "Jml. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            }
        } else if ($isBk) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Jml. Siswa (SMP)", "Guru BK Minimal", "Guru BK Maksimal", "Guru BK Tersedia", "Lebih", "Kurang");
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Jml. Siswa (SMP)", "Guru BK Minimal", "Guru BK Maksimal", "Guru BK Tersedia", "Lebih", "Kurang");
            }
        }

    }
    
    
    public function excelSimulasiPerWilayah($sourceTpl, $data) {
        
        $file = $sourceTpl;
        $namaFile = $data["filename"];
    
        try {
            // I/O
            $reader = \PHPExcel_IOFactory::createReaderForFile($file);
            $excelFile = new \PHPExcel();
            $excelFile = $reader->load($file);
             
            $ds = $excelFile->getSheet(0);
            //$ds = $excelFile->getSheetByName("data");
            $ds->getCell("A1")->setValue($data["title"]);
            $ds->getCell("A2")->setValue($data["subtitle"]);
    
            $alphas = range("A", "Z");
            $i = 0;
    
            foreach ($data["headers"] as $h) {
                $ds->getCell($alphas[$i]."4")->setValue($h);
                $i++;
            }
    
            $styleArrayHeader = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                            ),
                    )
            );
    
            $styleArray = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'right' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'left' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_NONE,
                            ),
                    )
            );
    
    
            if ($data["purpose"] == 'formasi') {
                $lastColumn = 'E';
            } else {
                $lastColumn = 'D';
            }
            
            $lastColumn = 'E';
            
            foreach (range('A', $lastColumn) as $alphabet) {
                $ds->duplicateStyleArray($styleArrayHeader, $alphabet."4");
            }
    
            $row = 5;
    
            foreach ($data["data"] as $d)
            {
                // Fill in the cells
                $ds->getCell("A".$row)->setValue($d["nama"]);
                $ds->getCell("B".$row)->setValue($d["lebih_jam"]);
                $ds->getCell("C".$row)->setValue($d["lebih_org"]);
                $ds->getCell("D".$row)->setValue($d["kurang_jam"]);
                $ds->getCell("E".$row)->setValue($d["kurang_org"]);
    
                // Add garis
                foreach (range('A', $lastColumn) as $alphabet) {
                    $ds->duplicateStyleArray($styleArray, $alphabet.$row);
                }
    
                // Add numbering format
                foreach (range('B', $lastColumn) as $alphabet) {
                    $ds->getStyle($alphabet.$row)->getNumberFormat()->setFormatCode('#,##');
                }
    
                $row++;
            }
             
        } catch (Exception $e){
    
        }
    
        $filename = $namaFile."-".addZeroes($id, 4).".xlsx";
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
        $objWriter->save('php://output'); die;
    
    }

    public function dataGuru(Request $request, Application $app) {

        //$per = $request->get('per');
        //$purpose = $request->get('purpose');
        $skup = $request->get('skup');
        $levelWilayah = $skup;
        $jenjang = $request->get('jenjang');
        $statusSekolah = $request->get('status_sekolah');
        
        $kode_wilayah = $request->get('kode_wilayah');
        $kode_wilayah = (!$kode_wilayah || (strtoupper($kode_wilayah) == 'NULL')) ? '000000 ' : $kode_wilayah;
        
        $mata_pelajaran_id = $request->get('mata_pelajaran_id');
        $def_mata_pelajaran_id = ($jenjang != 'smp') ? '4020' : '';
        $mata_pelajaran_id = (!$mata_pelajaran_id || (strtoupper($mata_pelajaran_id) == 'NULL')) ? $def_mata_pelajaran_id : $mata_pelajaran_id;

        $mataPelajaranId = $mata_pelajaran_id;
                
        $bentukPendidikanId = ($jenjang == 'sd') ? 5 : 6; 
        
        $objWilayah = Util::getObjWilayah($kode_wilayah);
        $objMatpel = Util::getObjMatpel($mata_pelajaran_id);

        $limit = 0;
        $start = 0;
        
        $data = array(
            "filename" => "daftar_guru",
            "title" => "Daftar Guru",
            "subtitle" => 'Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()
        );
            
        $file = $sourceTpl;
        $namaFile = $data["filename"];

        $file = TEMPLATEROOT."daftar_guru.xlsx";
        
        try {
            $sig = getSignificantDigits($kode_wilayah);
            if (strlen($sig) % 2 == 0) {
                // if even, do nothin
            } else {
                // if odd, means the significant digits still holds a significant number. i.e 2.
                $sig .= "0";
            }
            
            $kodeWilayah = $sig."%";
            
            // Data
            if ($levelWilayah < 4) {
                
                if (!$guruBk) {

                    if ($bentukPendidikanId == 5) {

                        $c = new  \Criteria();
                        
                        $c->add(PtkSdAdaPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
                        $c->add(PtkSdAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                        $c->add(PtkSdAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                        $c->add(PtkSdAdaPeer::NAMA_SEKOLAH, "SD%", \Criteria::LIKE);
                        //$cton1 = $c->getNewCriterion(ArticlePeer::TITLE, '%FooBar%', Criteria::LIKE);
                        //$cton2 = $c->getNewCriterion(ArticlePeer::SUMMARY, '%FooBar%', Criteria::LIKE);
                        //$cton1->addOr($cton2);

                        //$cton1 $c->getCriterion
                        /*        
			(is_pns = 1 and is_s1 = 1)
			or (is_pns = 1 and is_s1 = 0)
			or (is_honorer = 1 and is_s1 = 1)
			or (is_honorer = 1 and is_s1 = 0)
			or (is_honorer_lain = 1 and is_s1 = 1)
			or (is_honorer_lain = 1 and is_s1 = 0)
                        */
                        
                        //$count = PtkSdAdaPeer::doCount($c);
                        //print_r($statusSekolah); die;
                        //print_r($count); die;
                        if ($limit) {
                            $c->setLimit($limit);
                        }
                        $c->setOffset($start);
                        $gurus = PtkSdAdaPeer::doSelect($c);
                        //print_r($gurus); die;
                        
                    } else if ($bentukPendidikanId == 6) {

                        $c = new  \Criteria();

                        $c->add(PtkSmpAdaPeer::KODE_WILAYAH, $kodeWilayah, \Criteria::LIKE);
                        $c->add(PtkSmpAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                        $c->add(PtkSmpAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                        
                        //$count = PtkSmpAdaPeer::doCount($c);
                        if ($limit) {
                            $c->setLimit($limit);
                        }
                        $c->setOffset($start);
                        $gurus = PtkSmpAdaPeer::doSelect($c);

                    }

                } else {
                    $c = new \Criteria();
                    $c->add(PtkTersediaPeer::KODE_WILAYAH, $sekolahId);
                    $c->add(PtkTersediaPeer::IS_GURU_BK, 1);
                    $gurus = PtkTersediaPeer::doSelect($c);
                }

            } else {

                $sekolah = SekolahPeer::retrieveByPK($sekolahId);

                if (!$guruBk) {

                    if ($sekolah->getBentukPendidikanId() == 5) {

                        $c = new  \Criteria();

                        $c->add(PtkSdAdaPeer::SEKOLAH_ID, $sekolahId);
                        $c->add(PtkSdAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                        $c->add(PtkSdAdaPeer::STATUS_SEKOLAH, $statusSekolah);                        
                        $gurus = PtkSdAdaPeer::doSelect($c);


                    } else if ($sekolah->getBentukPendidikanId() == 6) {

                        $c = new  \Criteria();

                        $c->add(PtkSmpAdaPeer::SEKOLAH_ID, $sekolahId);
                        $c->add(PtkSmpAdaPeer::MATA_PELAJARAN_ID, $mataPelajaranId);
                        $c->add(PtkSmpAdaPeer::STATUS_SEKOLAH, $statusSekolah);
                        $gurus = PtkSmpAdaPeer::doSelect($c);

                    }
                    
                } else {
                    $c = new \Criteria();
                    $c->add(PtkTersediaPeer::SEKOLAH_ID, $sekolahId);
                    $c->add(PtkTersediaPeer::IS_GURU_BK, 1);
                    $gurus = PtkTersediaPeer::doSelect($c);
                }

                $count = sizeof($gurus);

            }
            
            //print_r($gurus); die;
            
            
            // I/O
            $reader = \PHPExcel_IOFactory::createReaderForFile($file);
            $excelFile = new \PHPExcel();
            $excelFile = $reader->load($file);
             
            $ds = $excelFile->getSheet(0);
            //$ds = $excelFile->getSheetByName("data");
            $ds->getCell("A1")->setValue($data["title"]);
            $ds->getCell("A2")->setValue($data["subtitle"]);
    
            $alphas = range("A", "Z");
            $i = 0;

            
            $styleArrayHeader = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                            ),
                    )
            );
            
            $styleArray = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'right' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'left' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_NONE,
                            ),
                    )
            );
            
            $styleArrayFooter = array(
                'alignment' => array(
                    'indent' => 1
                ),
                'font' => array(
                    'name' => 'Calibri',
                    'size' => '12'
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'argb' => '00000000',
                        )
                    ),
                    'bottom' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ),
                )
            );

            $lastColumn = 'O';
                        
            foreach (range('A', $lastColumn) as $alphabet) {
                $ds->duplicateStyleArray($styleArrayHeader, $alphabet."4");
            }
    
            $row = 5;

            foreach ($gurus as $g)
            {
                
                $i++;
                
                // Fill in the cells
                //$g = new Model\PtkSdAda();
                
                $ds->getCell("A".$row)->setValue($i);
                $ds->getCell("B".$row)->setValue($g->getNamaPtk());
                $ds->getCell("C".$row)->setValueExplicit($g->getNuptk(), \PHPExcel_Cell_DataType::TYPE_STRING);
                $ds->getCell("D".$row)->setValueExplicit($g->getNrg(), \PHPExcel_Cell_DataType::TYPE_STRING);  
                $ds->getCell("E".$row)->setValue($g->getTglLahir());
                $ds->getCell("F".$row)->setValue($g->getNamaTugasTambahan());
                $ds->getCell("G".$row)->setValue($g->getIsSertifikasi() ? "Ya" : "Tidak");
                $ds->getCell("H".$row)->setValue($g->getIsS1() ? "Ya" : "Tidak");
                $ds->getCell("I".$row)->setValue($g->getIsPns() ? "Ya" : "Tidak");
                
                $yearNow = intval(date('Y'));
                $date = intval(date_parse($g->getTglPensiun()));
                $yearDate = $date["year"];
                $isPensiun = ($yearNow == $yearDate) ? "Ya" : "Tidak";

                $ds->getCell("J".$row)->setValue($isPensiun);
                $ds->getCell("K".$row)->setValue($g->getJjm());
                $ds->getCell("L".$row)->setValue($g->getNamaSekolah());
                $ds->getCell("M".$row)->setValue($g->getNamaKecamatan());
                $ds->getCell("N".$row)->setValue($g->getNamaKabkota());
                $ds->getCell("O".$row)->setValue($g->getNamaPropinsi());

                // Add garis
                foreach (range('A', $lastColumn) as $alphabet) {
                    $ds->duplicateStyleArray($styleArray, $alphabet.$row);
                }
    
                // Add numbering format
                // foreach (range('B', $lastColumn) as $alphabet) {
                //     $ds->getStyle($alphabet.$row)->getNumberFormat()->setFormatCode('#,##');
                // }
    
                $row++;
            }

        } catch (Exception $e){
    
        }
            
        $filename = $namaFile."-".addZeroes($id, 4).".xlsx";
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
        $objWriter->save('php://output'); die;
        
    }
    
    
    public function allData(Request $request, Application $app) {
        
        $per = $request->get('per');
        $purpose = $request->get('purpose');
        $skup = $request->get('skup');
        $jenjang = $request->get('jenjang');
        
        $kode_wilayah = $request->get('kode_wilayah');
        $kode_wilayah = (!$kode_wilayah || (strtoupper($kode_wilayah) == 'NULL')) ? '000000 ' : $kode_wilayah;
        
        $mata_pelajaran_id = $request->get('mata_pelajaran_id');
        $def_mata_pelajaran_id = ($jenjang != 'smp') ? '4020' : '';
        $mata_pelajaran_id = (!$mata_pelajaran_id || (strtoupper($mata_pelajaran_id) == 'NULL')) ? $def_mata_pelajaran_id : $mata_pelajaran_id;
        
        $objWilayah = Util::getObjWilayah($kode_wilayah);
        $objMatpel = Util::getObjMatpel($mata_pelajaran_id);
        
        if ($purpose != "bk") {
            if ($jenjang != 'smp') {
                $data = DataSd::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            } else {
                $data = DataSmp::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            }
        } else {
            $data = DataSmp::getKebutuhanGuruBk($request, $per, $app, false, 'array', true);
        }
        //echo "$request, $per, $app"; die;
        //print_r($app); die;
        $perStr = ($per == 'wilayah') ? 'Wilayah' : 'Mata Pelajaran';
        $purposeStr = ($purpose == 'formasi') ? 'Formasi' : 'Sertifikasi';
         
        $jenjangStr = ($jenjang == 'sd') ? 'SD' : 'SMP';

        // Get Summary
        if ($purpose != "bk") {

            $title = "Analisis Kebutuhan Guru $jenjang Per-$perStr ($purposeStr)";
            $subTitle = ($per == 'wilayah') ?
            ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
            ('Wilayah: '.$objWilayah->getNama());

            $arr = array();
            $arr['jumlah_kebutuhan'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] = 0;
            
            foreach ($data as $r) {
                $arr['jumlah_kebutuhan'] += $r['jumlah_kebutuhan'];
                $arr['jumlah_ptk_ada_utk_matpel'] += $r['jumlah_ptk_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
            
                $arr['jumlah_ptk_ada_utk_matpel_sert'] += $r['jumlah_ptk_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
            }
        } else {

            $title = "Analisis Kebutuhan Guru BK ";
            $subTitle = 'Wilayah: '.$objWilayah->getNama();

            $arr = array();
            $arr['jml_siswarbl_total'] = 0;
            $arr['jumlah_gurubk_minimal'] = 0;
            $arr['jumlah_gurubk_maksimal'] = 0;
            $arr['jumlah_gurubk'] = 0;
            $arr['jumlah_gurubk_honorer_s1'] = 0;
            $arr['jumlah_gurubk_honorer_nons1'] = 0;
            $arr['jumlah_gurubk_pns_nons1'] = 0;
            $arr['jumlah_gurubk_pns_s1'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1'] = 0;
            $arr['jumlah_gurubk_nonpns_s1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_nons1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_s1'] = 0;
            $arr['jumlah_gurubk_sert'] = 0;
            $arr['jumlah_gurubk_honorer_s1_sert'] = 0;
            $arr['jumlah_gurubk_honorer_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_s1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_s1_sert'] = 0;
            $arr['kelebihan_guru'] = 0;
            $arr['kekurangan_guru'] = 0;
            
            foreach ($data as $r) {
                
                $arr['jml_siswarbl_total'] += $r['jml_siswarbl_total'];
                $arr['jumlah_gurubk_minimal'] += $r['jumlah_gurubk_minimal'];
                $arr['jumlah_gurubk_maksimal'] += $r['jumlah_gurubk_maksimal'];
                $arr['jumlah_gurubk'] += $r['jumlah_gurubk'];
                $arr['jumlah_gurubk_honorer_s1'] += $r['jumlah_gurubk_honorer_s1'];
                $arr['jumlah_gurubk_honorer_nons1'] += $r['jumlah_gurubk_honorer_nons1'];
                $arr['jumlah_gurubk_pns_nons1'] += $r['jumlah_gurubk_pns_nons1'];
                $arr['jumlah_gurubk_pns_s1'] += $r['jumlah_gurubk_pns_s1'];
                $arr['jumlah_gurubk_nonpns_nons1'] += $r['jumlah_gurubk_nonpns_nons1'];
                $arr['jumlah_gurubk_nonpns_s1'] += $r['jumlah_gurubk_nonpns_s1'];
                $arr['jumlah_gurubk_sertifikasi_nons1'] += $r['jumlah_gurubk_sertifikasi_nons1'];
                $arr['jumlah_gurubk_sertifikasi_s1'] += $r['jumlah_gurubk_sertifikasi_s1'];
        
                $arr['jumlah_gurubk_sert'] += $r['jumlah_gurubk_sert'];
                $arr['jumlah_gurubk_honorer_s1_sert'] += $r['jumlah_gurubk_honorer_s1_sert'];
                $arr['jumlah_gurubk_honorer_nons1_sert'] += $r['jumlah_gurubk_honorer_nons1_sert'];
                $arr['jumlah_gurubk_pns_nons1_sert'] += $r['jumlah_gurubk_pns_nons1_sert'];
                $arr['jumlah_gurubk_pns_s1_sert'] += $r['jumlah_gurubk_pns_s1_sert'];
                $arr['jumlah_gurubk_nonpns_nons1_sert'] += $r['jumlah_gurubk_nonpns_nons1_sert'];
                $arr['jumlah_gurubk_nonpns_s1_sert'] += $r['jumlah_gurubk_nonpns_s1_sert'];
                
            }
            $arr['kelebihan_guru'] = ($arr['jumlah_gurubk_maksimal'] < $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk'] - $arr['jumlah_gurubk_maksimal'] : 0;
            $arr['kekurangan_guru'] = ($arr['jumlah_gurubk_minimal'] > $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk_minimal'] - $arr['jumlah_gurubk'] : 0;
        }

        $arr['nama_rincian'] = "Total {$objWilayah->getNama()}";
        
        $data[] = $arr;
        
        if ($purpose != "bk") {
            $filename = "kg-{$purpose}-{$per}-".strtolower(underscoreCapitalize($objWilayah->getNama()))."-matpel-".strtolower(underscoreCapitalize($objMatpel->getNama())).".xls"; 
            $this->getExcel(TEMPLATEROOT."grid_per_matpel.xlsx", array(
                "per" => $per,
                "purpose" => $purpose,
                "skup" => $skup,
                "filename" => $filename,
                "title" => $title,
                "subtitle" => $subTitle,
                "wilayah" => $objWilayah->getNama(),
                "matpel" => $objMatpel->getNama(),
                "headers" => $this->getHeaders($per, $purpose, $skup),
                "data" => $data
            ));

        } else {
            $filename = "kg-{$purpose}-{$per}-".strtolower(underscoreCapitalize($objWilayah->getNama()))."bk.xls"; 
            $this->getExcel(TEMPLATEROOT."grid_bk.xlsx", array(
                "per" => $per,
                "purpose" => $purpose,
                "skup" => $skup,
                "filename" => $filename,
                "title" => $title,
                "subtitle" => $subTitle,
                "wilayah" => $objWilayah->getNama(),
                "headers" => $this->getHeaders($per, $purpose, $skup),
                "data" => $data
            ));
        }

    }

    public function allDataGuru(Request $request, Application $app) {

        //$per = $request->get('per');
        //$purpose = $request->get('purpose');
        $kode_wilayah = '000000 ';

        $limit = 0;
        $start = 0;
    
        $namaFile = "all_daftar_guru";
        $file = TEMPLATEROOT."all_daftar_guru.xlsx";        
        
        try {
          
            $c = new \Criteria();
            $c->addAscendingOrderByColumn(PtkSdAdaPeer::KODE_WILAYAH);
            $c->addAscendingOrderByColumn(PtkSdAdaPeer::NAMA_SEKOLAH);
            $c->addAscendingOrderByColumn(PtkSdAdaPeer::MATA_PELAJARAN_ID);            
            $gurus = PtkSdAdaPeer::doSelect($c);
            
            // I/O
            $reader = \PHPExcel_IOFactory::createReaderForFile($file);
            $excelFile = new \PHPExcel();
            $excelFile = $reader->load($file);
             
            $ds = $excelFile->getSheet(0);
            //$ds = $excelFile->getSheetByName("data");
            $ds->getCell("A1")->setValue($data["title"]);
            $ds->getCell("A2")->setValue($data["subtitle"]);
    
            $alphas = range("A", "Z");
            $i = 0;
            
            $styleArrayHeader = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'top' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                            ),
                    )
            );
            
            $styleArray = array(
                    'alignment' => array(
                            'indent' => 1
                    ),
                    'font' => array(
                            'name' => 'Calibri',
                            'size' => '12'
                    ),
                    'borders' => array(
                            'right' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'left' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                                    'color' => array(
                                            'argb' => '00000000',
                                    )
                            ),
                            'bottom' => array(
                                    'style' => \PHPExcel_Style_Border::BORDER_NONE,
                            ),
                    )
            );
            
            $styleArrayFooter = array(
                'alignment' => array(
                    'indent' => 1
                ),
                'font' => array(
                    'name' => 'Calibri',
                    'size' => '12'
                ),
                'borders' => array(
                    'top' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array(
                            'argb' => '00000000',
                        )
                    ),
                    'bottom' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ),
                )
            );

            $lastColumn = 'O';
                        
            foreach (range('A', $lastColumn) as $alphabet) {
                $ds->duplicateStyleArray($styleArrayHeader, $alphabet."4");
            }
    
            $row = 5;

            foreach ($gurus as $g)
            {
                
                $i++;
                
                // Fill in the cells
                $g = new Model\PtkSdAda();
                
                $ds->getCell("A".$row)->setValue($i);
                $ds->getCell("B".$row)->setValue($g->getNamaPtk());
                $ds->getCell("C".$row)->setValueExplicit($g->getNuptk(), \PHPExcel_Cell_DataType::TYPE_STRING);
                $ds->getCell("D".$row)->setValueExplicit($g->getNrg(), \PHPExcel_Cell_DataType::TYPE_STRING);  
                $ds->getCell("E".$row)->setValue($g->getTglLahir());
                $ds->getCell("F".$row)->setValue($g->getNamaTugasTambahan());
                $ds->getCell("G".$row)->setValue($g->getNamaBidStudiIjazahTerakhir());
                $ds->getCell("H".$row)->setValue($g->getIsSertifikasi() ? "Ya" : "Tidak");
                $ds->getCell("I".$row)->setValue($g->getIsS1() ? "Ya" : "Tidak");
                $ds->getCell("J".$row)->setValue($g->getIsPns() ? "Ya" : "Tidak");
                
                $yearNow = intval(date('Y'));
                $date = intval(date_parse($g->getTglPensiun()));
                $yearDate = $date["year"];
                $isPensiun = ($yearNow == $yearDate) ? "Ya" : "Tidak";

                $ds->getCell("K".$row)->setValue($isPensiun);
                $ds->getCell("L".$row)->setValue($g->getJjm());
                $ds->getCell("M".$row)->setValue($g->getNamaMatpel());
                $ds->getCell("N".$row)->setValue($g->getNamaSekolah());
                $ds->getCell("O".$row)->setValue($g->getNamaKecamatan());
                $ds->getCell("P".$row)->setValue($g->getNamaKabkota());
                $ds->getCell("Q".$row)->setValue($g->getNamaPropinsi());

                // Add garis
                foreach (range('A', $lastColumn) as $alphabet) {
                    $ds->duplicateStyleArray($styleArray, $alphabet.$row);
                }
    
                // Add numbering format
                // foreach (range('B', $lastColumn) as $alphabet) {
                //     $ds->getStyle($alphabet.$row)->getNumberFormat()->setFormatCode('#,##');
                // }
    
                $row++;
            }
            
            $filename = $namaFile.".xlsx";

            $objWriter = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel2007');
            //$objWriter->save('php://output'); die;
            $objWriter->save(ROOT."/output/".$filename); die;
        
        } catch (Exception $e){
            return $e->getMessage();
        }

    }

}


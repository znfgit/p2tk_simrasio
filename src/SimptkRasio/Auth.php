<?php

/**
 * Auth Module
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

use SimptkRasio\Model;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;

class Auth {
    
    public function login (Request $request, Application $app) {
        
        $username = ($request->get('username')) ? $request->get('username') : 0;
        $password = ($request->get('password')) ? $request->get('password') : 0;
        
        // 	$pass_enkrip = md5($password);
        
        $c = new \Criteria();
        $c->add(PenggunaPeer::USERNAME, $username);
        $logins = PenggunaPeer::doSelectOne($c);
        
        
        if (is_object($logins)) {
        
            if ($logins->getPassword() === $password) {
                	
                // echo $logins->getKodeWilayah(); die;
                	
                $cw = new \Criteria();
                $cw->add(MstWilayahPeer::KODE_WILAYAH, $logins->getKodeWilayah()."%",\Criteria::LIKE);
                $wilayahObj = MstWilayahPeer::doSelectOne($cw);
                	
                $app['session']->set('user', $username);
                $app['session']->set('kode_wilayah', $logins->getKodeWilayah());
                $app['session']->set('nama_wilayah', $wilayahObj->getNama());
                $app['session']->set('mst_kode_wilayah', $wilayahObj->getMstKodeWilayah());
                $app['session']->set('skup', $wilayahObj->getIdLevelWilayah());
                $app['session']->set('pengguna_id', $logins->getPrimaryKey());
        
                $kodewilayah = $app['session']->get('kode_wilayah');
                $namawilayah = $app['session']->get('nama_wilayah');
                $mstkodewilayah = $app['session']->get('mst_kode_wilayah');
                $skup = $app['session']->get('skup');
                	
                return "{ success:true, message: 'Login berhasil', username: '$username', kode_wilayah: '$kodewilayah', mst_kode_wilayah: '$mstkodewilayah', nama_wilayah: '$namawilayah', skup: '$skup' }";
        
        } else {
        
            return "{ success: false, message: 'Maaf, <b>Password</b> yg anda masukan salah' }";
        }
        
        } else {
        
            return "{ success: false, message: 'Maaf, <b>Username</b> yang anda masukan tidak terdaftar' }";
        }
        
    }
    
    public function cekLogin (Request $request, Application $app) {
    
        $user = $app['security']->getToken()->getUser();

        // $content = is_object($user) ? $user->getUsername() : 'ANONYMOUS';

        if ($app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {

            $username = $user->getUsername(); 
            
            $c = new \Criteria();
            $c->add(PenggunaPeer::USERNAME, $username);
            $logins = PenggunaPeer::doSelectOne($c);

            $cw = new \Criteria();
            $cw->add(MstWilayahPeer::KODE_WILAYAH, $logins->getKodeWilayah()."%",\Criteria::LIKE);
            $wilayahObj = MstWilayahPeer::doSelectOne($cw);
            
            
            $lastUpdated = processDate(getValueBySql("select last_updated from last_updated", \Propel::getDefaultDB()));
            
            $app['session']->set('user', $username);
            $app['session']->set('kode_wilayah', $logins->getKodeWilayah());
            $app['session']->set('nama_wilayah', $wilayahObj->getNama());
            $app['session']->set('mst_kode_wilayah', $wilayahObj->getMstKodeWilayah());
            $app['session']->set('skup', $wilayahObj->getIdLevelWilayah());
            $app['session']->set('pengguna_id', $logins->getPrimaryKey());
            $app['session']->set('last_updated', $lastUpdated);
            

            $username = $app['session']->get('user');
            $kodewilayah = $app['session']->get('kode_wilayah');
            $namawilayah = $app['session']->get('nama_wilayah');
            $mstkodewilayah = $app['session']->get('mst_kode_wilayah');
            $skup = $app['session']->get('skup');
        
            if ($username) {
                return "{ success:true, username: '$username', kode_wilayah: '$kodewilayah', mst_kode_wilayah: '$mstkodewilayah', nama_wilayah: '$namawilayah', skup: '$skup', last_updated: '$lastUpdated', message: 'Session exists for user $username' }";
            } else {
                return "{ success:false, message: 'No session' }";
            }

        }

    }
    
    public function logoutApp(Request $request, Application $app) {
        
        $app['session']->clear();
        
        $username = $app['session']->get('user');
        
        if ($username) {
            return "{ success:false, message: 'Logout gagal. Sesi masih ada.' }";
        } else {
            return "{ success:true, message: 'Logout berhasil. Tunggu sebentar..' }";
        }
        //return 'Session cleared. For proof, let me check app session. Username: '. ;
        //return $app->redirect('/');
        
    }
}

<?php

/**
 * Printing Module
 * @author Abah
 *
 */

namespace SimptkRasio;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;

class Printing {

    public function data(Request $request, Application $app) {
        
        $per = $request->get('per');
        $purpose = $request->get('purpose');
        $skup = $request->get('skup');
        $jenjang = $request->get('jenjang');
        
        $kode_wilayah = $request->get('kode_wilayah');
        $kode_wilayah = (!$kode_wilayah || (strtoupper($kode_wilayah) == 'NULL')) ? '000000 ' : $kode_wilayah;
        
        $mata_pelajaran_id = $request->get('mata_pelajaran_id');
        $def_mata_pelajaran_id = ($jenjang == 'sd') ? '4020' : '4200';
        $mata_pelajaran_id = (!$mata_pelajaran_id || (strtoupper($mata_pelajaran_id) == 'NULL')) ? $def_mata_pelajaran_id : $mata_pelajaran_id;
        
        $objWilayah = Util::getObjWilayah($kode_wilayah);
        $objMatpel = Util::getObjMatpel($mata_pelajaran_id);
        
        if ($purpose != "bk") {
            if ($jenjang != 'smp') {
                $data = DataSd::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            } else {
                $data = DataSmp::getKebutuhanGuru($request, $per, $app, false, 'array', true);
            }
        } else {
            $data = DataSmp::getKebutuhanGuruBk($request, $per, $app, false, 'array', true);
        }
        //echo "$request, $per, $app"; die;
        //print_r($app); die;
        $perStr = ($per == 'wilayah') ? 'Wilayah' : 'Mata Pelajaran'; 
        $purposeStr = ($purpose == 'formasi') ? 'Formasi' : 'Sertifikasi'; 
         
        $jenjangStr = ($jenjang == 'sd') ? 'SD' : 'SMP'; 
        
        if ($purpose != "bk") {
            $title = "Analisis Kebutuhan Guru ".strtoupper($jenjang)." Per-$perStr ($purposeStr)";
            $subTitle = ($per == 'wilayah') ? 
                ('Wilayah: '.$objWilayah->getNama()." , Mata Pelajaran: ". $objMatpel->getNama()) :
                ('Wilayah: '.$objWilayah->getNama());
        } else {
            $title = "Analisis Kebutuhan Guru BK ";
            $subTitle = 'Wilayah: '.$objWilayah->getNama();
        }

        // Get Summary
        if ($purpose != "bk") {
            
            $arr = array();
            $arr['jumlah_kebutuhan'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] = 0;
            $arr['jumlah_ptk_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] = 0;
            $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] = 0;
            
            foreach ($data as $r) {
                $arr['jumlah_kebutuhan'] += $r['jumlah_kebutuhan'];
                $arr['jumlah_ptk_ada_utk_matpel'] += $r['jumlah_ptk_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_nons1_ada_utk_matpel'];
                $arr['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'] += $r['jumlah_ptk_sertifikasi_s1_ada_utk_matpel'];
            
                $arr['jumlah_ptk_ada_utk_matpel_sert'] += $r['jumlah_ptk_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_honorer_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_pns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_pns_s1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_nons1_ada_utk_matpel_sert'];
                $arr['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'] += $r['jumlah_ptk_nonpns_s1_ada_utk_matpel_sert'];
            }
        } else {

            $arr = array();
            $arr['jml_siswarbl_total'] = 0;
            $arr['jumlah_gurubk_minimal'] = 0;
            $arr['jumlah_gurubk_maksimal'] = 0;
            $arr['jumlah_gurubk'] = 0;
            $arr['jumlah_gurubk_honorer_s1'] = 0;
            $arr['jumlah_gurubk_honorer_nons1'] = 0;
            $arr['jumlah_gurubk_pns_nons1'] = 0;
            $arr['jumlah_gurubk_pns_s1'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1'] = 0;
            $arr['jumlah_gurubk_nonpns_s1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_nons1'] = 0;
            $arr['jumlah_gurubk_sertifikasi_s1'] = 0;
            $arr['jumlah_gurubk_sert'] = 0;
            $arr['jumlah_gurubk_honorer_s1_sert'] = 0;
            $arr['jumlah_gurubk_honorer_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_pns_s1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_nons1_sert'] = 0;
            $arr['jumlah_gurubk_nonpns_s1_sert'] = 0;
            $arr['kelebihan_guru'] = 0;
            $arr['kekurangan_guru'] = 0;
            
            foreach ($data as $r) {
                
                $arr['jml_siswarbl_total'] += $r['jml_siswarbl_total'];
                $arr['jumlah_gurubk_minimal'] += $r['jumlah_gurubk_minimal'];
                $arr['jumlah_gurubk_maksimal'] += $r['jumlah_gurubk_maksimal'];
                $arr['jumlah_gurubk'] += $r['jumlah_gurubk'];
                $arr['jumlah_gurubk_honorer_s1'] += $r['jumlah_gurubk_honorer_s1'];
                $arr['jumlah_gurubk_honorer_nons1'] += $r['jumlah_gurubk_honorer_nons1'];
                $arr['jumlah_gurubk_pns_nons1'] += $r['jumlah_gurubk_pns_nons1'];
                $arr['jumlah_gurubk_pns_s1'] += $r['jumlah_gurubk_pns_s1'];
                $arr['jumlah_gurubk_nonpns_nons1'] += $r['jumlah_gurubk_nonpns_nons1'];
                $arr['jumlah_gurubk_nonpns_s1'] += $r['jumlah_gurubk_nonpns_s1'];
                $arr['jumlah_gurubk_sertifikasi_nons1'] += $r['jumlah_gurubk_sertifikasi_nons1'];
                $arr['jumlah_gurubk_sertifikasi_s1'] += $r['jumlah_gurubk_sertifikasi_s1'];
        
                $arr['jumlah_gurubk_sert'] += $r['jumlah_gurubk_sert'];
                $arr['jumlah_gurubk_honorer_s1_sert'] += $r['jumlah_gurubk_honorer_s1_sert'];
                $arr['jumlah_gurubk_honorer_nons1_sert'] += $r['jumlah_gurubk_honorer_nons1_sert'];
                $arr['jumlah_gurubk_pns_nons1_sert'] += $r['jumlah_gurubk_pns_nons1_sert'];
                $arr['jumlah_gurubk_pns_s1_sert'] += $r['jumlah_gurubk_pns_s1_sert'];
                $arr['jumlah_gurubk_nonpns_nons1_sert'] += $r['jumlah_gurubk_nonpns_nons1_sert'];
                $arr['jumlah_gurubk_nonpns_s1_sert'] += $r['jumlah_gurubk_nonpns_s1_sert'];
                
            }
            $arr['kelebihan_guru'] = ($arr['jumlah_gurubk_maksimal'] < $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk'] - $arr['jumlah_gurubk_maksimal'] : 0;
            $arr['kekurangan_guru'] = ($arr['jumlah_gurubk_minimal'] > $arr['jumlah_gurubk']) ? $arr['jumlah_gurubk_minimal'] - $arr['jumlah_gurubk'] : 0;
        }
        $arr['nama_rincian'] = "Total {$objWilayah->getNama()}";
        
        $data[] = $arr;
        //print_r($data); die;
        
        $outStr = $this->page(TEMPLATEROOT."grid_per_matpel.twig", array(
            "per" => $per,
            "purpose" => $purpose,
            "skup" => $skup,
            "title" => $title,
            "subtitle" => $subTitle,
            "wilayah" => $objWilayah->getNama(),
            "matpel" => $objMatpel->getNama(),
            "headers" => $this->getHeaders($per, $purpose, $skup),
            "data" => $data
        ));
        
        return $outStr;
    }
    
    public function page($sourceTpl, $data) {
    
        // Apply template
        $sourceTplDir = dirname($sourceTpl);
        $fileName = basename($sourceTpl);
        
        //$outStr = $sourceTplDir."<br>".$fileName;
        //return  $outStr;
        
        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader);
        
        $outStr = $twig->render($fileName, $data);
        
        return $outStr;
        
    }
    
    public function getHeaders($per, $purpose, $skup=1) {
        
        $isFormasi = ($purpose == "formasi");
        $isSertifikasi = ($purpose == "sertifikasi");
        $isBk = ($purpose == "bk");

        if ($isFormasi) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Keb. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");    
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Keb. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else {
                return array("Mata Pelajaran", "Keb. Guru", "PNS S1", "PNS Non S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            }
        } else if ($isSertifikasi) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Keb. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Keb. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            } else {
                return array("Mata Pelajaran", "Keb. Guru", "PNS S1", "PNS <S1", "Honor S1", "Hnr <S1", "GBPNS S1", "GBPNS <S1");
            }
        } else if ($isBk) {
            if ($per == 'wilayah' && $skup < 3) {
                return array("Wilayah", "Jml. Siswa (SMP)", "Guru BK Minimal", "Guru BK Maksimal", "Guru BK Tersedia", "Lebih", "Kurang");
            } else if ($per == 'wilayah' && $skup >= 3) {
                return array("Sekolah", "Jml. Siswa (SMP)", "Guru BK Minimal", "Guru BK Maksimal", "Guru BK Tersedia", "Lebih", "Kurang");
            }
        }
    } 

}
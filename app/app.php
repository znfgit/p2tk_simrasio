<?php 

use Symfony\Component\HttpFoundation\Request;
use SimptkRasio\Model\SekolahQuery;
use SimptkRasio\Model\SekolahPeer;
use SimptkRasio\Model\MataPelajaran;
use SimptkRasio\Model\MataPelajaranPeer;
use SimptkRasio\Model\MstWilayah;
use SimptkRasio\Model\MstWilayahPeer;
use SimptkRasio\Model\KebutuhanGuruSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulumPeer;
use SimptkRasio\Model\KebutuhanGuruSmpPeer;
use SimptkRasio\Model\RombelSekolahSdPeer;
use SimptkRasio\Model\MataPelajaranKurikulum;
use SimptkRasio\Model\RombelSekolahSd;
use SimptkRasio\Model\PtkTersediaPeer;
use SimptkRasio\Model\RefLinierBidstudiMatpelPeer;
use SimptkRasio\Model\RefLinierisasiPeer;
use SimptkRasio\Model\GuruPermatpelSdPeer;
use SimptkRasio\Model\PenggunaPeer;
use SimptkRasio\Model\NominasiPindahGuruSdPeer;
use SimptkRasio\Model\NominasiPindahGuruSd;
use SimptkRasio\Model\RombelSekolahSmpPeer;
use SimptkRasio\Model\GuruPermatpelSmpPeer;

$app = require_once __DIR__ . '/bootstrap.php';
$app['debug'] = true;

$app->get('/Test', function (Request $request) use ($app) {
    return 'test';
});

$namaAplikasi = $config["nama_aplikasi"];

$app->get('/passgen/{password}', function(Request $request) use ($app) {
    $password = $request->get('password');
    return "UUID: ". UUID::mint(1). " <br>Pass: ". $app['security.encoder.digest']->encodePassword($password, '');
});

$app->get('/multipassgen', function(Request $request) use ($app) {
    $penggunas = PenggunaPeer::doSelect(new Criteria());
    $retStr = "";
    foreach ($penggunas as $p) {
        $newPassword = $app['security.encoder.digest']->encodePassword($p->getYm(), '');
        $p->setPassword($newPassword);
        if ($p->getUsername() == 'abah' || $p->getUsername() == 'admin_simrasio') {
            continue;
        }
        $retStr .= "User: {$p->getNama()} <br>\r\n- Oldpass: {$p->getYm()}<br>\r\n- Newpass: $newPassword <br>\r\n";
        if ($p->save()) {
        //     $retStr .= "User: {$p->getNama()} Oldpass: {$p->getYm()}  Newpass: $newPassword <br>\r\n";
            $retStr .= "- Process: Success changing the password<br>\r\n";
        } else {
            $retStr .= "- Process: Fail to change password<br>\r\n";
        }
    }
    return $retStr;
});

// Root
$app->get('/', function (Request $request) use ($app) {
    
    global $namaAplikasi;
    
    return $app['twig']->render('index.html', array(
        'error' => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),
        'haha' => 'foo',
        'theme' => $theme,
        'aps' => $aps
    ));
    
})->bind('homepage');

// Security Tests //
$app->get('/login', function (Request $request) use ($app) {
    
    global $namaAplikasi;
    
    return $app['twig']->render('login_sky.html', array(
        'apptitle' => $namaAplikasi,
        'error' => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),
        'haha' => 'foo',
        'theme' => 'theme',
        'aps' => 'aps'
    ));
    
});

// Authentikasi
$app->get('/cekLogin', 'SimptkRasio\Auth::cekLogin');
// $app->get('/login', 'SimptkRasio\Auth::login');
// $app->get('/logoutApp', 'SimptkRasio\Auth::logoutApp');

$app['security.firewalls'] = array(
        'login_path' => array(
                'pattern' => '^/login$',
                'anonymous' => true
        ),
        'passgen' => array(
                'pattern' => '^/passgen.*$',
                'anonymous' => true
        ),
        'default' => array(
                'pattern' => '^/.*$',
                'anonymous' => false,
                'form' => array(
                        'login_path' => '/login',
                        'check_path' => '/login_check',
                ),
                'logout' => array(
                        'logout_path' => '/logout',
                        'invalidate_session' => false
                ),
                'users' => $app->share(function($app) {
                    return new SimptkRasio\UserProvider();
                }),
        )
);

$app['security.access_rules'] = array(
        array('^/login$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
        array('^/passgen.+$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
        array('^/.+$', 'ROLE_USER')
);
    

// Data SD
$app->get('/KebutuhanGuruSdNasional', 'SimptkRasio\DataSd::kebutuhanGuruNasional');
$app->get('/KebutuhanGuruSdPerWilayah', 'SimptkRasio\DataSd::kebutuhanGuruPerWilayah');
$app->get('/KebutuhanGuruSdPerMatpel', 'SimptkRasio\DataSd::kebutuhanGuruPerMatpel');
$app->get('/KebutuhanGuruSdSwastaPerWilayah', 'SimptkRasio\DataSdSwasta::kebutuhanGuruPerWilayah');

$app->get('/UnduhPerhitunganKgSd', 'SimptkRasio\DataSd::unduhPerhitunganKgSd');
$app->get('/PerhitunganKgSd', 'SimptkRasio\DataSd::perhitunganKgSd');
$app->get('/PerhitunganKgSdWilayah', 'SimptkRasio\DataSd::perhitunganKgSdWilayah');
$app->get('/PrintingSimulasiSdPerWilayah', 'SimptkRasio\DataSd::printingSimulasiPerWilayah');
$app->get('/ExcelSimulasiSdPerWilayah', 'SimptkRasio\DataSd::excelSimulasiPerWilayah');

// Data SMP
$app->get('/KebutuhanGuruSmpNasional', 'SimptkRasio\DataSmp::kebutuhanGuruNasional');
$app->get('/KebutuhanGuruSmpPerWilayah', 'SimptkRasio\DataSmp::kebutuhanGuruPerWilayah');
$app->get('/KebutuhanGuruSmpPerMatpel', 'SimptkRasio\DataSmp::kebutuhanGuruPerMatpel');
$app->get('/KebutuhanGuruSmpSwastaPerWilayah', 'SimptkRasio\DataSmpSwasta::kebutuhanGuruPerWilayah');
$app->get('/KebutuhanGuruBk', 'SimptkRasio\DataSmp::kebutuhanGuruBk');

$app->get('/UnduhPerhitunganKgSmp', 'SimptkRasio\DataSmp::unduhPerhitunganKgSmp');
$app->get('/PerhitunganKgSmp', 'SimptkRasio\DataSmp::perhitunganKgSmp');
$app->get('/PerhitunganKgSmpWilayah', 'SimptkRasio\DataSmp::perhitunganKgSmpWilayah');
$app->get('/PrintingSimulasiSmpPerWilayah', 'SimptkRasio\DataSmp::printingSimulasiPerWilayah');
$app->get('/ExcelSimulasiSmpPerWilayah', 'SimptkRasio\DataSmp::excelSimulasiPerWilayah');

// Component Data
$app->get('/CariMatpelCombo', 'SimptkRasio\CompData::cariMatpelCombo');
$app->get('/CariMatpelComboSmp', 'SimptkRasio\CompData::cariMatpelComboSmp');
$app->get('/SearchWilayah/{kode_wilayah}', 'SimptkRasio\CompData::search_wilayah');
$app->get('/CariWilayahCombo', 'SimptkRasio\CompData::cariWilayahCombo');
$app->get('/GridWilayah', 'SimptkRasio\CompData::gridWilayah');
$app->get('/CariSekolahCombo', 'SimptkRasio\CompData::cariSekolahCombo');

$app->post('/controller/search_wilayah', 'SimptkRasio\CompData::search_wilayah')->direct();

// Simulasi Mutasi
$app->get('/ListNominasi', 'SimptkRasio\SimulasiMutasi::listNominasi');
$app->get('/ListPtkNominasi', 'SimptkRasio\SimulasiMutasi::listPtkNominasi');
$app->get('/ListPtkSekolah', 'SimptkRasio\Info::listPtkSekolah');
$app->post('/SaveNominasi', 'SimptkRasio\SimulasiMutasi::saveNominasi');
$app->post('/DeleteNominasi', 'SimptkRasio\SimulasiMutasi::deleteNominasi');

// Print
$app->get('/PrintingData/{per}/{purpose}/{kode_wilayah}/{skup}/{mata_pelajaran_id}/{jenjang}', 'SimptkRasio\Printing::data');
$app->get('/ExcelData/{per}/{purpose}/{kode_wilayah}/{skup}/{mata_pelajaran_id}/{jenjang}', 'SimptkRasio\Excel::data');
$app->get('/ExcelDataGuru/{kode_wilayah}/{skup}/{mata_pelajaran_id}/{jenjang}/{status_sekolah}', 'SimptkRasio\Excel::dataGuru');

//{kode_wilayah}/{skup}/{mata_pelajaran_id}/{jenjang}/{status_sekolah}
$app->get('/ExcelAllDataGuru', 'SimptkRasio\Excel::allDataGuru');

// Get Info PTK
$app->get('/InfoPtk', 'SimptkRasio\InfoPtk::get');
$app->get('/InfoPtk/{which}', 'SimptkRasio\InfoPtk::get');


return $app;
?>
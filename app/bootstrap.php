<?php

	require_once 'config.php';
	error_reporting(E_ERROR);

	define ('ROOT', realpath(dirname(__FILE__).'/../'));
	define ('D', DIRECTORY_SEPARATOR);
	define ('P', PATH_SEPARATOR); 
	define ('SYSDIR', ROOT.D.'system'.D);
	
	// ROOT for print templates
	define ('TEMPLATEROOT', ROOT.D."src".D.APPNAME.D."templates".D);
	
	// Prepare Class Environment 
	$loader = require __DIR__.'/../vendor/autoload.php';
	$loader->add('SimptkRasio', __DIR__.'/../src/');

	// Initialize the App
	$app = new Silex\Application();
	
    // Attaching Config
    $app['app_config'] = $config;
    
	//  Propel 
	$app['propel.config_file'] = __DIR__.'/config/conf/simptk_rasio-conf.php';
	$app['propel.model_path'] = __DIR__.'/../src/';
	$app->register(new Propel\Silex\PropelServiceProvider());

	// Register Twig
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__.'/../web',
	));

	// Register Session
	$app->register(new Silex\Provider\SessionServiceProvider());

	// Register Security
	$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
	$app->register(new Silex\Provider\SecurityServiceProvider(), array());
	
	// Register Ext-Direct
	$app->register(new Direct\DirectServiceProvider(), array());

	// Set TIMEZONE
	date_default_timezone_set('Asia/Jakarta');

	return $app;